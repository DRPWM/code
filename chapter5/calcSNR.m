%% Automated calculation and reporting of SNR in a flat-field image
% By Johan Helmenkamp, 2018

% The function "calcSNR" reads the DICOM image from the current directory,
% places a 100x100 pixels ROI in the center of the image, and calculates 
% the SNR for that ROI. The result is then reported in a PDF document and 
% stored in a database.

function calcSNR(~)
%% Section 1 - finding the path to the DICOM file
tic

% Use the function dirPlus to create a list of all files in the current
% directory and subfolders
% "pwd" is a MATLAB command that returns the current working directory
allPaths = dirPlus(pwd);

% Pre-allocate the vector indexDICOM in computer memory
indexDICOM = zeros(numel(allPaths),1);

% Loop through the paths of all files in the current directory and
% sub-folders and create an index of all DICOM file locations in allPaths
for i = 1:numel(allPaths)
    imagePath = char(allPaths(i));
    if isdicom(imagePath)
        indexDICOM(i) = 1;
    end
end

% Close the program if there are no DICOM-files in the current directory,
% or if there are more than one
if sum(indexDICOM) == 0 || sum(indexDICOM) > 1
    return
else
    % Remove all paths from allPaths which are not DICOM-files
    pathImage = allPaths(logical(indexDICOM));
end

% Define paths to folders containing the reports and database
reportPath = fullfile(pwd,'Reports');
databasePath = fullfile(pwd,'Database');

% Create the report and database paths if they don't exist
if ~exist(databasePath, 'dir')
    mkdir(databasePath);
end
if ~exist(reportPath, 'dir')
    mkdir(reportPath);
end

%% Section 2 - reading the contents of the DICOM file to memory

% Read the metadata and the image data. Notice the conversion to datatype 
% "single" for the image data!
dcmInfo = dicominfo(pathImage{1});
dcmImage = single(dicomread(pathImage{1}));

% Find the size of the image in pixels and define the size of the ROI
sizeImage = size(dcmImage);
sizeY = sizeImage(1);
sizeX = sizeImage(2);
roiSize = 100;

% Define the coordinates of the ROI, so that it'll be placed in the center
% of the image, based on the image size
ROIPositionxMin = round(sizeX/2 - roiSize/2);
ROIPositionxMax = round(sizeX/2 + roiSize/2 - 1);
ROIPositionyMin = round(sizeY/2 - roiSize/2);
ROIPositionyMax = round(sizeY/2 + roiSize/2 - 1);

%% Section 3 - calculate the SNR in the ROI

% Calculate the mean and standard deviation in the ROI
meanVal = mean2(dcmImage(ROIPositionyMin:ROIPositionyMax,...
    ROIPositionxMin:ROIPositionxMax));
stdVal = std2(dcmImage(ROIPositionyMin:ROIPositionyMax,ROIPositionxMin: ...
    ROIPositionxMax));

% Calculate the SNR, which is casted to double for compliance with the
% SQLite database later on
SNR = double(meanVal/stdVal);

%% Section 4 - report the results

% Fire up the MATLAB Report Generator and make it available for deployment
import mlreportgen.dom.*
makeDOMCompilable();

% Define the filename of the report based on DICOM metadata
reportFileName = erase([dcmInfo.Manufacturer '_' dcmInfo.ManufacturerModelName ...
    '_' dcmInfo.StudyDate],'"');

% Create the Document object
report = Document(fullfile(reportPath,reportFileName), 'pdf','myTemplate');
open(report);

% Create some headings
append(report, 'Signal-to-noise ratio report','Title');

% Display the image in the background, visualize the ROI and save the image
% into the working directory
figSNR = figure('Visible','off');clf;
set(figSNR, 'position', [65 99 512 512])
ax = axes(figSNR, 'position', [0 0 1 1]);
imagesc(dcmImage);
colormap(gray);
axis off
rectangle('Position',[ROIPositionxMin ROIPositionyMin roiSize roiSize], ...
    'LineWidth',2);
print(figSNR,'-r0',fullfile(reportPath,'figSNR.png'),'-dpng')

% Insert the image and calculation result in the Document object
img = Image(fullfile(reportPath,'figSNR.png'));
img.Width = '5cm'; img.Height = '5cm';
result = Paragraph(sprintf('SNR: %1.1f', SNR),'Bold');
table = Table({img,result}); table.Width = '100%';
table.TableEntriesVAlign = 'middle';
table.entry(1,1).Style = {Width('6cm')};
append(report, table);

% Some exposure parameters
p1 = Paragraph('kVp: ');p1.Style = {WhiteSpace('preserve')};
d1 = Paragraph([num2str(dcmInfo.KVP) ' kV']);
d1.Style = {WhiteSpace('preserve')};
p2 = Paragraph('mAs: ');p2.Style = {WhiteSpace('preserve')};
d2 = Paragraph([num2str(round(dcmInfo.ExposureInuAs/1000),1) ' mAs']);
d2.Style = {WhiteSpace('preserve')};

% Present the parameters in the report
append(report, Heading3('Exposure parameters'));
exposureTable = Table({{p1;p2},{d1;d2}});
exposureTable.entry(1,2).Style = {Width('6cm')};
append(report, exposureTable);

close(report);

% Delete the separate image file
delete(fullfile(reportPath,'figSNR.png'));

%rptview(report)

%% Section 5 - store the results in an SQlite database

% Define the database filename
dbFile = fullfile(databasePath,'DB.db');

% Define the column names and data to input into the database table
colNames = {'StudyDate','SNR','InstitutionName','StationName', ...
    'SOPInstanceUID'};
colData = {dcmInfo.StudyDate,SNR,dcmInfo.InstitutionName, ...
        dcmInfo.StationName,dcmInfo.SOPInstanceUID};

% Get a list of all file paths in the databasePath
fileIndex = dirPlus(databasePath);
sizeIndex = size(fileIndex);

% Run the following code only if there are no files in the database path
% - this creates a new database file and populates it with the data
if sizeIndex(1) == 0 
    conn = sqlite(dbFile,'create');
    createTable = ['create table SNRTable ' ...
    '(studyDate VARCHAR, SNR DOUBLE, InstitutionName VARCHAR, ' ...
    'StationName VARCHAR, SOPInstanceUID VARCHAR)'];
    exec(conn,createTable)
    insert(conn,'SNRTable',colNames,colData)
    
% Otherwise, run the following code only if there is exactly 1 file in the 
% database path and the filename exactly matches the expected
% - this connects to the database file and populates it with the data
elseif sizeIndex(1) == 1 && strcmp(fileIndex{1,1},dbFile)
    conn = sqlite(dbFile);
    
    % Check if the data already exists in the database, and write to the
    % database only if not
    sqlQuery = 'SELECT `SOPInstanceUID` FROM SNRTable';
    results = fetch(conn,sqlquery);
    if strcmp(results{1,1},dcmInfo.SOPInstanceUID)
        close(conn)
        return
    else
        insert(conn,'SNRTable',colNames,colData)
    end
end

% Close the connection to the database
close(conn);
toc % "toc" for timing the code
end % Ends the function
