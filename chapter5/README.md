# Creating automated workflows using MATLAB
The code in this folder demonstrates how automated processes can be set up
using a simple example based on the calculation and reporting of the signal-to-noise ratio (SNR) for a flat-field x-ray image. The scripts are:

**calcSNR.m** - the example function which is organized in sections that exactly matches the book chapter  
**myTemplate.m** - an script that forms part of the reporting example

In addition one example DICOM image file, "myProjXray.m", is supplied. This forms the input data to the "calcSNR.m" function in this example.

## Setup ##
A base install of MATLAB is required.

Required toolboxes:  
- *Imaging Process Toolbox*  
- *Database Toolbox*  
- *MATLAB Report Gnerator*  

Tested with MATLAB R2020a.

## Contact ##
Johan Helmenkamp  
johan.helmenkamp@sll.se

## License ##
[MIT](https://choosealicense.com/licenses/mit/). See the license file in this folder (LICENSE.txt).