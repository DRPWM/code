% Import the Document Object Model (DOM) base class
import mlreportgen.dom.*

% Create a copy of the default DOM template file
mlreportgen.dom.Document.createTemplate('myTemplate','pdf');

% Unzip the template file so that it can be edited manually
unzipTemplate('myTemplate.pdftx','myTemplateDirectory');
zipTemplate('myTemplate.pdftx','myTemplateDirectory');