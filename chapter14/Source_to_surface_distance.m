%% Example input data
% Run Patient_beam_alignment.m script to get r_iso and r_target
Patient_beam_alignment; clearvars -except r_iso r_target
% The position of the target organ (heart) in the computational phantom:
rc_target=[0.27;-3.37;49.80];
% The x-ray beam rotation angles (rad) and the source-to-isocenter
% distance, SID [cm], for each irradiation event:
RDSR.PositionerPrimaryAngle    = [ 1.25307  1.25307  1.25307  1.25307  ...
    1.25307  1.25307  1.25307  1.25307 -0.19042 -0.19042 -0.19042 ...
    -0.19042 -0.08727  1.25813 1.25813 0.32289 0.32289 0.32289 0.32289 ...
    0.32289 0.32289 0.32289 0.32289 0.32289 0.32289 -0.11868 -0.11868 ...
    -0.11868 -0.14486 -0.12566];
RDSR.PositionerSecondaryAngle  = [-0.00925 -0.00925 -0.00925 -0.00925 ... 
    -0.00925 -0.00925 -0.00925 -0.07819 -0.07819 -0.07819 -0.07819 ...
    -0.02950 -0.11921 -0.11921 0.00995 0.00995 0.00995 0.00995 0.00995 ...
    0.00995 0.00995 0.00995 0.00995 0.00995 0.01065  0.01065  0.01065 ...
    0.01065  0.01065  0.01065];
RDSR.DistanceSourcetoIsocenter = repmat(76.5,1,30);
% Patient data:
patient.age    = 33 ; % [years].
patient.height = 180; % [cm].
patient.weight = 80 ; % [kg].
% Anonymous functions for the basic rotations Rx(), Ry(), Rz() [rad]:
Rx = @(alph) [ 1       ,  0        ,  0         ; ... 
               0       ,  cos(alph), -sin(alph) ; ...
               0       ,  sin(alph),  cos(alph)      ];
Ry = @(bet)  [ cos(bet),  0        ,  sin(bet)  ; ...
               0       ,  1        ,  0         ; ...
              -sin(bet),  0        ,  cos(bet)       ];
Rz = @(gam)  [ cos(gam), -sin(gam) ,  0         ; ...
               sin(gam),  cos(gam) ,  0         ; ...
               0       ,  0        ,  1              ];

%% Position vectors defining a ray line
% [INPUT]:  RDSR   - data extracted from RDSR, rearranged into a struct.
%           r_iso  - the isocenter position vector relative to the end of
%                    the table (output of Patient_beam_alignment.m script).
%           r_target  - the target position vector relative to the end of
%                    the table (output of Patient_beam_alignment.m script).
%           rc_target - the target position in the computational coordinate
%                    system (example data include in the .m file)
% [OUTPUT]: (u,v)  - position vectors defining a ray line L = u+k*v, 
%                    (eq. 14.8), in the computational coordinate system.

% The x-ray beam isocenter position (x;y;z) [cm;cm;cm] in the computational
% coordinate system (r^comp_iso in eq. 14.7), for each irradiation event
% (exposure series) part of a cardiovascular intervention:
rc_iso = rc_target + (r_iso-r_target);

% A ray line can be constructed using RDSR data:
gam  = RDSR.PositionerPrimaryAngle   ; % Rotation angle [rad].
alph = RDSR.PositionerSecondaryAngle ; % Rotation angle [rad].
dSID = RDSR.DistanceSourcetoIsocenter; % SID [cm].
eyc  = [0;1;0]; % Basis vector in the computational coordinate system.

for i = 1:numel(rc_iso(1,:)) % For each irradiation event.
    R = Rz(gam(i))*Rx(alph(i)); % Rotation (see section 14.2.3).
       
    u(:,i) = rc_iso(:,i)+R*eyc*dSID(i); % According to eq. 14.9.
    v(:,i) = rc_iso(:,i)-u(:,i);        % According to eq. 14.10.
end

%% Stylized (mathematical) phantom models
% General characteristics (age [years], height [cm], weigh [kg]):
phantom.age     = [ 0   ,  1   ,   5   ,  10   ,  15   ,  30   ];
phantom.height  = [50.9 , 74.4 , 109.1 , 139.8 , 168.1 , 178.6 ]; % h_0.
phantom.weight  = [ 3.4 ,  9.2 ,  19.0 ,  32.4 ,  56.3 ,  73.2 ]; % m_0.
% Principal dimensions in units of cm (specified in figure 14.4):
phantom.skull.a = [ 4.52,  6.13,   7.13,   7.43,   7.77,   8.00]; % a_s.
phantom.skull.b = [ 5.78,  7.84,   9.05,   9.40,   9.76,  10.00]; % b_s.
phantom.skull.c = [ 3.99,  5.41,   6.31,   6.59,   6.92,   7.15]; % c_s.
phantom.skull.l = [30.17, 42.50,  54.80,  67.18,  83.15,  91.90]; % l_s.
phantom.neck.r  = [ 2.8 ,  3.6 ,   3.8 ,   4.4 ,   5.2 ,   5.4 ]; % r_n.
phantom.trunk.a = [ 5.47,  7.56,   9.82,  11.92,  14.83,  17.20]; % a_t.
phantom.trunk.b = [ 4.9 ,  6.5 ,   7.5 ,   8.4 ,   9.8 ,  10.0 ]; % b_t.
phantom.trunk.l = [21.6 , 30.7 ,  40.8 ,  50.8 ,  63.1 ,  70.0 ]; % l_t.
% Target organ position (cm,cm,cm) (r^comp_target in eq. 14.7):
phantom.heart.x = [-0.02, -0.01,   0.16,   0.12,   0.11,   0.27];
phantom.heart.y = [-1.68, -2.43,  -2.68,  -2.87,  -3.48,  -3.37];
phantom.heart.z = [15.97, 22.30,  29.47,  36.45,  44.92,  49.80];
   
%% Estimating the SSD for the exposure of a patient's trunk 
% [INPUT]:  patient - a struct containting patient information.
%           phantom - a struct containting phantom information.
%           (u,v)   - position vectors defining a ray line L = u+k*v.
%                     (example input data is included in the .m file)
% [OUTPUT]: dSSD    - array with source-to-surface distances.

% An anonymous function (+/-) is constructed for (k+,k-) in eq. 14.13:
kp = @(U,V) - (dot(U,V) + sqrt(norm(V)^2-norm(cross(U,V))^2))/norm(V)^2;
km = @(U,V) - (dot(U,V) - sqrt(norm(V)^2-norm(cross(U,V))^2))/norm(V)^2;

% A phantom model is selected by rounding the patient age to the nearest
% phantom model age [0 1 5 10 15 30] (Patients over 18y are dealt the 
% adult phantom (30y)):
modelNo = sum((patient.age./[0.5, 3.0, 7.5, 12.5, 18]) > 1) + 1;
% And patient-specific scaling (sxy,sxy,sz) is applied using eq. 14.12:
sxy = sqrt( phantom.height(modelNo)*patient.weight / ...
           (phantom.weight(modelNo)*patient.height) );
sz  = patient.height/phantom.height(modelNo);
s = [sxy;sxy;sz]; % Scaling vector.
   
% The vectors U and V that enter eq. 14.13 are calculated:
w = s.*[phantom.trunk.a(modelNo); phantom.trunk.b(modelNo); Inf].^-1;
d = s.*[0                       ; 0                       ; 0  ]    ;
U = w.*(u-d); V = w.*v; % According to eq. 14.14.
   
for i = 1:numel(u(1,:)) % For each irradiation event.
    % The intersection points are determined according to eq. 14.8:
    Lp = u(:,i) + kp(U(:,i),V(:,i))*v(:,i); 
    Lm = u(:,i) + km(U(:,i),V(:,i))*v(:,i);
    % And the SSD is determined according to eq. 14.15:
    dSSD(i) = min( norm(Lp-u(:,i)), norm(Lm-u(:,i)) );
end