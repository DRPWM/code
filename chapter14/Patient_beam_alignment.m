%% Example input data
% The x-ray beam isocenter and table end position (x;y;z) [cm;cm;cm] in the
% x-ray system coordinate system (r_b and r_t in eq. 14.5), for each
% irradiation event (exposure series) part of a neurovascular intervention:
RDSR.xrayBeamPos = repmat([0;-113.5;151],1,30);
RDSR.tableEndPos = [ -3.34  -3.34  -3.34  -3.34  -3.34  -3.34  -3.34 ...
    -3.30  -1.67  -1.65  -1.14  -1.14  -1.15  -1.15  -1.15  -1.15  ...
    -1.15  -1.15  -1.15  -0.55  -0.55  -0.55  -0.91  -2.20  -2.20  ...
    -2.20  -1.84  -1.84  -1.84  -1.35; ...
    -96.40 -96.40 -96.40 -96.40 -96.40 -96.40 -96.40 -96.40 -96.40 ...
    -96.40 -96.40 -96.40 -96.40 -96.40 -96.40 -94.20 -94.20 -94.20 ...
    -94.20 -94.20 -94.20 -94.20 -94.20 -94.20 -94.20 -94.20 -94.20 ...
    -94.20 -94.20 -94.20; ...
    196.83 192.83 180.83 176.83 176.83 176.83 176.83 171.33 168.44 ...
    166.34 165.25 165.25 165.85 165.85 165.85 165.85 165.85 165.85 ...
    165.85 166.45 166.45 166.45 185.05 188.34 188.34 188.34 186.44 ...
    166.44 166.44 165.74 ];
% The duration of each irradiation event (exposure series) [s]:
RDSR.ExposureDuration = [5.60 2.08 4.80 6.72 1.12 1.28 2.24 3.84 1.60 ...
    3.52 1.44 6.24 10.64 10.32 3.52 4.00 14.50 14.50 5.36 1.20 4.07 ...
    2.56 3.04 51.20 26.64 3.84 2.67 24.32 5.44 0.80]; 

%% Locating the target organ position
% [INPUT]:  RDSR     - data extracted from RDSR, rearranged into a struct
%                      (example input data is included in the .m file).
% [OUTPUT]: r_target - the position [x;y;z] of the patient's target organ  
%                      in the x-ray system coordinate system.

% Spatial extent of head fixation apparatus relative to the table end [cm]:
headFixLimit_maxWidth  = 25; headFixLimit_minWidth  = -25;
headFixLimit_maxLength =  0; headFixLimit_minLength = -30;

% The beam isocenter relative to the end of patient table is determined:
r_iso = RDSR.xrayBeamPos-RDSR.tableEndPos; % According to eq. 14.5.

% The irradiation events with the beam isocenter located within the
% limits imposed by a head fixation apparatus are identified
% (relevant only for neurovascular interventions):
isEvent = r_iso(1,:) < headFixLimit_maxWidth  & ...
          r_iso(1,:) > headFixLimit_minWidth  & ...
          r_iso(3,:) < headFixLimit_maxLength & ... 
          r_iso(3,:) > headFixLimit_minLength      ;
% And the associated (x,y,z) positions are defined:
[X,Y,Z] = deal( r_iso(1,isEvent), ... % deal is a native function to
                r_iso(2,isEvent), ... % distribute input variables
                r_iso(3,isEvent) );   % to outputs

% Each irradiation event is given an integer exposure duration [s]:
w = ceil(RDSR.ExposureDuration(isEvent)); % ceil rounds up to whole sec.

% The (x,y,z) positions are converted into a time series, i.e., 
% the positions are indexed in time order (1 s intervals):
[XinTime,YinTime,ZinTime] = deal([]);
for i = 1:numel(w) % For each irradiation event
    % (x,y,z) of irradiation event "i" is converted into a time series:
    iX = repmat( X(i), 1, w(i) ); % repmat (a native function) returns
    iY = repmat( Y(i), 1, w(i) ); % an array of w(i) copies of e.g. X(i).
    iZ = repmat( Z(i), 1, w(i) );
    % The different time series (iX,iY,iZ) are iteratively concatenated:
    XinTime = cat( 2, XinTime, iX ); % cat (a native function)   
    YinTime = cat( 2, YinTime, iY ); % concatenates e.g. XinTime and iX. 
    ZinTime = cat( 2, ZinTime, iZ ); 
end

% The target organ position is approximated according to eq. 14.6: 
r_target = [median(XinTime); ... 
            median(YinTime); ... 
            median(ZinTime)];
