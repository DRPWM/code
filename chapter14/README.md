# Chapter 14: Reconstructing the geometry of x-ray interventions
The demo scripts in this folder execute the code discussed in Chapter 14. The scripts are:

**Elementary\_vector\_algebra.m** - a script to translate, scale, and rotate vectors

**Patient\_beam\_alignment.m** - an example script to reconstruct the patient to beam alignment

**Source\_to\_surface\_distance.m** - an example script to reconstruct the source to surface distance

**Incident\_air\_kerma.m** - an example script to estimate the incident air Kerma to a patient

"Elementary\_vector\_algebra.m" is a standalone script that demonstrates the principles of coordinate transforms.

The scripts "Patient\_beam\_alignment.m", "Source\_to\_surface\_distance.m" and "incident\_air\_kerma.m" are associated. "Patient\_beam\_alignment.m" runs independently. However, "Source\_to\_surface\_distance.m" is dependent on "Patient\_beam\_algorithm.m" (it runs it). Further, "Incident\_air\_kerma" is dependent on "Source\_to\_surface\_distance.m" (it runs it).

## Setup ##
A base install of MATLAB is required.

Required toolboxes:  
- *None*

Tested with MATLAB R2019b.

## Contact ##
Artur Omar  
artur.omar@sll.se

## License ##
[MIT](https://choosealicense.com/licenses/mit/). See the license file in this folder (LICENSE.txt).
