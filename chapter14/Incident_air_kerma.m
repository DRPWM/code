%% Example input data
% Run Source_to_surface_distance.m script to get dSSD
Source_to_surface_distance; clearvars -except dSSD
% Table transmission (=1.00 for projections that don't intersect the table):
ftable = [1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 0.81, 0.82, ...
          0.82, 0.82, 0.83, 1.00, 1.00, 0.79, 0.79, 0.79, 0.79, 0.79, ...
          0.78, 0.78, 0.78, 0.78, 0.78, 0.82, 0.82, 0.82, 0.83, 0.83];
% The reference air kerma [mGy] for each irradiation event:
RDSR.Kref = [20.68, 23.45, 24.07, 11.41, 8.79, 14.27, 5.87, 25.41, ...
             28.02, 20.43, 13.27, 9.75, 17.27, 22.63, 13.41, 12.41, ...
             13.97, 12.57, 5.32, 23.68, 17.63, 5.75, 10.30, 9.48, ...
             7.87, 20.54, 29.15, 5.99, 24.18, 15.89];
% The source-to-isocenter distance, SID [cm], for each irradiation event:
RDSR.DistanceSourcetoIsocenter = repmat(76.5,1,30);

%% Calculating the incident air kerma
% [INPUT]:  RDSR   - data extracted from RDSR, rearranged into a struct.
%           dSSD   - array with source-to-surface distances (section 14.4)
%                    (output of Source_to_surface_distance.m script).
%           ftable - array with predetermined table transmission factors
%                    (example input data is included in the .m file).
% [OUTPUT]: Ki     - array with incident air kerma values.

% The source-to-reference distance is determined:
dSRD = RDSR.DistanceSourcetoIsocenter-15; % 15 cm from the isocenter
% The incident air kerma is determined by element-wise multiplication:
Ki   = RDSR.Kref.*ftable.*(dSRD./dSSD).^2; % According to eq. 14.16.
