%% Vector translation
r = [1,1,1]; t = [1,2,3]; % An arbitrary position vector and translation.

rp = r+t;    % r'= [2,3,4].

%% Vector scaling
r = [1,1,1]; s = [1,2,3]; % An arbitrary position and scaling vector.

rp = s.*r;   % r'= [1,2,3].

%% Anonymous functions for the basic rotations Rx(), Ry(), Rz() (rad):
Rx = @(alph) [ 1       ,  0        ,  0         ; ... 
               0       ,  cos(alph), -sin(alph) ; ...
               0       ,  sin(alph),  cos(alph)      ];
Ry = @(bet)  [ cos(bet),  0        ,  sin(bet)  ; ...
               0       ,  1        ,  0         ; ...
              -sin(bet),  0        ,  cos(bet)       ];
Rz = @(gam)  [ cos(gam), -sin(gam) ,  0         ; ...
               sin(gam),  cos(gam) ,  0         ; ...
               0       ,  0        ,  1              ];

%% Vector rotation
r = [0;0.6;0];                       % Position vector in fig. 14.2.
alph = pi/2; bet = pi/2; gam = pi/4; % Rotation angles in fig. 14.2.

rp = Rz(gam)*Ry(bet)*Rx(alph)*r; % r'= 0.6.*[cos(pi/4); sin(pi/4); 0].


