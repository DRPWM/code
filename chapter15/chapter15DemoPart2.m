% chapter15DemoPart2
%
% Demonstrates calculation of fractal Perlin noise using pre-generation of 
% octaves and shows the application of a perturbation function
%
% The code in this script is divided into two sections.
% The sections can be run separately using the "Run Section" button.
% The first section must have been run once to allow the second to execute
% (the first section generates data needed by the second)
%
% Sections may take quite some time to execute: please be patient!
% The code is demanding on memory. If MATLAB keeps crashing, try closing 
% unneeded applications on your computer. If that doesn't help, try 
% reducing the grid size from 256 to 128 (that will require changes
% in the "pregenData.m" scripts as well as in this file).

%% Pregenerating octaves
% Uses the script "pregenData" in "codeFiles" subfolder

% Allow the script to find the script in the "codeFiles" folder
addpath codeFiles;

% Clear the workspace to save memory
clearvars;

% Let the user know the script is running
disp(" "); disp("Pregenerating Perlin Noise. Please wait!");

% Pregenerate Perlin noise using the "pregenData" script
pregenData;


%% Generating fractal Perlin noise using pregenerated octaves
% Based on "codeSnippet8.m" in the "chapterFiles" folder
% Noise image is displayed using MATLAB's imshow

% Allow the script to find the functions in the "codeFiles" folder
addpath codeFiles;

% Clear the workspace to save memory
clearvars;

% Let the user know the script is running
disp(""); disp("Calculating Perlin Noise. Please wait!");

% Generate fractal noise and apply a perturbation function
[noise] = fractalPregen(0.8,1.5,256);
perturbation = @(x) (1-abs(x)).^6;
perturbedNoise = perturbation(noise);

% Project to a 2D noise image
noise2D = mean(noise,3);

% Free up some memory
clear noise

% Display the image
figure('Name','Fractal Perlin noise with perturbation')
imshow(noise2D,[])
drawnow

% Free up some memory
clearvars;