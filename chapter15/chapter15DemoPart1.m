% chapter15DemoPart1.m
%
% Demonstrates the generation and visualization of Perlin noise
%
% The code in this script is divided into three sections.
% The sections can be run separately using the "Run Section" button.
%
% Sections may take quite some time to execute: please be patient!
% The code is demanding on memory. If MATLAB keeps crashing, try closing 
% unneeded applications on your computer. If that doesn't help, try 
% reducing the grid size from 256 to 128

%% Generating Perlin noise of specified grid and gradient field size
% Based on "codeSnippet4.m" in the "chapterFiles" folder
% Noise image is displayed using MATLAB's imshow

% Allow the script to find the functions in the "codeFiles" folder
addpath codeFiles;

% Clear the workspace to save memory
clearvars;

% Let the user know the script is running
disp(" "); disp("Calculating Perlin Noise. Please wait!");

% Generate 3D Perlin noise
[noise] = perlinNoise(16,256);

% Project to a 2D noise image
noise2D = mean(noise,3);

% Display the image
figure('Name','Perlin noise')
imshow(noise2D,[])
drawnow


%% Generating 8 frequencies of Perlin noise using the gradient field size
% Based on "codeSnippet5.m" in the "chapterFiles" folder
% Noise images are displayed using MATLAB's montage

% Allow the script to find the functions in the "codeFiles" folder
addpath codeFiles;

% Clear the workspace to save memory
clearvars;

% Let the user know the script is running
disp(" "); disp("Calculating Perlin Noise. Please wait!");

% Generate Perlin noise for 8 frequencies 
for index = 1:8
disp("Frequency "+num2str(index)+" of 8")
[out{index}] = perlinNoise(2^(index-1),256);
end

% Create a multiframe image from the output (one frame for each frequency)
multi = mean(out{1},3);
for index = 2:8
noise2D = mean(out{index},3);
multi=cat(3,multi,noise2D);
end

% Free up some memory
clear out noise2D;

% Display the different frequencies of Perlin noise
figure('Name','Perlin noise (8 freq)')
montage(multi,'DisplayRange',[],'Size',[2 4])
drawnow


%% Generating fractal Perlin noise: persistence =  0.5, lacunarity = 2
% Based on "codeSnippet6.m"/"codeSnippet7.m" in the "chapterFiles" folder
% Noise image displayed using MATLAB's imshow

% Allow the script to find the functions in the "codeFiles" folder
addpath codeFiles:

% Clear the workspace to save memory
clearvars;

% Let the user know the script is running
disp(" "); disp("Calculating Perlin Noise. This may take a long time!");

% Calculate the fractal noise with specified persistence and lacunarity
[noise,~] = fractalNoise(0.5,2,256);

% Project to a 2D noise image
noise2D = mean(noise,3);

% Free up some memory
clear noise;

% Display the image
figure('Name','Fractal Perlin noise')
imshow(noise2D,[])
drawnow

% Free up some memory
clearvars;