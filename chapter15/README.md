# Chapter 15: Simulation of anatomicalstructure in mammography and breast tomosynthesis using Perlin noise
This folder contains code relevant to Chapter 15. There are two subfolders at the level of this README file:

**chapterFiles** - contains the MATLAB code extracts presented in the text of the book chapter

**codeFiles** - complete code for the chapter saved as separate MATLAB functions and scripts

To make it easier for the user to get started we have created two demo scripts:

**chapter15DemoPart1.m** - consists of three sections demonstrating use of the *perlinNoise()* and *fractalNoise()* functions and displaying the resulting noise images

**chapter15DemoPart2.m** - consists of two sections; the first pregenerates octaves of Perlin noise and the second uses these to create noise images using the *fractalPregen()* function. This function does the same job as *fractalNoise()* but is considerably quicker (once the necessary data is pregenerated).

The sections in the script can be run independently in MATLAB using the "Run Section" button. Sections may take quite some time to execute so please be patient!

The code is also demanding on computer memory. If MATLAB keeps crashing, try closing unneeded applications on your computer. If that doesn't help, try reducing the grid size from 256 to 128.

When the first section of "chapter15DemoPart1.m" is executed, a folder caled "pregenData" is created at the same level as "chapterFiles" and "codeFiles" and the pregenerated octaves are stored in there. With the default settings, around 5 GB of data is generated. The *lacunarity* and *gridsize* values are hardcoded for the pregeneration. If you wish to change the values you will need to edit the script "codeFiles/pregenData.m".

## Setup ##
A base install of MATLAB is required.

Required MATLAB toolboxes:  
- *None*

Tested with MATLAB R2019b.

## Contact ##
Magnus Dustler  
Magnus.Dustler@med.lu.se

## License ##
[MIT](https://choosealicense.com/licenses/mit/). See the license file in this folder (LICENSE.txt).
