function [x,y,z,gridSize] = generateGrid(gridSize)
% Generate grid
gridSize = single(gridSize);
if length(gridSize) == 1
    [X,Y,Z] = meshgrid(1:gridSize,1:gridSize,1:gridSize);
    gridSize = [gridSize gridSize gridSize];
else
    [X,Y,Z] = meshgrid(1:gridSize(1),1:gridSize(2),1:gridSize(3));
end
% Reshape coordinate gridSizes to vectors
x = reshape(single(X),gridSize(1)*gridSize(2)*gridSize(3),1);
y = reshape(single(Y),gridSize(1)*gridSize(2)*gridSize(3),1);
z = reshape(single(Z),gridSize(1)*gridSize(2)*gridSize(3),1);
end
