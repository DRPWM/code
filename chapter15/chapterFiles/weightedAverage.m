function [v] = weightedAverage(xrel,yrel,zrel,dots)
% Distances
tx0 = xrel;
tx1 = abs(xrel-1);
ty0 = yrel;
ty1 = abs(yrel-1);
tz0 = zrel;
tz1 = abs(zrel-1);

% Define weighted average polynomial
w = @(x)6.*x.^5-15.*x.^4 + 10.*x.^3;

% Calculate weights
wx0 = w(tx0);
wx1 = w(tx1);
wy0 = w(ty0);
wy1 = w(ty1);
wz0 = w(tz0);
wz1 = w(tz1);

% Weights of corners 1:8
weight(:,1) = wx1.*wy1.*wz1;
weight(:,2) = wx1.*wy0.*wz1;
weight(:,3) = wx0.*wy1.*wz1;
weight(:,4) = wx0.*wy0.*wz1;
weight(:,5) = wx1.*wy1.*wz0;
weight(:,6) = wx1.*wy0.*wz0;
weight(:,7) = wx0.*wy1.*wz0;
weight(:,8) = wx0.*wy0.*wz0;

% Weighted sum
v = sum(dots.*weight,2);
end
