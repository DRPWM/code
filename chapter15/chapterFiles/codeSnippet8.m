[noise] = fractalPregen(0.8,1.5,256);
perturbation = @(x) (1-abs(x)).^6;
perturbedNoise = perturbation(noise);