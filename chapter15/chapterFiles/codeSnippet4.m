% Generate 3D Perlin noise
[noise] = perlinNoise(16,256);

% Project to a 2D noise image
noise2D = mean(noise,3);

% Display the image
imshow(noise2D,[])
