function [gradients] = randomGradfield(inSize)
% Generates a random set of gradient vectors associated with a 3D gradient field
% inSize: Size of gradient field in a 1x3 vector [i,j,k]
% gradients: nX3 set of gradient coordinates
[x,y,z] = cube12rand((inSize(1)+1)*(inSize(2)+1)*(inSize(3)+1));
gradients = single([x' y' z']');
end
