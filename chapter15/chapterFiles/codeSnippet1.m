% Find size of cubes in Perlin field
cubeSize = min(gridSize)./min(fieldSize);
% Number of edges to gradient field
fieldSizePlus1 = fieldSize+1; 
 
% Determine position of points within field cube
x1 = ceil(x/cubeSize)+1;
x0 = x1 - 1;
y1 = ceil(y/cubeSize)+1;
y0 = y1 - 1;
z1 = ceil(z/cubeSize)+1;
z0 = z1 - 1;
 
% Find gradients of cube corners for each point
% 1: [0 0 0] 2: [0 1 0] 3: [1 0 0] 4: [1 1 0]
% 5: [0 0 1] 6: [0 1 1] 7: [1 0 1] 8: [1 1 1]
% Preallocate matrix
grad = single(zeros(length(x),24));
% Gradient of 1st corner
grad(:,1:3) = gradients(:,sub2ind([fieldSizePlus1(1) fieldSizePlus1(2) ...
    fieldSizePlus1(3)],x0,y0,z0))';
% Gradient of 2nd corner
grad(:,4:6) = gradients(:,sub2ind([fieldSizePlus1(1) fieldSizePlus1(2) ...
    fieldSizePlus1(3)],x0,y1,z0))';
% Gradient of 3rd corner
grad(:,7:9) = gradients(:,sub2ind([fieldSizePlus1(1) fieldSizePlus1(2) ...
    fieldSizePlus1(3)],x1,y0,z0))';
% Gradient of 4th corner
grad(:,10:12) = gradients(:,sub2ind([fieldSizePlus1(1) fieldSizePlus1(2) ...
    fieldSizePlus1(3)],x1,y1,z0))';
% Gradient of 5th corner
grad(:,13:15) = gradients(:,sub2ind([fieldSizePlus1(1) fieldSizePlus1(2) ...
    fieldSizePlus1(3)],x0,y0,z1))';
% Gradient of 6th corner
grad(:,16:18) = gradients(:,sub2ind([fieldSizePlus1(1) fieldSizePlus1(2) ...
    fieldSizePlus1(3)],x0,y1,z1))';
% Gradient of 7th corner
grad(:,19:21) = gradients(:,sub2ind([fieldSizePlus1(1) fieldSizePlus1(2) ...
    fieldSizePlus1(3)],x1,y0,z1))';
% Gradient of 8th corner
grad(:,22:24) = gradients(:,sub2ind([fieldSizePlus1(1) fieldSizePlus1(2) ...
    fieldSizePlus1(3)],x1,y1,z1))';
 
% Determine relative coordinates of points within relevant cubes
xrel = (x/cubeSize+1)-x0;
yrel = (y/cubeSize+1)-y0;
zrel = (z/cubeSize+1)-z0;
 
% Corners: Relative positions
% 1: [0 0 0] 2: [0 1 0] 3: [1 0 0] 4: [1 1 0]
% 5: [0 0 1] 6: [0 1 1] 7: [1 0 1] 8: [1 1 1]
c0 = single(zeros(length(x),1));
c1 = single(ones(length(x),1));
corners = {[c0 c0 c0] [c0 c1 c0] [c1 c0 c0] [c1 c1 c0] [c0 c0 c1]...
    [c0 c1 c1] [c1 c0 c1] [c1 c1 c1]};
