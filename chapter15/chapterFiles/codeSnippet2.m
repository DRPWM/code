% Calculate vectors between points and corners and calculate dot products
% between them and respective gradients, i.e. calculate corner values 1:8

dots = single(zeros(length(x),8));
for i = 1:8
    dots(:,i) = sum(grad(:,(1+3*(i-1)):(3+3*(i-1)))'.*([xrel yrel zrel]'...
        -corners{i}'))';
end
% Perform weighted averaging
[v] = weightedAverage(xrel,yrel,zrel,dots);
