function [ex,ey,ez] = cube12rand(n)
% Generates [i,j,k] coordinates of n vectors chosen from the set defined below
gradients = [[0 1 1]' [0 1 -1]' [0 -1 1]' [0 -1 -1]' [1 0 1]' [-1 0 1]'...
    [1 0 -1]' [-1 0 -1]' [1 1 0]' [-1 1 0]' [1 -1 0]' [-1 -1 0]'];
gradients = bsxfun(@rdivide, gradients, sqrt(sum(abs(gradients).^2,1)));
numbers = randi([1 12],1,n);
ee = gradients(:,numbers);
ex = ee(1,:);
ey = ee(2,:);
ez = ee(3,:);
end


