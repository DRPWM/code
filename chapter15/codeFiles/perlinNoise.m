function [out,inds,v] = perlinNoise(fieldSizeSc,gridSize,gradField,coords)
% fieldSizeSc: size of the Perlin gradient field in one dimension 
% (frequency of noise)
% gridSize: size of the points grid over which the Perlin gradient field
% will be defined and evaluated
% gradField: Optional predetermined gradiant field
% coords: Optional evaluation of specific coordinates within the gridSize
% to be evaluated instead of the entire gridSize
% Please check http://www.noisemachine.com/talk1/index.html for futher
% information on Perlin noise
% Magnus Dustler 2017 magnus.dustler@med.lu.se

if nargin < 4
    [x,y,z,gridSize] = generateGrid(gridSize);
else
    % Collect coordinates to evaluate
    x = single(coords(:,1));
    y = single(coords(:,2));
    z = single(coords(:,3));
    clear coords
end
% Create gradient field
if nargin <4
    baseline = min(gridSize);
    fieldSize = [fieldSizeSc fieldSizeSc fieldSizeSc];
    fieldSize(1) = fieldSize(1)*(gridSize(1)/baseline);
    fieldSize(2) = fieldSize(2)*(gridSize(2)/baseline);
    fieldSize(3) = fieldSize(3)*(gridSize(3)/baseline);
    [gradients] = randomGradfield(fieldSize);
else
    baseline = min(gridSize);
    fieldSize = [fieldSizeSc fieldSizeSc fieldSizeSc];
    fieldSize(1) = fieldSize(1)*(gridSize(1)/baseline);
    fieldSize(2) = fieldSize(2)*(gridSize(2)/baseline);
    fieldSize(3) = fieldSize(3)*(gridSize(3)/baseline);
    gradients = gradField;
    clear gradField
end
% Find size of squares in Perlin field
cubeSize = min(gridSize)./min(fieldSize);
% Number of edges to gradient field
fieldSizePlus1 = fieldSize+1;

% Determine position of points within field cube
x1 = ceil(x/cubeSize)+1;
x0 = x1 - 1;
y1 = ceil(y/cubeSize)+1;
y0 = y1 - 1;
z1 = ceil(z/cubeSize)+1;
z0 = z1 - 1;

% Find gradients of cube corners for each point
% 1: [0 0 0] 2: [0 1 0] 3: [1 0 0] 4: [1 1 0]
% 5: [0 0 1] 6: [0 1 1] 7: [1 0 1] 8: [1 1 1]
% Preallocate matrix
grad = single(zeros(length(x),24));
% Gradient of 1st corner
grad(:,1:3) = gradients(:,sub2ind([fieldSizePlus1(1) fieldSizePlus1(2) ...
    fieldSizePlus1(3)],x0,y0,z0))';
% Gradient of 2nd corner
grad(:,4:6) = gradients(:,sub2ind([fieldSizePlus1(1) fieldSizePlus1(2) ...
    fieldSizePlus1(3)],x0,y1,z0))';
% Gradient of 3rd corner
grad(:,7:9) = gradients(:,sub2ind([fieldSizePlus1(1) fieldSizePlus1(2) ...
    fieldSizePlus1(3)],x1,y0,z0))';
% Gradient of 4th corner
grad(:,10:12) = gradients(:,sub2ind([fieldSizePlus1(1) fieldSizePlus1(2) ...
    fieldSizePlus1(3)],x1,y1,z0))';
% Gradient of 5th corner
grad(:,13:15) = gradients(:,sub2ind([fieldSizePlus1(1) fieldSizePlus1(2) ...
    fieldSizePlus1(3)],x0,y0,z1))';
% Gradient of 6th corner
grad(:,16:18) = gradients(:,sub2ind([fieldSizePlus1(1) fieldSizePlus1(2) ...
    fieldSizePlus1(3)],x0,y1,z1))';
% Gradient of 7th corner
grad(:,19:21) = gradients(:,sub2ind([fieldSizePlus1(1) fieldSizePlus1(2) ...
    fieldSizePlus1(3)],x1,y0,z1))';
% Gradient of 8th corner
grad(:,22:24) = gradients(:,sub2ind([fieldSizePlus1(1) fieldSizePlus1(2) ...
    fieldSizePlus1(3)],x1,y1,z1))';

% Determine relative coordinates of points within relevant cubes
xrel = (x/cubeSize+1)-x0;
yrel = (y/cubeSize+1)-y0;
zrel = (z/cubeSize+1)-z0;

% Corners: Relative positions
% 1: [0 0 0] 2: [0 1 0] 3: [1 0 0] 4: [1 1 0]
% 5: [0 0 1] 6: [0 1 1] 7: [1 0 1] 8: [1 1 1]
c0 = single(zeros(length(x),1));
c1 = single(ones(length(x),1));
corners = {[c0 c0 c0] [c0 c1 c0] [c1 c0 c0] [c1 c1 c0] [c0 c0 c1]...
    [c0 c1 c1] [c1 c0 c1] [c1 c1 c1]};

% Calculate vectors between points and corners and calculate dot products
% between them and respective gradients, i.e. calculate corner values 1:8
dots = single(zeros(length(x),8));
for i = 1:8
    dots(:,i) = sum(grad(:,(1+3*(i-1)):(3+3*(i-1)))'.*([xrel yrel zrel]'...
        -corners{i}'))';
end
% Perform weighted averaging
[v] = weightedAverage(xrel,yrel,zrel,dots);

% Collect results
inds = sub2ind([gridSize(1) gridSize(2) gridSize(3)],x,y,z);
out = zeros(gridSize(1),gridSize(2),gridSize(3),'single');
out(inds) = v;
end

function [x,y,z,gridSize] = generateGrid(gridSize)
% Generate grid
gridSize = single(gridSize);
if length(gridSize) == 1
    [X,Y,Z] = meshgrid(1:gridSize,1:gridSize,1:gridSize);
    gridSize = [gridSize gridSize gridSize];
else
    [X,Y,Z] = meshgrid(1:gridSize(1),1:gridSize(2),1:gridSize(3));
end
% Reshape coordinate gridSizes to vectors
x = reshape(single(X),gridSize(1)*gridSize(2)*gridSize(3),1);
y = reshape(single(Y),gridSize(1)*gridSize(2)*gridSize(3),1);
z = reshape(single(Z),gridSize(1)*gridSize(2)*gridSize(3),1);
end

function [gradients] = randomGradfield(inSize)
% Generates a random set of gradient vectors assosciated with a 3D gradient field
% inSize: Size of gradient field (either a scalar value or [i,j,k])
% gradients: nX3 set of gradient coordinates
if length(inSize) == 1
    inSize = [inSize inSize inSize];
end
[x,y,z] = cube12rand((inSize(1)+1)*(inSize(2)+1)*(inSize(3)+1));
gradients = single([x' y' z']');
end

function [v] = weightedAverage(xrel,yrel,zrel,dots)
% Distances
tx0 = xrel;
tx1 = abs(xrel-1);
ty0 = yrel;
ty1 = abs(yrel-1);
tz0 = zrel;
tz1 = abs(zrel-1);

% Calculate weights
wx0 = w(tx0);
wx1 = w(tx1);
wy0 = w(ty0);
wy1 = w(ty1);
wz0 = w(tz0);
wz1 = w(tz1);

% Weights of corners 1:8
weight(:,1) = wx1.*wy1.*wz1;
weight(:,2) = wx1.*wy0.*wz1;
weight(:,3) = wx0.*wy1.*wz1;
weight(:,4) = wx0.*wy0.*wz1;
weight(:,5) = wx1.*wy1.*wz0;
weight(:,6) = wx1.*wy0.*wz0;
weight(:,7) = wx0.*wy1.*wz0;
weight(:,8) = wx0.*wy0.*wz0;

% Weighted sum
v = sum(dots.*weight,2);
end

function [S] = w(e)
S = 6.*e.^5-15.*e.^4 + 10.*e.^3;
end

function [ex,ey,ez] = cube12rand(n)
% Generates [i,j,k] coordinates of n vectors chosen from the set defined below
gradients = [[0 1 1]' [0 1 -1]' [0 -1 1]' [0 -1 -1]' [1 0 1]' [-1 0 1]'...
    [1 0 -1]' [-1 0 -1]' [1 1 0]' [-1 1 0]' [1 -1 0]' [-1 -1 0]'];
gradients = bsxfun(@rdivide, gradients, sqrt(sum(abs(gradients).^2,1)));
numbers = randi([1 12],1,n);
ee = gradients(:,numbers);
ex = ee(1,:);
ey = ee(2,:);
ez = ee(3,:);
end
