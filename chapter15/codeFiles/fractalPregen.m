function [noise,octs] = fractalPregen(persistence,lacunarity,gridSize,func)
% persistence: Amplitude of next octave as fraction of preceeding one
% (dampening, gain)
% lacunarity: Frequency of next octave as fraction of preceeding one
% gridSize: Size of output noise volume
% func: Perturbation function to apply to octaves 
%  eg. @(x)max(max(max(abs(x))))-abs(x);

% Unless a perturbation function specified, apply no perturbation
if nargin <4 
    func = @(x) x;
end

% Locate pregenerated data and determine the number of iterarions available
pregenDir = fullfile('pregenData', ...
    ['l' num2str(lacunarity) '_s' num2str(gridSize)]);
nFiles=length(dir(fullfile(pregenDir,'f1_*.mat')));
if nFiles==0
    error('Pregenerated data is missing')
else
    nIter=nFiles;

% Calculate number of octaves needed
nOct = ceil(log(gridSize)/log(lacunarity));

% Predefine noise volume    
noise = single(zeros(gridSize,gridSize,gridSize));

% Run loop through frequencies
amplitude = 1;
for iOct = 1:nOct
    frequency = round(lacunarity^(iOct-1));
    iIter = randi(nIter); % Randomly select an iteration
    pregenFile=fullfile(pregenDir,['f' num2str(frequency) '_s' ...
        num2str(gridSize) '_i' num2str(iIter) '.mat']);
    vars = load(pregenFile);  % Read in the selected pregenerated octave
    oct = amplitude.*func(vars.oct); 
    noise = noise + oct;
    if nargout == 2
        octs{iOct} = oct;
    end   
    amplitude = amplitude*persistence;
end
end
