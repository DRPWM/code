function [noise,octs] = fractalNoise(persistence,lacunarity,gridSize)
% persistence: Amplitude of next octave as fraction of preceeding one
% (dampening, gain)
% lacunarity: Frequency of next octave as fraction of preceeding one >1 
% gridSize: Size of output noise volume

% Calculate number of octaves needed
nOct = ceil(log(gridSize)/log(lacunarity));

% Predefine noise volume
noise = zeros(gridSize,gridSize,gridSize);

% Run generation loop
amplitude = 1;
for iOct = 1:nOct
    frequency = round(lacunarity^(iOct-1));
    octave = amplitude*(perlinNoise(frequency,gridSize));   
    noise = noise + octave;
    if nargout == 2
        octs{iOct} = octave;
    end
    amplitude = amplitude*persistence;
end
end



