% Script to pregenerate octaves for use by fractalPregen.m
% lacunarity: Frequency of next octave as fraction of preceeding one >1
% gridSize: Size of output noise volume
% nIter: number of noise realizations for each frequency-gridSize combo

lacunarity = 1.5; % Edit this to change the lacunarity factor
gridSize =256; % Edit this to change the size of the output noise volume
nIter = 6; % Edit this to change the number of iterations per octave

% Create directory for pregenerated data
pregenDir = fullfile('pregenData', ['l' num2str(lacunarity) ...
    '_s' num2str(gridSize)]);
mkdir(pregenDir)

% Calculate number of octaves needed
nOct=ceil(log(gridSize)/log(lacunarity));

% Run generation loop
for iIter = 1:nIter
    for iOct = 1:nOct
        frequency = round(lacunarity^(iOct-1));
        disp(['iIter, iOct: ' num2str(iIter) ',' num2str(iOct)])
        oct=perlinNoise(frequency,gridSize);
        pregenFile=fullfile(pregenDir,['f' num2str(frequency) ...
            '_s' num2str(gridSize) '_i' num2str(iIter) '.mat']);
        save(pregenFile,'oct')
    end
    clear oct
end
