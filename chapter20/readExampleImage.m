%%%%%%%
%%%%%%% First and second chapter code snippets

fileID = fopen('settingsExample.xml');

allBytes = fread(fileID);
fclose(fileID);
allSettingsText = char(allBytes');


%%%%%%%
%%%%%%% Third and fourth chapter code snippets

parameterOfInterest = 'Study-Owner';

% Return the position of the byte corresponding to the first letter
bytePos = strfind(allSettingsText,parameterOfInterest); 

allQuotationsMarks = strfind(allSettingsText,'"');

% Find the first 3 QM following the bytePos
indexFirstThree = find(allQuotationsMarks>bytePos,3,'first');

% Find first byte of value = first byte after second QM
firstByte = allQuotationsMarks(indexFirstThree(2)) + 1;

% Find last byte of value = last byte before third QM
lastByte = allQuotationsMarks(indexFirstThree(3)) - 1;

% Extract value between second and third QM
studyOwner = allSettingsText(firstByte:lastByte); 


%%%%%%%
%%%%%%% Extra code for this demo (not in chapter text)

parameterOfInterest = 'Image-Depth';

% Return the position of the byte corresponding to the first letter
bytePos = strfind(allSettingsText,parameterOfInterest); 

allQuotationsMarks = strfind(allSettingsText,'"');

% Find the first 3 QM following the bytePos
indexFirstThree = find(allQuotationsMarks>bytePos,3,'first');

% Find first byte of value = first byte after second QM
firstByte = allQuotationsMarks(indexFirstThree(2)) + 1;

% Find last byte of value = last byte before third QM
lastByte = allQuotationsMarks(indexFirstThree(3)) - 1;

% Extract value between second and third QM
imageDepth = str2double(allSettingsText(firstByte:lastByte)); 


%%%%%%%
%%%%%%% Extra code for this demo (not in chapter text)

parameterOfInterest = 'Depth-Offset';

% Return the position of the byte corresponding to the first letter
bytePos = strfind(allSettingsText,parameterOfInterest);

allQuotationsMarks = strfind(allSettingsText,'"');

% Find the first 3 QM following the bytePos
indexFirstThree = find(allQuotationsMarks>bytePos,3,'first');

% Find first byte of value = first byte after second QM
firstByte = allQuotationsMarks(indexFirstThree(2)) + 1;

% Find last byte of value = last byte before third QM
lastByte = allQuotationsMarks(indexFirstThree(3)) - 1;

% Extract value between second and third QM
depthOffset = str2double(allSettingsText(firstByte:lastByte)); 


%%%%%%%
%%%%%%% Extra code for this demo (not in chapter text)

parameterOfInterest = 'Samples';

% Return the position of the byte corresponding to the first letter
bytePos = strfind(allSettingsText,parameterOfInterest);

allQuotationsMarks = strfind(allSettingsText,'"');

% Find the first 3 QM following the bytePos
indexFirstThree = find(allQuotationsMarks>bytePos,3,'first');

% Find first byte of value = first byte after second QM
firstByte = allQuotationsMarks(indexFirstThree(2)) + 1;

% Find last byte of value = last byte before third QM
lastByte = allQuotationsMarks(indexFirstThree(3)) - 1;

% Extract value between second and third QM
nSamples = str2double(allSettingsText(firstByte:lastByte)); 


%%%%%%%
%%%%%%% Extra code for this demo (not in chapter text)

parameterOfInterest = 'Image-Width';

% Return the position of the byte corresponding to the first letter
bytePos = strfind(allSettingsText,parameterOfInterest);

allQuotationsMarks = strfind(allSettingsText,'"');

% Find the first 3 QM following the bytePos
indexFirstThree = find(allQuotationsMarks>bytePos,3,'first');

% Find first byte of value = first byte after second QM
firstByte = allQuotationsMarks(indexFirstThree(2)) + 1;

% Find last byte of value = last byte before third QM
lastByte = allQuotationsMarks(indexFirstThree(3)) - 1;

% Extract value between second and third QM
imageWidth = str2double(allSettingsText(firstByte:lastByte)); 


%%%%%%%
%%%%%%% Extra code for this demo (not in chapter text)

parameterOfInterest = 'Lines';

% Return the position of the byte corresponding to the first letter
bytePos = strfind(allSettingsText,parameterOfInterest);

allQuotationsMarks = strfind(allSettingsText,'"');

% Find the first 3 QM following the bytePos
indexFirstThree = find(allQuotationsMarks>bytePos,3,'first');

% Find first byte of value = first byte after second QM
firstByte = allQuotationsMarks(indexFirstThree(2)) + 1;

% Find last byte of value = last byte before third QM
lastByte = allQuotationsMarks(indexFirstThree(3)) - 1;

% Extract value between second and third QM
nLines = str2double(allSettingsText(firstByte:lastByte)); 


%%%%%%%
%%%%%%% Extra code for this demo (not in chapter text)

pixelsPerMmWidth = nLines/imageWidth;
pixelsPerMmHeight = nSamples/(imageDepth-depthOffset);


%%%%%%%
%%%%%%% Fifth chapter code snippet

fileID = fopen('imageExample.rf');

%%% Start File Header %%%
dataVer = fread(fileID,4,'uint8'); % Version number
dataFormat = fread(fileID,4,'uint8'); % Format of image data
nFrames = fread(fileID,1,'uint32'); % Number of frames
fread(fileID,5,'uint32'); % Reserved (no storing, read to move pointer only)
%%% End file header %%%


%%%%%%%
%%%%%%% Sixth chapter code snippet

rfData = zeros(nSamples, nLines, nFrames);
 
for k=1:nFrames
    %%% Start frame header %%%
    timeStamp = fread(fileID,1,'double'); % Time stamp in milliseconds
    if k==1
        startTime=timeStamp;
    end
    if k==nFrames
        stopTime=timeStamp;
    end
    fread(fileID,4,'uint8'); % Frame format (not stored)
    fread(fileID,1,'uint32'); % dataSize (not stored)
    fread(fileID,4,'uint32'); % Reserved (not stored)
    %%% End frame header %%%
    
    % Reads frame data
    frameData = fread(fileID, nSamples*nLines, 'single');
    % Converts frame data from vector to matrix format
    rfData(:,:,k) = reshape(frameData,nSamples,nLines); 
end
 
fclose(fileID); % Close file
 
totalTime=stopTime-startTime; % Calculate acquisition time
frameRate = (nFrames-1)/totalTime; % Image frame rate


%%%%%%%
%%%%%%% Seventh chapter code snippet

% Display image
hilbertFrame = hilbert(rfData(:,:,1)); % Perform HT on first frame
amplitudeFrame = abs(hilbertFrame); % Extract the amplitude
bmode = log10(amplitudeFrame); % Log compress
clims = [1.5 4]; % Define display range
imageHandle = imagesc(bmode,clims); % Display B-Mode image
colormap(gray) % Change to grayscale

% Define start and stop values in mm for x- and y-directions
startX = 1/pixelsPerMmWidth;
stopX = nLines/pixelsPerMmWidth;
startY = 1/pixelsPerMmHeight + depthOffset;
stopY = nSamples/pixelsPerMmHeight + depthOffset;

% Change the x- and y-ranges for the displayed data
set(imageHandle, 'XData', [startX, stopX]);
set(imageHandle, 'YData', [startY, stopY]);
axis([startX stopX startY stopY])

% Set labels
xlabel('Width (mm)')
ylabel('Depth (mm)')


%%%%%%%
%%%%%%% Extra code for this demo (not in chapter text)

pause(3)


%%%%%%%
%%%%%%% Eighth chapter code snippet

figure; % Create figure
figureID = gca; % Retrieves ID for current axes
for k=1:nFrames
    hilbertFrame = hilbert(rfData(:,:,k)); % Perform HT on frame k
    amplitudeFrame = abs(hilbertFrame); % Extract the amplitude
    bmode = log10(amplitudeFrame); % Log compress
    imageHandle = imagesc(figureID,bmode); % Display image
    caxis(figureID,[1.5 4]); % Set colormap display limits
    colormap(gray) % Change colormap to grayscale
    % Change the x- and y-ranges for the displayed data
    set(imageHandle, 'XData', [startX, stopX]);
    set(imageHandle, 'YData', [startY, stopY]);
    axis([startX stopX startY stopY])
    
    pause(0.01) % Pause here for 0.01 seconds
end


%%%%%%%
%%%%%%% Extra code for this demo (not in chapter text)

pause(3)


%%%%%%%
%%%%%%% Ninth chapter code snippet

timePerFrame = 1/frameRate;
figure; % Create figure
figureID = gca; % Retrieves ID for current axes
tic
for k=1:nFrames
    timePassed=toc;
    if timePassed<(timePerFrame*k+1) && timePassed>(timePerFrame*k-1)
        hilbertFrame = hilbert(rfData(:,:,k)); % HT on frame k
        amplitudeFrame = abs(hilbertFrame); % Extract the amplitude
        bmode = log10(amplitudeFrame); % Log compress
        imageHandle = imagesc(figureID,bmode); % Display image
        caxis(figureID,[1.5 4]); % Set colormap display limits
        colormap(gray) % Change colormap to grayscale
        % Change the x- and y-ranges for the displayed data
        set(imageHandle, 'XData', [startX, stopX]);
        set(imageHandle, 'YData', [startY, stopY]);
        axis([startX stopX startY stopY])
        
        drawnow; % Pause-command not needed but drawnow forces MATLAB to
        % display the image before continuing 
    end
end