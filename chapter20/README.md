# Importation and visualization of ultrasound data
Raw data can often be exported from medical imaging devices but different manufacturers and models may export in different file formats. The code in this folder demonstrates an example of how to read an arbitrary, but predefined file format containing ultrasound image data and how to display the ultrasound images. The script is:

**readExampleImage.m** - an example script that reads a particular set of ultrasound binary data, extracts some information and plays a cine loop of the embedded ultrasound B-Mode images

In addition the required dataset files "imageExample.rf" and "settingsExample.xml" are supplied via the Dropbox link in the "Link to input data.txt" file.

## Setup ##
A base install of MATLAB is required.

Required toolboxes:  
- *Signal Processing Toolbox*

Tested with MATLAB R2020a.

## Contact ##
Tobias Erlöv  
tobias.erlov@bme.lth.se

## License ##
[MIT](https://choosealicense.com/licenses/mit/). See the license file in this folder (LICENSE.txt).