% ----------
% Function name: findBackgroundPoints.m
% Version: 1.0
% By Sven Mansson, 2018
% This function finds the position of sharp transitions between dark and 
% bright.
%
% Input: signal (double), skipExtra (double)
% Output: index (double) index of points
% ----------
function index = findBackgroundPoints(signal, skipExtra)
dsignal = diff(signal);
midpoint = round(length(dsignal)/2);

% Find the position of sharp transitions between dark and bright
% when moving from left to right along the signal profile
[~,startIndex] = min(dsignal(1:midpoint));  % Bright to dark
[~,stopIndex] = max(dsignal(midpoint:end)); % Dark to bright
startIndex = startIndex + 1 + skipExtra;
stopIndex = stopIndex + midpoint - 1 - skipExtra;
signal = signal(startIndex:stopIndex);
background = signal;
bNdxOld = [];
maxIter = 50; % Prevent endless loop

for iter = 1:maxIter
   backgroundLevel = median(background);
   standardDev = std(background);
   threshold = min(backgroundLevel + 2.5*standardDev,4*backgroundLevel);
   bNdx = find(signal < threshold);
   background = signal(bNdx);
   if isequal(bNdx, bNdxOld), break, end
   bNdxOld = bNdx;
end

if iter == maxIter
   warning('findBackgroundPixels: maximum number of iterations reached.')
end

% Return the indices of the background pixels along the signal profile
index = find(signal < threshold) + startIndex - 1;
end % findBackgroundPoints