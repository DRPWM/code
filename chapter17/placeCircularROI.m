% ----------
% Function name: placeCircularROI.m
% Version: 1.0
% By Sven Mansson, 2018
% This function calculates the coordinates needed to place a circular ROI.
%
% Input: data (double) Image data
% Output: pos (double), r0 (double) The outer and center positions of the ROI 
% ----------
function [pos,r0] = placeCircularROI(data,relDiameter)
% relDiameter is the ROI diameter as fraction of the phantom diameter
% compute a mask which finds bright pixels
mask = findBrightPoints(data);

% Find the extent of the mask
px = find(any(mask,1));
py = find(any(mask,2));

% Find the length and center of the mask. All lengths are in pixel units
xCenter = mean([px(1) px(end)]);
xLength = px(end) - px(1) + 1;
yCenter = mean([py(1) py(end)]);
yLength = py(end) - py(1) + 1;

phantomDiameter = mean([xLength yLength]);
ROIdiameter = phantomDiameter*relDiameter;
xMin = xCenter - ROIdiameter/2;
yMin = yCenter - ROIdiameter/2;
pos = [xMin yMin ROIdiameter ROIdiameter];
r0 = [xCenter yCenter];
end % placeCircularROI