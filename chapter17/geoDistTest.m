% ----------
% Function name: geoDistTest.m
% Version: 1.0
% By Sven Mansson, 2018
% This function evalutes the geometric distortion of the ACR phantom. 
%
% Input: imagePath (cell) A cell containing the paths to all of the images
% Output: result (struct) A struct containing the results from the test
% ----------
function result = geoDistTest(imagePath)
%% Load slice #5
info = dicominfo(imagePath{5});
data = double(dicomread(info));
result.originalImage = data;

% Show the image
figure(2), clf, colormap(gray(255)), ax = axes;
imagesc(ax, data)
set(gca,'DataAspectRatio',[1 1 1])

% This routine only works for isotropic image resolution
assertMsg = 'geoDistTest: cannot handle non-isotropic resolution';
assert(info.PixelSpacing(1)==info.PixelSpacing(2), assertMsg)
pixelSpacing = mean(info.PixelSpacing);

[pos, center] = placeCircularROI(data ,1);
ROI_c = imellipse(ax, pos);

% Draw 4 lines: vertical, horizontal and diagonals
alpha = [90 -45 0 45]*pi/180;
directions = {'vertical', 'NE-SW diagonal', 'horizontal', 'NW-SE diagonal'};
D = 0.25; % Profile resolution 0.25 pixel
radius = pos(3)/2;
r0 = 0:D:1.15*radius; % Define a radial spoke

for k = 1:length(alpha)
   % x- and y-coordinates for the lines are expressed in complex notation: r = x + j*y
   r = r0*exp(j*alpha(k));
   r = r(2:end);
   % Extend the spoke to cover the full diameter of the phantom
   r = [fliplr(-r) 0 r] + center(1) + j*center(2);
   
   % Get the intensity profile along the spoke
   profile = interp2(data, real(r), imag(r));
   
   % Find the length of the profile and convert to mm
   [lengthInPixels, mask] = findProfileLength(profile, D);
   profileLength(k) = lengthInPixels*pixelSpacing;
   
   % Draw the lines
   line(ax, real(r(mask)), imag(r(mask)), 'color', 'r');
   result.r{k} = r(mask);
end

% The true diameter of the ACR phantom is 190 mm
trueProfileLength = 190;
deviation = profileLength - trueProfileLength;
[~, ndx] = max(abs(deviation));
result.maxDeviation = deviation(ndx);
result.direction = directions{ndx};
result.profileLength = profileLength;
result.trueProfileLength = trueProfileLength;

% Check if the test passed the ACR's tolerance
md = abs(result.maxDeviation); % maximum deviation from true length
if md > 3
   fprintf('TEST FAILED. Maximum deviation > 3 mm: %1.1f mm.\n', md)
elseif md > 2
   fprintf('WARNING, maximum deviation > 2 mm: %1.1f mm.\n', md)
else
   fprintf('Geometric distortion test PASSED\n')
end
end % geoDistTest