% ----------
% Function name: findBrightPoints.m
% Version: 1.0
% By Sven Mansson, 2018
% This function finds bright points in an image. 
%
% Input: data (double) Image data
% Output: mask (logical) A mask with the bright points 
% ----------
function mask = findBrightPoints(data)
% Compute a mask which finds bright pixels
Hi = mean(data(:));
Lo = 0;
oldThreshold = 0;

for k = 1:20 % Prevent endless loop
   threshold = (Hi+Lo)/2;
   mask = data > threshold;
   d = abs(threshold - oldThreshold)/threshold;
   if d < 1e-4, break, end
   oldThreshold = threshold;
   Hi = mean(data(mask));
   Lo = mean(data(~mask));
end
end % findBrightPoints