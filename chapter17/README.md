# Chapter 17: Automating daily QC for an MRI scanner
The example scripts will show how images of a dedicated quality control phantom (the ACR MRI accreditation phantom) can be used to examine slice thickness, image uniformity and geometric distortion. The scripts are:

### Example script
**demoScript.m** - runs through all the example test scripts using user input for locating the image data folder

### Slice thickness test
**sliceThickTest.m** - analyzes the profile bars in the ACR phantom images to determine the slice thickness  
**doHoughTransform.m** - finds the profile bars in the ACR phantom images image using the Hough transform  
**findBackgroundPoints.m** - removes bright background areas in the vicinity of the bars  
**calculateSliceThickness.m** - find the full width at half maximum (FWHM) of the vertical profile of the bars

### Uniformity test
**uniformityTest.m** - analyzes a uniform area in the ACR phantom images to determine a uniformity measure  
**placeCircularROI.m** - creates a circular ROI based on the image data  
**findBrightPoints.m** - creates a ROI mask based on the mean signal level of the bright parts of the image  

### Geometric distortion test
**geoDistTest.m** - calculates a distortion metric based on the measured and known radius of the ACR phantom  
**placeCircularROI.m** - as above  
**findProfileLength.m** - calculates the length of the measured phantom radius  
**findBrightPoints.m** - as above

In addition an example image dataset of the ACR MRI accreditation phantom is supplied ("ExampleDataChapter17.zip").

## Setup ##
A base install of MATLAB is required.

Required MATLAB toolboxes:  
- *Image Processing Toolbox*

Tested with MATLAB R2020a.

## Contact ##
Sven Månsson  
sven.mansson@med.lu.se

## License ##
[MIT](https://choosealicense.com/licenses/mit/). See the license file in this folder (LICENSE.txt).
