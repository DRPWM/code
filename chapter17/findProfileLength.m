% ----------
% Function name: findProfileLength.m
% Version: 1.0
% By Sven Mansson, 2018
% This function calculates the length of a profile. 
%
% Input: signal (double), D (double) The profile and resolution of profile
% Output: L (double), mask (double) The length and mask of the profile.
% ----------
function [L, mask] = findProfileLength(signal, D)
mask = findBrightPoints(signal);
L = find(mask);
L = (L(end) - L(1))*D;
end % findProfileLength
