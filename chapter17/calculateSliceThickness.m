% ----------
% Function name: calculateSliceThickness.m
% Version: 1.0
% By Sven Mansson, 2018
% This function calculates the slice thickness from signal data
%
% Input: signal (double), dx (double) The signal and pixel spacing in an image
% Output: thickness (double) the calculated slice thickness
% ----------
function thickness = calculateSliceThickness(signal, dx)
% Skip 5 points at both ends of the signal profile
% and calculate the background level
backgroundIndices = findBackgroundPoints(signal, 5);
backgroundLevel = median(signal(backgroundIndices));

signal = signal(backgroundIndices(1):backgroundIndices(end));
[~,topNdx] = max(signal);
topLevel = mean(signal(topNdx-1:topNdx+1));
midLevel = mean([backgroundLevel topLevel]);
midNdx = find(signal>=midLevel);

% Calculate width in mm
FWHM = length(midNdx(1):midNdx(end))*dx;
thickness = FWHM/10;
end % calculateSliceThickness