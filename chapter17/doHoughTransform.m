% ----------
% Function name: doHoughTransform.m
% Version: 1.0
% By Sven Mansson, 2018
% This function performs the Hough Transform on an a scaled image.
%
% Input: data (double) An image that has been scaled by the image max value
% Output: rotationAngle (double), verticalPosition (double)
% ----------
function [rotationAngle, verticalPosition] = doHoughTransform(data)
binaryImage = imbinarize(data);
edgeImage = edge(binaryImage,'canny');
theta = -90:0.1:89.9; % Use angular resolution of 0.1 degrees

% Straight lines in edgeImage will produce large values in H
[H,~,rho] = hough(edgeImage,'Theta',theta);

% Search for the two largest peaks in H
P = houghpeaks(H,2);
rotationAngle = mean(theta(P(:,2))) + 90; % Input angle to imrotate

% Linear algebra to calculate the vertical position of the lines
% AFTER the image has been rotated such that the lines are horizontal
alpha = -mean(theta(P(:,2)))*pi/180; % Assume the lines are parallel
unitVector = [cos(alpha);sin(alpha)]; % Unit vector perpendicular to the lines
pointLine = unitVector*rho(P(:,1)); % Calculate a point on each line
centerPoint = [size(data,1);-size(data,2)]/2 + 0.5; % The center point of the image
vector = pointLine - [centerPoint centerPoint]; % Vector from centerPoint...
                                                % to pointLine
distance = dot([unitVector unitVector],vector); % Distance from centerPoint
                                                % to lines
% y-indices of the lines after rotation
verticalPosition = -round((centerPoint(2)+distance));
end % doHoughTransform
