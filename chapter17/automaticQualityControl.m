% ----------
% Function name: automaticQualityControl.m
% Version: 1.0
% By Sven Mansson, 2018
% This function makes an automatic analysis of DICOM images acquired of the
% "ACR MRI Accreditation Phantom".
%
% Input: None
% Output: result (struct) A struct containing the results from the IQ tests
% ----------
function result = automaticQualityControl
%% Locate the directory where the multi-slice data set is stored
defaultDirectory = 'ExampleDataChapter17';
imageDirectory = uigetdir(defaultDirectory, 'Locate multi-slice dataset');
if imageDirectory==0, return, end

% Store the path to each DICOM image
directoryList = dir(imageDirectory);
if isempty(directoryList), return, end
imageNum = 0;
for fileNum = 1:length(directoryList)
   if strcmp(directoryList(fileNum).name, '.') || ...
         strcmp(directoryList(fileNum).name, '..')
      continue
   end
   fileName = [imageDirectory filesep directoryList(fileNum).name];
   if isdicom(fileName)
      imageNum = imageNum+1;
      qualityControlImagesPath{imageNum} = fileName;
      % Find and store the slice location
      info = dicominfo(fileName);
      if isfield(info,'SliceLocation')
         sliceLocation(imageNum) = info.SliceLocation;
      else
         error('Could not detect slice location of file %s',fileName)
      end
   end
end

% Sort image paths according to slice location, starting with lowest value
[~,index] = sort(sliceLocation, 'ascend');
qualityControlImagesPath = qualityControlImagesPath(index);

% Do quality control tests
result.sliceThicknessTest      = sliceThickTest(qualityControlImagesPath);
result.uniformityTest          = uniformityTest(qualityControlImagesPath);
result.geometricDistortionTest = geoDistTest(qualityControlImagesPath);
disp('=== automaticQualityControl completed ok ===')

end % automaticQualityControl