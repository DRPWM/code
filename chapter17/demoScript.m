% Script to run a demo of daily QC for MRI 
 
disp('Run QC tests')
results = automaticQualityControl;
disp('Display Results')
sliceThickness = results.sliceThicknessTest.sliceThickness;
fprintf('Slice thickness: %.2f\n', sliceThickness)
uniformity = results.uniformityTest.uniformity;
fprintf('Uniformity: %.2f %%\n', uniformity)
geoDistMaxDev = results.geometricDistortionTest.maxDeviation;
geoDistDirection = results.geometricDistortionTest.direction;
fprintf('Max Geometric Distortion: %.2f, Direction: %s\n', geoDistMaxDev, geoDistDirection)