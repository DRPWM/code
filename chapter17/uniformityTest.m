% ----------
% Function name: uniformityTest.m
% Version: 1.0
% By Sven Mansson, 2018
% This function performs a uniformity test on the ACR phantom. 
%
% Input: imagePath (cell) A cell containing the paths to all of the images
% Output: result (struct) A struct containing the results from the test
% ----------
function result = uniformityTest(imagePath)
%% Load slice #7
dcmInfo = dicominfo(imagePath{7});
dcmData = double(dicomread(dcmInfo));
result.originalImage = dcmData;

% Apply a 3x3 median filter to reduce the noise sensitivity
dcmDataFiltered = medfilt2(dcmData);

% Show the image
figure(1), clf, colormap(gray(255)), ax = axes;
imagesc(ax, dcmDataFiltered)
set(gca, 'DataAspectRatio', [1 1 1])

% Create a circular ROI. The ROI covers 65% of the phantom area
pos = placeCircularROI(dcmDataFiltered, sqrt(0.65));
result.ROIpos = pos;
ROI = imellipse(ax, pos);
ROImask = ROI.createMask; % createMask is a MATLAB built-in function

% Get the highest and lowest signal intensities from the ROI
maxSignal = max(dcmDataFiltered(ROImask));
minSignal = min(dcmDataFiltered(ROImask));
result.uniformity = 100*(1 - (maxSignal - minSignal)/(maxSignal + minSignal));
result.signalIntensities = [maxSignal minSignal];

% Check if the test passed the ACR's tolerance
IIU = result.uniformity; % Image intensity uniformity
if dcmInfo.MagneticFieldStrength < 3
   tol = [85 87.5];
else
   tol = [80 82];
end
if IIU < tol(1)
   fprintf('TEST FAILED. Uniformity < %1.1f%%: %1.1f%%.\n', tol(1), IIU)
elseif IIU < tol(2)
   fprintf('WARNING, uniformity < %1.1f%%: %1.1f%%.\n', tol(2), IIU)
else
   fprintf('Uniformity test PASSED\n')
end
end % uniformityTest