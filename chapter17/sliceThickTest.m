% ----------
% Function name: sliceThickTest.m
% Version: 1.0
% By Sven Mansson, 2018
% This function evaluates slice thickness using the ACR phantom.
%
% Input: imagePath (cell) A cell containing the paths to all of the images
% Output: result (struct) A struct containing the results from the IQ tests
% ----------
function result = sliceThickTest(imagePath)
%% Load slice #1
info = dicominfo(imagePath{1});
data = double(dicomread(info));
result.dateAndTime = [info.SeriesDate, ' ', info.SeriesTime(1:6)];
% Scale image between 0.0 and 1.0
dataScaled = data/max(data(:));
result.originalImage = dataScaled;

% Use the Hough transform to detect the angle of the thickness bars
% and create a rotation-corrected image.
% "verticalPosition" are the coarse y-positions of the
% upper and lower edges of the thickness bars (after rotation)
[rotationAngle, verticalPosition] = doHoughTransform(dataScaled);
dataRot = imrotate(dataScaled, rotationAngle, 'bilinear','crop');

% Cut out a horizontal stripe surrounding the thickness bars
margin = 6;
stripe = dataRot(min(verticalPosition)-margin: ...
                 max(verticalPosition)+margin, :);
result.rampImage = stripe;

% Average the rows of the stripe to get a vertical profile
verticalProfile = mean(stripe, 2);

% Cut away the bright areas above and below the dark bars
lowIntensityIndices = findBackgroundPoints(verticalProfile, 0);
stripe = stripe(lowIntensityIndices, :);

% Separate the upper from the lower bar and average the columns of each,
% with some upper and lower margins
height = size(stripe,1);
upperStripe = mean(stripe(2:floor(height/2)-1, :), 1);
lowerStripe = mean(stripe(ceil(height/2)+1:height-2, :), 1);

% Analyze each profile for slice thickness
dx = info.PixelSpacing(2);
upperThickness = calculateSliceThickness(upperStripe, dx);
lowerThickness = calculateSliceThickness(lowerStripe, dx);
result.sliceThickness = mean([upperThickness lowerThickness]);
result.barThicknesses = [upperThickness lowerThickness];

% Check if the test passed the ACR's tolerance
ST = result.sliceThickness;
if abs(ST - 5) > 1
   fprintf('TEST FAILED. Deviation > 1.0 mm: %1.2f mm.\n', ST)
elseif abs(ST - 5) > 0.7
   fprintf('WARNING, deviation > 0.7 mm: %1.2f mm.\n', ST)
else
   fprintf('Slice thickness test PASSED\n')
end
end % sliceThickTest