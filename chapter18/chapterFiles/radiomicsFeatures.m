function radiomicsFeatures(input, output)
    
    jAimTk = icr.etherj.aim.AimToolkit.getToolkit();
    jDcmTk = icr.etherj.dicom.DicomToolkit.getToolkit();

    jAimPs = jAimTk.createPathScan();
    jDcmPs = jDcmTk.createPathScan();

    jAimR  = icr.etherj.aim.AimReceiver();
    jDcmR  = icr.etherj.dicom.DicomReceiver();

    jAimPs.addContext(jAimR);

    jDcmPs.addContext(jDcmR);
    jDcmPs.scan(input, true);
    jPr = jDcmR.getPatientRoot();

    jiacList = jAimR.getImageAnnotationCollections();

    radItemList = ether.collect.CellArrayList('radiomics.RadItem');

    for i = 1:jiacList.size()
        % Note how everywhere we have a MATLAB Java object, we need to adjust
        % the loop variables to be zero indexed.jiac = jiacList.get(i-1);
        iac = ether.aim.ImageAnnotationCollection(jiac);
        jiaList = jiac.getAnnotationList();
        for j = 1:jiaList.size()
            jia = jiaList.get(j-1);
            ia = ether.aim.ImageAnnotation(jia);
            iaItem = radiomics.IaItem(ia, iac);
            radItemList.add(iaItem);
        end
    end

    jPatList = jPr.getPatientList();
    for p = 1:jPatList.size()
        jStudyList = jPr.getPatientList().get(p-1).getStudyList();
        for j = 1:jStudyList.size()
            jStudy = jStudyList.get(j-1);
            if strcmp(jStudy.getModality, 'RTSTRUCT')
                jSeriesList = jStudy.getSeriesList();

                for k = 1:jSeriesList.size()
                    jSeries  = jSeriesList.get(k-1);
                    jSopList = jSeries.getSopInstanceList();

                    for m = 1:jSopList.size()
                        jSopInst = jSopList.get(m-1);
                        jRts = jDcmTk.createRtStruct(jSopInst.getDicomObject());
                        rts  = ether.dicom.RtStruct(jRts);
                        jSsrList = ...
                            jRts.getStructureSetModule().getStructureSetRoiList();

                        for n = 1:jSsrList.size()
                            rtRoi = ether.dicom.RtRoi(jSsrList.get(n-1), rts); 
                            radItemList.add(radiomics.RtRoiItem(rtRoi, rts));
                        end                    
                    end
                end
            end
        end
    end

    dataSource = RadiomicsFeaturesDataSource(jPr);

    textureAnalyser = radiomics.TextureAnalyser3D();
    % The TextureAnalyser3D object is used in other contexts where explicit
    % setting of the project is needed. Here the data have all been
    % supplied in the /input directory already and this version of the
    % dataSource does not require a projectId to be passed.
    resultList = textureAnalyser.analyse(radItemList.toCellArray(),...
        dataSource, 'DUMMY_PROJECT');
    featureOutput = RadiomicsFeaturesOutputResults(resultList, output);
    featureOutput.sendToFile();
    
end
