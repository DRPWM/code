# Use Phusion's baseimage to get a proper init process, syslog and others
FROM phusion/baseimage:latest
 
# Use baseimage-docker's init system.
CMD ["/sbin/my_init"]
 
# Update everything
RUN apt-get update && apt-get upgrade -y -o Dpkg::Options::="--force-confold"
 
# Install needed packages
RUN apt-get install wget unzip
RUN apt-get update && apt-get install -y libxt6 xorg xvfb
 
# Set latest MATLAB version
ENV MATLAB_VERSION R2018b
 
# Pull and install MATLAB Compiler Runtime
RUN mkdir /mcr-install
RUN wget -P /mcr-install http://www.mathworks.com/supportfiles/downloads/\
${MATLAB_VERSION}/deployment_files/${MATLAB_VERSION}/installers/\
glnxa64/MCR_${MATLAB_VERSION}_glnxa64_installer.zip
RUN unzip -d /mcr-install /mcr-install/MCR_${MATLAB_VERSION}_glnxa64_installer.zip
RUN /mcr-install/install -mode silent -agreeToLicense yes
 
# Cleanup after installer
RUN rm -Rf /mcr-install
 
# Store the LD_LIBRARY_PATH required by MATLAB but don't set LD_LIBRARY_PATH
# as this breaks apt-get
ENV MATLAB_LD_LIBRARY_PATH /usr/local/MATLAB/MATLAB_Runtime/v95/runtime/\
glnxa64:/usr/local/MATLAB/MATLAB_Runtime/v95/bin/glnxa64:/usr/local/\
MATLAB/MATLAB_Runtime/v95/sys/os/glnxa64
 
# Clean up APT when done.
RUN apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*
