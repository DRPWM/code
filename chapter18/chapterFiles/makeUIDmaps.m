% Make maps for SOP instances and series that use the SOP instance UID
% and series instance UID as keys.

function [sopInstanceMap,seriesMap] = makeUIDmaps(jPatientList)

import icr.etherj.dicom.*

% Only include instances of MR image data by checking SOP class UID.
mrSopClassUid = '1.2.840.10008.5.1.4.1.1.4';

% Values in sopInstanceMap will be java objects of class 
% icr.etherj.dicom.impl.DefaultSopInstance
sopInstanceMap = containers.Map('KeyType','char','ValueType','any');

% Values in seriesMap will be structures with fields for the series 
% description and a map of the SOP instances in that series.
seriesMap = containers.Map('KeyType','char','ValueType','any');

% Nested loops to delve into DICOM hierarchy.
for iP = 1:jPatientList.size
    
    % Note the -1 because Java is zero-indexed.
    jPatient = jPatientList.get(iP-1);
    
    jStuList = jPatient.getStudyList();
    for iSt = 1:jStuList.size
        jStudy = jStuList.get(iSt-1);
        jSerList = jStudy.getSeriesList();
        for iSe = 1:jSerList.size
            jSeries = jSerList.get(iSe-1);
            jInstList = jSeries.getSopInstanceList();
            nInst = jInstList.size;
            
            % Initialise structure for this series.
            % map field will be populated in next loop.
            % Note use of char() to convert Java string to MATLAB
            % character array.
            seriesValue = ...
                struct('seriesDescription',char(jSeries.getDescription),...
                'map',containers.Map('KeyType','char','ValueType','any'));
            
            for iIn = 1:nInst
                jInst = jInstList.get(iIn-1);
                
                % Only include MR image data.
                if strcmp(char(jInst.getSopClassUid),mrSopClassUid)
                    seriesValue.map(char(jInst.getUid)) = jInst;
                    sopInstanceMap(char(jInst.getUid)) = jInst;
                end
                
            end
            
            % Put structure made to represent this series into seriesMap
            seriesMap(char(jSeries.getUid)) = seriesValue;
            
        end
    end
end

end