% Get series UIDs from map
seriesUIDs = seriesMap.keys;

% Get SOP instances for e.g. 3rd series
jInstMap = seriesMap(seriesUIDs{3}).map;
thisSeriesSopInstUIDs = jInstMap.keys;

% Get sizes from first SOP instance to initialise variables
jInstInit = jInstMap(thisSeriesSopInstUIDs{1});
nRows = jInstInit.getRowCount;
nCols = jInstInit.getColumnCount;
pixelData = zeros(nRows,nCols,jInstMap.Count);
instanceNumber = zeros(1,jInstMap.Count);

% Read pixel data and get instance number for all SOP Instances
for n = 1:jInstMap.Count
    jInst = jInstMap(thisSeriesSopInstUIDs{n});
    instanceNumber(n) = jInst.getInstanceNumber;
    pixelData(:,:,n) = getSliceData(jInst);
end

% Assume slices should be stacked in instance number order
[~,idx] = sort(instanceNumber);
pixelData = pixelData(:,:,idx);
