function jPatientList = scanPath(path)

import icr.etherj.dicom.*;

jScanner = DicomToolkit.getToolkit().createPathScan();
jReceiver = DicomReceiver();
jScanner.addContext(jReceiver);
searchSubfolders = false;
jScanner.scan(path, searchSubfolders);
jPatientList = jReceiver.getPatientRoot().getPatientList();

end
