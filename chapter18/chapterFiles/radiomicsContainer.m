function radiomicsContainer()
    
USE_MOCK = 0;
logger = ether.log4m.Logger.getLogger('radiomicsContainer');
    
logger.info('Starting radiomics analysis ...');
    
if USE_MOCK
    MOCK_INPUT  ='<DIR>/radiomics_MATLAB_dev/radiomics_container_mock_data/input';
    MOCK_OUTPUT ='<DIR>/radiomics_MATLAB_dev/radiomics_container_mock_data/output';
    radiomicsFeatures(MOCK_INPUT, MOCK_OUTPUT);
else
    radiomicsFeatures('/input/', '/output/');
end
    
logger.info('Radiomics analysis complete - container exiting.');

end
