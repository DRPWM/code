% Get pixel data and pixel positions for a SOP instance.

function [pixelData,pixelPos] = getSliceData(jInst)

% Import package to enable human readable DICOM Tag access.
import org.dcm4che2.data.Tag

% Access these metadata tags using EtherJ convenience functions.
nRows = jInst.getRowCount;
nCols = jInst.getColumnCount;
imPos = jInst.getImagePositionPatient;
imOri = jInst.getImageOrientationPatient;
pixSz = jInst.getPixelSpacing;

% Get DICOM Object for accessing other metadata tags.
jDcm = jInst.getDicomObject();

% Access these metadata tags using basic EtherJ functions and
% dcm4che Tag convenience function.
rsSlope = jDcm.getFloat(Tag.RescaleSlope);
rsInt = jDcm.getFloat(Tag.RescaleIntercept);

% Access pixel data using HEX field code.
pixelData = jDcm.getShorts(uint32(hex2dec('7fe00010')));

% Reshape and rescale (if appropriate) the image data.
pixelData = reshape(pixelData,nCols,nRows)';
if rsSlope == 0
    pixelData = double(pixelData);
else
    pixelData = rsSlope*double(pixelData) + rsInt;
end

% Calculate co-ordinates of every pixel - oblique slices are handled correctly.
T = [imOri(1:3)*pixSz(2) imOri(4:6)*pixSz(1) imPos];
[pixIdxRows,pixIdxCols] = ndgrid(0:nRows-1,0:nCols-1);
pixIdx = [pixIdxCols(:)'; pixIdxRows(:)'; ones(1,nRows*nCols)];
pixCoord = T*pixIdx;

% Reshape co-ordinates and append to output variable.
pixelPos.X = reshape(pixCoord(1,:),nRows,nCols);
pixelPos.Y = reshape(pixCoord(2,:),nRows,nCols);
pixelPos.Z = reshape(pixCoord(3,:),nRows,nCols);

end