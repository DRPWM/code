# Chapter 18: Image processing at scale by "containerizing" MATLAB
This folder contains code and supplementary materials relevant to Chapter 18. The are two subdirectories at the level of this README file:

**chapterFiles** - contains the MATLAB files and Docker files presented in the text of the book chapter

**radiomicsExample** - contains a complete example of containerizing a radiomics analysis in MATLAB using Docker (see the README file therein for in-depth instructions)

## Setup ##
A base install of MATLAB is required.

Required MATLAB toolboxes:  
- *MATLAB Compiler*  
- *Wavelet Toolbox*  
- *Image Processing Toolbox*  

Tested with MATLAB R2018b. The Docker scripts will need to be adapted for other MATLAB versions.

Additional requirements:  
- for the containerization, you will need to use a Linux system and install [Docker](https://www.docker.com/resources/what-container

## Contact ###
James d'Arcy  
James.Darcy@icr.ac.uk

## License ##
[MIT](https://choosealicense.com/licenses/mit/). See the license file in this folder (LICENSE.txt).
