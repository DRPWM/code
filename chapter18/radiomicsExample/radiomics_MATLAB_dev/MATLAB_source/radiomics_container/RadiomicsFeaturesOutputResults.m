classdef RadiomicsFeaturesOutputResults
    %UNTITLED Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        resultList
        outputPath
    end
    
    methods
        function this = RadiomicsFeaturesOutputResults(resultList, outputPath)
            %UNTITLED Construct an instance of this class
            %   Detailed explanation goes here
            this.resultList = resultList;
            this.outputPath = outputPath;
        
        end
        
        
        function sendToFile(this)
			for i=1:this.resultList.size()
                
				result = this.resultList.get(i);
                
                if isa(result.radItem, 'radiomics.IaItem')
                       this.iaOutputResults(result.results, result.radItem)
                elseif isa(result.radItem, 'radiomics.RtRoiItem')
                       this.rtRoiOutputResults(result.results, result.radItem)
                end 
                
			end
        end
        
        
    
        function iaOutputResults(this, results, IaItem)
			import radiomics.*;
        
			iac = iaItem.iac;
			patientName = iac.person.name;
			if isempty(patientName)
				patientName = iac.person.id;
			end
			if isempty(patientName)
				patientName = iac.uniqueIdentifier;
			end
			patDir = [this.outputPath,patientName,filesep()];
			if (exist(patDir, 'dir') == 0)
				mkdir(patDir);
			end
			lesionStr = '';
			if (~isempty(iaItem.scan))
				if (iaItem.lesionNumber > 0) && (iaItem.roiNumber > 0)
					lesionStr = sprintf('%s-%03i-%s-%03i', iaItem.personName, ...
						iaItem.lesionNumber, iaItem.scan, iaItem.roiNumber);
				else
					lesionStr = strjoin(strsplit(erase(iaItem.scan, '.'), ' '), '_');
				end
			end
			ia = iaItem.ia;
			dt = strjoin(strsplit(ia.dateTime, ':'), '-');
			fileName = [patDir,'AertsResults'];
			desc = strjoin(strsplit(iac.description, ' '), '_');
			if ~isempty(desc)
				fileName = [fileName,'_',desc];
			end
			if ~isempty(lesionStr)
				fileName = [fileName,'_',lesionStr];
			end
			fileName = [fileName,'_',dt,'_',ia.uniqueIdentifier,'.txt'];
			%this.logInfo(['Writing results to: ',fileName]);
			fileId = fopen(fileName, 'w');
			fprintf(fileId, 'PatientName: %s\n', patientName);
			fprintf(fileId, 'AnnotationCollectionDescription: %s\n', iac.description);
			fprintf(fileId, 'AnnotationName: %s\n', ia.name);
			fprintf(fileId, 'AnnotationUid: %s\n', ia.uniqueIdentifier);
			metrics = Aerts.getMetrics();
			prefix = {'', 'LLL.', 'LLH.', 'LHL.', 'LHH.', 'HLL.', 'HLH.', 'HHL.', 'HHH.'};
			for j=1:numel(prefix)
				for i=1:metrics.size()
					name = [prefix{j},metrics.get(i)];
					if ~results.isKey(name)
						continue;
					end
					fprintf(fileId, '%s: %f\n', name, results(name));
				end
			end
			fclose(fileId);
		end


		%-------------------------------------------------------------------------
		function rtRoiOutputResults(this, results, rtRoiItem)
			import radiomics.*;
        
			rts = rtRoiItem.rts;
			patientName = rts.getPatientName();
			if isempty(patientName)
				patientName = rts.getPatientId();
			end
			if isempty(patientName)
				patientName = rts.getSopInstanceUid();
			end
			patDir = [this.outputPath,filesep(),patientName,filesep()];
			if (exist(patDir, 'dir') == 0)
				mkdir(patDir);
			end
			lesionStr = '';
			if (~isempty(rtRoiItem.scan))
				if (rtRoiItem.lesionNumber > 0) && (rtRoiItem.roiNumber > 0)
					lesionStr = sprintf('%s-%03i-%s-%03i', patientName, ...
						rtRoiItem.lesionNumber, rtRoiItem.scan, rtRoiItem.roiNumber);
				else
					lesionStr = strjoin(strsplit(erase(rtRoiItem.scan, '.'), ' '), '_');
				end
			end
		   rtRoi = rtRoiItem.rtRoi;
			dt = strjoin(strsplit(rtRoiItem.dateTime, ':'), '-');
			fileName = [patDir,'AertsResults'];
			desc = strjoin(strsplit(rts.getDescription(), ' '), '_');
			if ~isempty(desc)
				fileName = [fileName,'_',desc];
			end
			if ~isempty(lesionStr)
				fileName = [fileName,'_',lesionStr];
			end
			fileName = [fileName,'_',dt,'_',rts.getSopInstanceUid(),'_',num2str(rtRoi.number),'.txt'];
			%this.logInfo(['Writing results to: ',fileName]);
			fileId = fopen(fileName, 'w');
			fprintf(fileId, 'PatientName: %s\n', patientName);
			fprintf(fileId, 'RT-STRUCT Description: %s\n', rts.getDescription());
			fprintf(fileId, 'ROI name: %s\n', rtRoi.name);
			fprintf(fileId, 'ROI UID: %s\n', [rts.getSopInstanceUid(),'_', num2str(rtRoi.number)]);
            fprintf(fileId, 'Series description: %s\n', rtRoiItem.seriesDescription);
			metrics = Aerts.getMetrics();
			prefix = {'', 'LLL.', 'LLH.', 'LHL.', 'LHH.', 'HLL.', 'HLH.', 'HHL.', 'HHH.'};
			for j=1:numel(prefix)
				for i=1:metrics.size()
					name = [prefix{j},metrics.get(i)];
					if ~results.isKey(name)
						continue;
					end
					fprintf(fileId, '%s: %f\n', name, results(name));
				end
			end
			fclose(fileId);
        end
    end

end

