classdef RadiomicsFeaturesDataSource < radiomics.DataSource
    %RADIOMICSFEATURESDATASOURCE Summary of this class goes here
    %   Detailed explanation goes here

    properties(Access=private)
        dcmTk = [];
        jpatientRoot = [];
        seriesMap = [];
    end

    %----------------------------------------------------------------------------
    methods
    %-------------------------------------------------------------------------
        function this = RadiomicsFeaturesDataSource(jPr)
            this.dcmTk = ether.dicom.Toolkit.getToolkit();
            this.jpatientRoot = jPr;
            this.seriesMap = this.mapSeries();
        end

        function series = getImageSeries(this, ~, uid, ~, varargin)
            if (this.jpatientRoot.getPatientCount() == 0)
               return;
            end
            series = this.seriesMap(uid);
        end

        function roiList = getRtStructList(~, ~, ~, varargin)
            roiList = [];
        end
    end
    
    %----------------------------------------------------------------------------
    methods(Access=private)
    %-------------------------------------------------------------------------
        function seriesMap = mapSeries(this)
            seriesMap = containers.Map('KeyType','char','ValueType','any');

            jpatientList = this.jpatientRoot.getPatientList();
            if (jpatientList.isEmpty())
                return;
            end

            for i = 1:jpatientList.size()
                jpatient = jpatientList.get(i-1);
                jstudyList = jpatient.getStudyList();
                for j = 1:jstudyList.size()
                    jstudy = jstudyList.get(j-1);
                    jseriesList = jstudy.getSeriesList();
                    for k = 1:jseriesList.size()
                        jseries = jseriesList.get(k-1);
                        seriesUid = char(jseries.getUid());
                        seriesMap(seriesUid) = this.dcmTk.createSeries(jseries);
                    end
                end
            end
        end

    end

    
    %----------------------------------------------------------------------------

end


