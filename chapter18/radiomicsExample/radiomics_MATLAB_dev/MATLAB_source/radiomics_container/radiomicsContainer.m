function radiomicsContainer()
    
USE_MOCK = 1;
logger = ether.log4m.Logger.getLogger('radiomicsContainer');
    
logger.info('Starting radiomics analysis ...');
    
if USE_MOCK
    MOCK_INPUT  ='/home/gavin/Projects/ch18/source_code_oct2020/radiomicsExample/radiomics_MATLAB_dev/radiomics_container_mock_data/input';
    MOCK_OUTPUT ='/home/gavin/Projects/ch18/source_code_oct2020/radiomicsExample/radiomics_MATLAB_dev/radiomics_container_mock_data/output';
    radiomicsFeatures(MOCK_INPUT, MOCK_OUTPUT);
else
    radiomicsFeatures('/input/', '/output/');
end
    
logger.info('Radiomics analysis complete - container exiting.');

end
