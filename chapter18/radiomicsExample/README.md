# MATLAB and Docker Code for MATLAB

by Simon Doran (edited by Gavin Poludniowski)

## 1. Introduction

Running the MATLAB code in test mode (i.e., not containerized) needs a
slightly different environment from the container version. This is
primarily because it is often not straightforward for a regular user to
create a "/input" and "/output" directory at root level, whereas that is
where the Docker container will expect to read and write data.

Hence what is supplied is code and a test dataset for running the
software in development mode and then, separately, the containerized
version. Obviously, the development environment is necessary if one
wishes to make changes to the container and test them.

## 2. Development code

The following code is provided in the directory "radiomics\_MATLAB\_dev"

-   "MATLAB\_source" directory containing all the MATLAB code written by us needed to run
    the radiomics feature generation application described in the book
    chapter. There are three aspects to this:

    -   "radiomics\_container": the top level calling routines required to containerize the app
        and a number of these are referred to directly in the book
        chapter

    -   "radiomics-matlab": the radiomics calculations and associated code

    -   "ether-matlab": a library written by James d'Arcy, containing a number of useful
        DICOM utility functions

-   "MATLAB\_user\_java\_libs" directory. As described in the book chapter, our code integrates Java into XNAT and these libraries are required to run that code:

    -   "etherj-core.jar": the main toolkit, as referred to in the text

    -   "EtherJ-XNAT.jar": the extensions that allow us to use XNAT as a data source for
        MATLAB

    -   "user\_lib\_dependencies": third-party code (all open-source) required for basic DICOM
        functionality

-   "radiomics\_container\_mock\_data" directory.
    Sample data and directories mimicking "/input" and "/output" in the
    final container:

    -   "input"

        -   "case001": one of the XNAT distribution's sample datasets, with
            annotations created in the XNAT viewer by us

    -   "output"

        -   "case001": when performing a run of the software, this would normally
            start out empty and files would be created by the software.
            In this case, we provide the final result of running the
            radiomics software on the data in "/input"

-   ".ether": a hidden directory for the configuration file and Ether's logging
    output

    -   "log4m.xml": version of the logging configuration file for use in development
        mode

    -   "ether.log": sample logging output of the program when run on the data
        supplied

## 3. Running the development code

-   Place the directory called "radiomics\_MATLAB\_dev" in a chosen location. 
    The full path of this directory will be denoted by <DIR\> in what follows.

-   Copy the ".ether" hidden directory to your user home directory i.e.
    "/home/<your_username\>/.ether" (if you don't do this the files within it won't
    be found).

-   Locate the "javaclasspath.txt" in your MATLAB preferences directory.
    You can find the location of the preferences directory by typing
    the command `prefdir` at the MATLAB command prompt.

-   Edit the "javaclasspath.txt" file to add the following lines, in each
    case, replacing <DIR\> with the full path of this top level
    directory.

    `<DIR>/MATLAB_user_java_libs/user_lib_dependencies/dcm4che-audit-2.0.29.jar`  
    `<DIR>/MATLAB_user_java_libs/user_lib_dependencies/dcm4che-base64-2.0.29.jar`  
    `<DIR>/MATLAB_user_java_libs/user_lib_dependencies/dcm4che-core-2.0.29.jar`  
    `<DIR>/MATLAB_user_java_libs/user_lib_dependencies/dcm4che-filecache-2.0.29.jar`  
    `<DIR>/MATLAB_user_java_libs/user_lib_dependencies/dcm4che-hp-2.0.29.jar`  
    `<DIR>/MATLAB_user_java_libs/user_lib_dependencies/dcm4che-image-2.0.29.jar`  
    `<DIR>/MATLAB_user_java_libs/user_lib_dependencies/dcm4che-imageio-2.0.29.jar`  
    `<DIR>/MATLAB_user_java_libs/user_lib_dependencies/dcm4che-imageio-rle-2.0.29.jar`  
    `<DIR>/MATLAB_user_java_libs/user_lib_dependencies/dcm4che-iod-2.0.29.jar`  
    `<DIR>/MATLAB_user_java_libs/user_lib_dependencies/dcm4che-net-2.0.29.jar`  
    `<DIR>/MATLAB_user_java_libs/user_lib_dependencies/dcm4che-soundex-2.0.29.jar`  
    `<DIR>/MATLAB_user_java_libs/etherj-core.jar`  
    `<DIR>/MATLAB_user_java_libs/EtherJ-XNAT.jar`  

-   Now shut down MATLAB and restart to read in these new jar files. You
    can verify that the jar files are now on the static java class path
    by entering the following in the MATLAB command prompt:

    `>> javaclasspath('-static')`

-   You now need to let the software know where you have put the data.
    When run in development mode, we use a mock input and you need to
    specify where this is located on your system. Edit the file
    "radiomicsContainer.m" in the "MATLAB_source/radiomics_container"
    directory by changing the lines 9 and 10 to read:
    
    `MOCK_INPUT  = '<DIR>/radiomics_container_mock_data/input';`  
    `MOCK_OUTPUT = '<DIR>/radiomics_container_mock_data/output';`
    
    where, once again, <DIR\> is replaced with the value on your
    system.

-   Things should now be ready to run, so click on the `Run` button on the
    file "radiomicsContainer.m". If all goes well, something resembling
    the following transcript should appear as part of the output in the
    console window and be mirrored to the log file in ".ether/ether.log".
    If you delete the example output file, it should be regenerated in
    the "radiomics\_container\_mock\_data/output directory".

Log4M: INFO 6575 \[Log4M.m](Log4M.readConfig:190) - Configuration file
read - /home/gavin/.ether/log4m.xml  
Log4M: INFO 6575 \[Log4M.m](Log4M.parseLogger:248) - \* Logger: root
(debug)  
Log4M: INFO 6575 \[Log4M.m](Log4M.parseAppender:206) - \* Appender:
ether.log4m.ConsoleAppender  
Log4M: INFO 6575 \[Log4M.m](Log4M.parseAppender:206) - \* Appender:
ether.log4m.RollingFileAppender  
Log4M: INFO 6575 \[Log4M.m](Log4M.parseAppender:215) - \* Key: File
Value: .ether/ether.log  
Log4M: INFO 6575 \[Log4M.m](Log4M.parseAppender:215) - \* Key:
MaxFileSize Value: 1M  
Log4M: INFO 6575 \[Log4M.m](Log4M.parseAppender:215) - \* Key:
MaxRetainIndex Value: 9  
Log4M: DEBUG 6575 \[LogManager.m](LogManager.LogManager:80) -
LogManager initialising\...  
Log4M: DEBUG 6575 \[RootLogger.m](RootLogger.RootLogger:22) -
RootLogger level: DEBUG  
Log4M: DEBUG 6575 \[Logger.m](Logger.set.level:65) - Logger level:
DEBUG  
Log4M: DEBUG 6575 \[LogManager.m](LogManager.configureRepo:92) -
LogManager::configureRepo(ether)  
Log4M: DEBUG 6575 \[LoggerRepository.m](LoggerRepository.configure:32)
- Configuring repository  
Log4M: DEBUG 6575 \[Logger.m](Logger.set.level:65) - Logger level:
DEBUG  
Log4M: DEBUG 6575 \[Logger.m](Logger.addAppender:76) - Adding
appender(Console) to logger(root)  
Log4M: DEBUG 6575 \[RollingFileAppender.m](RollingFileAppender.RollingFileAppender:37) -
RollingFileAppender: /home/gavin/.ether/ether.log  
Log4M: DEBUG 6575 \[LoggerRepository.m](LoggerRepository.createRollingFileAppender:156) -
MaxFileSize: 1M  
Log4M: DEBUG 6575 \[Logger.m](Logger.addAppender:76) - Adding
appender(RollingFile) to logger(root)  
Log4M: DEBUG 6575 \[LoggerRepository.m](LoggerRepository.getNamedLogger:173) - Requested:
radiomicsContainer  
Log4M: DEBUG 6575 \[LoggerRepository.m](LoggerRepository.updateParents:244) - Updating
parents for radiomicsContainer  
Log4M: DEBUG 6575 \[LoggerRepository.m](LoggerRepository.updateParents:275) - Logger
radiomicsContainer has parent root  
09-Oct-2020 16:11:10.524 INFO 6575 \[radiomicsContainer.m](radiomicsContainer:6) - Starting radiomics
analysis \...  
log4j:WARN No appenders could be found for logger
(icr.etherj.AbstractPathScan).  
log4j:WARN Please initialize the log4j system properly.
Log4M: DEBUG 6575 \[LoggerRepository.m](LoggerRepository.getNamedLogger:173) - Requested:
ether.dicom.Toolkit  
Log4M: DEBUG 6575 \[LoggerRepository.m](LoggerRepository.updateParents:244) - Updating
parents for ether.dicom.Toolkit  
Log4M: DEBUG 6575 \[LoggerRepository.m](LoggerRepository.updateParents:247) - Searching
repository for ether.dicom  
Log4M: DEBUG 6575 \[LoggerRepository.m](LoggerRepository.updateParents:249) - No parent
ether.dicom found, creating LoggerNode  
Log4M: DEBUG 6575 \[LoggerRepository.m](LoggerRepository.updateParents:247) - Searching
repository for ether  
Log4M: DEBUG 6575 \[LoggerRepository.m](LoggerRepository.updateParents:249) - No parent
ether found, creating LoggerNode  
Log4M: DEBUG 6575 \[LoggerRepository.m](LoggerRepository.updateParents:275) - Logger
ether.dicom.Toolkit has parent root  
Log4M: DEBUG 6575 \[LoggerRepository.m](LoggerRepository.getNamedLogger:173) - Requested:
ether.dicom.SopInstance  
Log4M: DEBUG 6575 \[LoggerRepository.m](LoggerRepository.updateParents:244) - Updating
parents for ether.dicom.SopInstance  
Log4M: DEBUG 6575 \[LoggerRepository.m](LoggerRepository.updateParents:247) - Searching
repository for ether.dicom  
Log4M: DEBUG 6575 \[LoggerRepository.m](LoggerRepository.updateParents:275) - Logger
ether.dicom.SopInstance has parent root  
Log4M: DEBUG 6575 \[LoggerRepository.m](LoggerRepository.getNamedLogger:173) - Requested:
ether.dicom.JavaSopInstance  
Log4M: DEBUG 6575 \[LoggerRepository.m](LoggerRepository.updateParents:244) - Updating
parents for ether.dicom.JavaSopInstance  
Log4M: DEBUG 6575 \[LoggerRepository.m](LoggerRepository.updateParents:247) - Searching
repository for ether.dicom  
Log4M: DEBUG 6575 \[LoggerRepository.m](LoggerRepository.updateParents:275) - Logger
ether.dicom.JavaSopInstance has parent root  
Log4M: DEBUG 6575 \[LoggerRepository.m](LoggerRepository.getNamedLogger:173) - Requested:
ether.dicom.Image  
Log4M: DEBUG 6575 \[LoggerRepository.m](LoggerRepository.updateParents:244) - Updating
parents for ether.dicom.Image  
Log4M: DEBUG 6575 \[LoggerRepository.m](LoggerRepository.updateParents:247) - Searching
repository for ether.dicom  
Log4M: DEBUG 6575 \[LoggerRepository.m](LoggerRepository.updateParents:275) - Logger
ether.dicom.Image has parent root  
Log4M: DEBUG 6575 \[LoggerRepository.m](LoggerRepository.getNamedLogger:173) - Requested:
ether.dicom.MRImage  
Log4M: DEBUG 6575 \[LoggerRepository.m](LoggerRepository.updateParents:244) - Updating
parents for ether.dicom.MRImage  
Log4M: DEBUG 6575 \[LoggerRepository.m](LoggerRepository.updateParents:247) - Searching
repository for ether.dicom  
Log4M: DEBUG 6575 \[LoggerRepository.m](LoggerRepository.updateParents:275) - Logger
ether.dicom.MRImage has parent root  
Log4M: DEBUG 6575 \[LoggerRepository.m](LoggerRepository.getNamedLogger:173) - Requested:
radiomics.TextureAnalyser3D  
Log4M: DEBUG 6575 \[LoggerRepository.m](LoggerRepository.updateParents:244) - Updating
parents for radiomics.TextureAnalyser3D  
Log4M: DEBUG 6575 \[LoggerRepository.m](LoggerRepository.updateParents:247) - Searching
repository for radiomics  
Log4M: DEBUG 6575 \[LoggerRepository.m](LoggerRepository.updateParents:249) - No parent
radiomics found, creating LoggerNode  
Log4M: DEBUG 6575 \[LoggerRepository.m](LoggerRepository.updateParents:275) - Logger
radiomics.TextureAnalyser3D has parent root  
09-Oct-2020 16:11:13.116 INFO 6575 \[TextureAnalyser3D.m](TextureAnalyser3D.analyse:23) - Fetching
referenced series  
09-Oct-2020 16:11:13.129 INFO 6575 \[TextureAnalyser3D.m](TextureAnalyser3D.buildRefSeriesMap:54) -
Fetching referenced series  
09-Oct-2020 16:11:13.883 INFO 6575 \[TextureAnalyser3D.m](TextureAnalyser3D.insertSeries:228) - Series loaded: 1.2.840.113654.2.45.2.109935  
09-Oct-2020 16:11:13.889 INFO 6575
\[TextureAnalyser3D.m](TextureAnalyser3D.buildRefSeriesMap:76) -
Series: 2 - T1 SPIN ECHO (1.2.840.113654.2.45.2.109935)  
09-Oct-2020 16:11:13.909 INFO 6575
\[TextureAnalyser3D.m](TextureAnalyser3D.buildRefSeriesMap:86) - 1
referenced series loaded  
09-Oct-2020 16:11:13.912 INFO 6575
\[TextureAnalyser3D.m](TextureAnalyser3D.analyse:27) - Processing
annotation (1 of 1)  
09-Oct-2020 16:11:13.914 INFO 6575
\[TextureAnalyser3D.m](TextureAnalyser3D.processRtRoiItem:290) -
Processing Patient case001, RT-STRUCT: Gavin\_test  
09-Oct-2020 16:11:19.820 INFO 6575
\[TextureAnalyser3D.m](TextureAnalyser3D.processWavelet:322) -
Processing wavelet decomposition: LLL  
09-Oct-2020 16:11:19.910 INFO 6575
\[TextureAnalyser3D.m](TextureAnalyser3D.processWavelet:322) -
Processing wavelet decomposition: LLH  
09-Oct-2020 16:11:19.976 INFO 6575
\[TextureAnalyser3D.m](TextureAnalyser3D.processWavelet:322) -
Processing wavelet decomposition: LHL  
09-Oct-2020 16:11:20.049 INFO 6575
\[TextureAnalyser3D.m](TextureAnalyser3D.processWavelet:322) -
Processing wavelet decomposition: LHH  
09-Oct-2020 16:11:20.150 INFO 6575
\[TextureAnalyser3D.m](TextureAnalyser3D.processWavelet:322) -
Processing wavelet decomposition: HLL  
09-Oct-2020 16:11:20.208 INFO 6575
\[TextureAnalyser3D.m](TextureAnalyser3D.processWavelet:322) -
Processing wavelet decomposition: HLH  
09-Oct-2020 16:11:20.259 INFO 6575
\[TextureAnalyser3D.m](TextureAnalyser3D.processWavelet:322) -
Processing wavelet decomposition: HHL  
09-Oct-2020 16:11:20.323 INFO 6575
\[TextureAnalyser3D.m](TextureAnalyser3D.processWavelet:322) -
Processing wavelet decomposition: HHH  
09-Oct-2020 16:11:20.650 INFO 6575
\[radiomicsContainer.m](radiomicsContainer:16) - Radiomics analysis
complete - container exiting.  
\>\>

## 4. Creating and testing the MATLAB executable

-   Type `which mcc`  to verify that the MATLAB compilation command is on
    your PATH (mine is in /usr/local/bin). If not, locate the necessary directory
    with the MATLAB command line tools and add it to PATH.

    Type the following commands:

    `cd <DIR>/MATLAB_source/radiomics_container`  
    `mcc -m radiomicsContainer.m -a ../ether-matlab/ -a ../radiomics-matlab -a ../../MATLAB_user_java_libs/`

    This can be done either in a Linux console or in the MATLAB command window, as long as the MATLAB compiler can be found on the search path. If all goes well, you will see output resembling the following from
the mcc command (there may be additional warning messages):

Log4M: INFO 8350 - Configuration file read - /home/gavin/.ether/log4m.xml  
Log4M: INFO 8350 - * Logger: root (debug)  
Log4M: INFO 8350 -   * Appender: ether.log4m.ConsoleAppender  
Log4M: INFO 8350 -   * Appender: ether.log4m.RollingFileAppender  
Log4M: INFO 8350 -     * Key: File Value: .ether/ether.log  
Log4M: INFO 8350 -     * Key: MaxFileSize Value: 1M  
Log4M: INFO 8350 -     * Key: MaxRetainIndex Value: 9  
Log4M: DEBUG 8350 - LogManager initialising...  
Log4M: DEBUG 8350 - RootLogger level: DEBUG  
Log4M: DEBUG 8350 - Logger level: DEBUG  
Log4M: DEBUG 8350 - LogManager::configureRepo(ether)  
Log4M: DEBUG 8350 - Configuring repository  
Log4M: DEBUG 8350 - Logger level: DEBUG  
Log4M: DEBUG 8350 - Adding appender(Console) to logger(root)  
Log4M: DEBUG 8350 - RollingFileAppender: /home/gavin/.ether/ether.log  
Log4M: DEBUG 8350 - MaxFileSize: 1M  
Log4M: DEBUG 8350 - Adding appender(RollingFile) to logger(root)  
Log4M: DEBUG 8350 - Requested: ether.app.AbstractApplication  
Log4M: DEBUG 8350 - Updating parents for ether.app.AbstractApplication  
Log4M: DEBUG 8350 - Searching repository for ether.app  
Log4M: DEBUG 8350 - No parent ether.app found, creating LoggerNode  
Log4M: DEBUG 8350 - Searching repository for ether  
Log4M: DEBUG 8350 - No parent ether found, creating LoggerNode  
Log4M: DEBUG 8350 - Logger ether.app.AbstractApplication has parent root  
Log4M: DEBUG 8350 - Requested: ether.dicom.Image  
Log4M: DEBUG 8350 - Updating parents for ether.dicom.Image  
Log4M: DEBUG 8350 - Searching repository for ether.dicom  
Log4M: DEBUG 8350 - No parent ether.dicom found, creating LoggerNode  
Log4M: DEBUG 8350 - Searching repository for ether  
Log4M: DEBUG 8350 - Logger ether.dicom.Image has parent root  
Log4M: DEBUG 8350 - Requested: ether.dicom.MRImage  
Log4M: DEBUG 8350 - Updating parents for ether.dicom.MRImage  
Log4M: DEBUG 8350 - Searching repository for ether.dicom  
Log4M: DEBUG 8350 - Logger ether.dicom.MRImage has parent root  
Log4M: DEBUG 8350 - Requested: ether.dicom.SopInstance  
Log4M: DEBUG 8350 - Updating parents for ether.dicom.SopInstance  
Log4M: DEBUG 8350 - Searching repository for ether.dicom  
Log4M: DEBUG 8350 - Logger ether.dicom.SopInstance has parent root  
Log4M: DEBUG 8350 - Requested: ether.optim.AbstractSolver  
Log4M: DEBUG 8350 - Updating parents for ether.optim.AbstractSolver  
Log4M: DEBUG 8350 - Searching repository for ether.optim  
Log4M: DEBUG 8350 - No parent ether.optim found, creating LoggerNode  
Log4M: DEBUG 8350 - Searching repository for ether  
Log4M: DEBUG 8350 - Logger ether.optim.AbstractSolver has parent root  
Log4M: DEBUG 8350 - Requested: ether.dicom.Toolkit  
Log4M: DEBUG 8350 - Updating parents for ether.dicom.Toolkit  
Log4M: DEBUG 8350 - Searching repository for ether.dicom  
Log4M: DEBUG 8350 - Logger ether.dicom.Toolkit has parent root  
Log4M: DEBUG 8350 - Requested: ether.process.Toolkit  
Log4M: DEBUG 8350 - Updating parents for ether.process.Toolkit  
Log4M: DEBUG 8350 - Searching repository for ether.process  
Log4M: DEBUG 8350 - No parent ether.process found, creating LoggerNode  
Log4M: DEBUG 8350 - Searching repository for ether  
Log4M: DEBUG 8350 - Logger ether.process.Toolkit has parent root  
Log4M: DEBUG 8350 - Requested: ether.parallel.Pool  
Log4M: DEBUG 8350 - Updating parents for ether.parallel.Pool  
Log4M: DEBUG 8350 - Searching repository for ether.parallel  
Log4M: DEBUG 8350 - No parent ether.parallel found, creating LoggerNode  
Log4M: DEBUG 8350 - Searching repository for ether  
Log4M: DEBUG 8350 - Logger ether.parallel.Pool has parent root  
Log4M: DEBUG 8350 - Requested: ether.dicom.EnhancedMRImage  
Log4M: DEBUG 8350 - Updating parents for ether.dicom.EnhancedMRImage  
Log4M: DEBUG 8350 - Searching repository for ether.dicom  
Log4M: DEBUG 8350 - Logger ether.dicom.EnhancedMRImage has parent root  
Log4M: DEBUG 8350 - LogManager deleted for PID=8350  
Log4M: DEBUG 8350 - LogManager::shutdown()  
Log4M: DEBUG 8350 - LoggerRepository::shutdown()  
Log4M: DEBUG 8350 - Closing 2 appenders for root

and the executable file "radiomicsContainer" will have been (re)created,
    along with the script file "run_radiomicsContainer.sh" and several other
    files ("mccExcludedFiles.log", "readme.txt" and "requiredMCRProducts.txt")
    that we will not use.

-   To demonstrate that the executable just generated produces the same
    output as obtained from the interactive MATLAB session in Section 3,
    you will need to locate your MATLAB installation directory. This
    will be the path that terminates with "R20XXY" e.g.
    "/home/<your_username\>/MATLAB/R2018b".

    For historical reasons, my MATLAB is installed under my home directory in "~/MATLAB_installation", but the equivalent file might typically be found somewhere under "/usr/local".

    From the directory containing the "run_radiomicsContainer.sh" script,
    type the following in a Linux console:

    `sh ./run_radiomicsContainer.sh <YOUR_MATLAB>`

    where `<YOUR_MATLAB>` should be replaced with the appropriate full path of the installation directory. All being well, MATLAB will respond with something like:

\------------------------------------------  
Setting up environment variables  
\---  
LD_LIBRARY_PATH is .:/home/gavin/Software/MATLAB/R2018b/runtime/glnxa64:/home/gavin/Software/MATLAB/R2018b/bin/glnxa64:/home/gavin/Software/MATLAB/R2018b/sys/os/glnxa64:/home/gavin/Software/MATLAB/R2018b/sys/opengl/lib/glnxa64  
Log4M: INFO 8576 \[Log4M.m](Log4M.readConfig:190) - Configuration file read - /home/gavin/.ether/log4m.xml  
Log4M: INFO 8576 \[Log4M.m](Log4M.parseLogger:248) - * Logger: root (debug)  
Log4M: INFO 8576 \[Log4M.m](Log4M.parseAppender:206) -   * Appender: ether.log4m.ConsoleAppender  
Log4M: INFO 8576 \[Log4M.m](Log4M.parseAppender:206) -   * Appender: ether.log4m.RollingFileAppender  
Log4M: INFO 8576 \[Log4M.m](Log4M.parseAppender:215) -     * Key: File Value: .ether/ether.log  
Log4M: INFO 8576 \[Log4M.m](Log4M.parseAppender:215) -     * Key: MaxFileSize Value: 1M  
Log4M: INFO 8576 \[Log4M.m](Log4M.parseAppender:215) -     * Key: MaxRetainIndex Value: 9  
Log4M: DEBUG 8576 \[LogManager.m](LogManager.LogManager:80) - LogManager initialising...  
Log4M: DEBUG 8576 \[RootLogger.m](RootLogger.RootLogger:22) - RootLogger level: DEBUG  
Log4M: DEBUG 8576 \[Logger.m](Logger.set.level:65) - Logger level: DEBUG  
Log4M: DEBUG 8576 \[LogManager.m](LogManager.configureRepo:92) - LogManager::configureRepo(ether)  
Log4M: DEBUG 8576 \[LoggerRepository.m](LoggerRepository.configure:32) - Configuring repository  
Log4M: DEBUG 8576 \[Logger.m](Logger.set.level:65) - Logger level: DEBUG  
Log4M: DEBUG 8576 \[Logger.m](Logger.addAppender:76) - Adding appender(Console) to logger(root)  
Log4M: DEBUG 8576 \[RollingFileAppender.m](RollingFileAppender.RollingFileAppender:37) - RollingFileAppender: /home/gavin/.ether/ether.log  
Log4M: DEBUG 8576 \[LoggerRepository.m](LoggerRepository.createRollingFileAppender:156) - MaxFileSize: 1M  
Log4M: DEBUG 8576 \[Logger.m](Logger.addAppender:76) - Adding appender(RollingFile) to logger(root)  
Log4M: DEBUG 8576 \[LoggerRepository.m](LoggerRepository.getNamedLogger:173) - Requested: radiomicsContainer  
Log4M: DEBUG 8576 \[LoggerRepository.m](LoggerRepository.updateParents:244) - Updating parents for radiomicsContainer  
Log4M: DEBUG 8576 \[LoggerRepository.m](LoggerRepository.updateParents:275) - Logger radiomicsContainer has parent root  
09-Oct-2020 16:32:42.651 INFO 8576 \[radiomicsContainer.m](radiomicsContainer:6) - Starting radiomics analysis ...  
log4j:WARN No appenders could be found for logger (icr.etherj.AbstractPathScan).  
log4j:WARN Please initialize the log4j system properly.  
Log4M: DEBUG 8576 \[LoggerRepository.m](LoggerRepository.getNamedLogger:173) - Requested: ether.dicom.Toolkit  
Log4M: DEBUG 8576 \[LoggerRepository.m](LoggerRepository.updateParents:244) - Updating parents for ether.dicom.Toolkit  
Log4M: DEBUG 8576 \[LoggerRepository.m](LoggerRepository.updateParents:247) - Searching repository for ether.dicom  
Log4M: DEBUG 8576 \[LoggerRepository.m](LoggerRepository.updateParents:249) - No parent ether.dicom found, creating LoggerNode  
Log4M: DEBUG 8576 \[LoggerRepository.m](LoggerRepository.updateParents:247) - Searching repository for ether  
Log4M: DEBUG 8576 \[LoggerRepository.m](LoggerRepository.updateParents:249) - No parent ether found, creating LoggerNode  
Log4M: DEBUG 8576 \[LoggerRepository.m](LoggerRepository.updateParents:275) - Logger ether.dicom.Toolkit has parent root  
Log4M: DEBUG 8576 \[LoggerRepository.m](LoggerRepository.getNamedLogger:173) - Requested: ether.dicom.SopInstance  
Log4M: DEBUG 8576 \[LoggerRepository.m](LoggerRepository.updateParents:244) - Updating parents for ether.dicom.SopInstance  
Log4M: DEBUG 8576 \[LoggerRepository.m](LoggerRepository.updateParents:247) - Searching repository for ether.dicom  
Log4M: DEBUG 8576 \[LoggerRepository.m](LoggerRepository.updateParents:275) - Logger ether.dicom.SopInstance has parent root  
Log4M: DEBUG 8576 \[LoggerRepository.m](LoggerRepository.getNamedLogger:173) - Requested: ether.dicom.JavaSopInstance  
Log4M: DEBUG 8576 \[LoggerRepository.m](LoggerRepository.updateParents:244) - Updating parents for ether.dicom.JavaSopInstance  
Log4M: DEBUG 8576 \[LoggerRepository.m](LoggerRepository.updateParents:247) - Searching repository for ether.dicom  
Log4M: DEBUG 8576 \[LoggerRepository.m](LoggerRepository.updateParents:275) - Logger ether.dicom.JavaSopInstance has parent root  
Log4M: DEBUG 8576 \[LoggerRepository.m](LoggerRepository.getNamedLogger:173) - Requested: ether.dicom.Image  
Log4M: DEBUG 8576 \[LoggerRepository.m](LoggerRepository.updateParents:244) - Updating parents for ether.dicom.Image  
Log4M: DEBUG 8576 \[LoggerRepository.m](LoggerRepository.updateParents:247) - Searching repository for ether.dicom  
Log4M: DEBUG 8576 \[LoggerRepository.m](LoggerRepository.updateParents:275) - Logger ether.dicom.Image has parent root  
Log4M: DEBUG 8576 \[LoggerRepository.m](LoggerRepository.getNamedLogger:173) - Requested: ether.dicom.MRImage  
Log4M: DEBUG 8576 \[LoggerRepository.m](LoggerRepository.updateParents:244) - Updating parents for ether.dicom.MRImage  
Log4M: DEBUG 8576 \[LoggerRepository.m](LoggerRepository.updateParents:247) - Searching repository for ether.dicom  
Log4M: DEBUG 8576 \[LoggerRepository.m](LoggerRepository.updateParents:275) - Logger ether.dicom.MRImage has parent root  
Log4M: DEBUG 8576 \[LoggerRepository.m](LoggerRepository.getNamedLogger:173) - Requested: radiomics.TextureAnalyser3D  
Log4M: DEBUG 8576 \[LoggerRepository.m](LoggerRepository.updateParents:244) - Updating parents for radiomics.TextureAnalyser3D  
Log4M: DEBUG 8576 \[LoggerRepository.m](LoggerRepository.updateParents:247) - Searching repository for radiomics  
Log4M: DEBUG 8576 \[LoggerRepository.m](LoggerRepository.updateParents:249) - No parent radiomics found, creating LoggerNode  
Log4M: DEBUG 8576 \[LoggerRepository.m](LoggerRepository.updateParents:275) - Logger radiomics.TextureAnalyser3D has parent root  
09-Oct-2020 16:32:43.719 INFO 8576 \[TextureAnalyser3D.m](TextureAnalyser3D.analyse:23) - Fetching referenced series  
09-Oct-2020 16:32:43.729 INFO 8576 \[TextureAnalyser3D.m](TextureAnalyser3D.buildRefSeriesMap:54) - Fetching referenced series  
09-Oct-2020 16:32:44.098 INFO 8576 \[TextureAnalyser3D.m](TextureAnalyser3D.insertSeries:228) - Series loaded: 1.2.840.113654.2.45.2.109935  
09-Oct-2020 16:32:44.103 INFO 8576 \[TextureAnalyser3D.m](TextureAnalyser3D.buildRefSeriesMap:76) - Series: 2 - T1 SPIN ECHO (1.2.840.113654.2.45.2.109935)  
09-Oct-2020 16:32:44.126 INFO 8576 \[TextureAnalyser3D.m](TextureAnalyser3D.buildRefSeriesMap:86) - 1 referenced series loaded  
09-Oct-2020 16:32:44.128 INFO 8576 \[TextureAnalyser3D.m](TextureAnalyser3D.analyse:27) - Processing annotation (1 of 1)  
09-Oct-2020 16:32:44.129 INFO 8576 \[TextureAnalyser3D.m](TextureAnalyser3D.processRtRoiItem:290) - Processing Patient case001, RT-STRUCT: Gavin_test  
09-Oct-2020 16:32:47.840 INFO 8576 \[TextureAnalyser3D.m](TextureAnalyser3D.processWavelet:322) - Processing wavelet decomposition: LLL  
09-Oct-2020 16:32:47.934 INFO 8576 \[TextureAnalyser3D.m](TextureAnalyser3D.processWavelet:322) - Processing wavelet decomposition: LLH  
09-Oct-2020 16:32:47.999 INFO 8576 \[TextureAnalyser3D.m](TextureAnalyser3D.processWavelet:322) - Processing wavelet decomposition: LHL  
09-Oct-2020 16:32:48.076 INFO 8576 \[TextureAnalyser3D.m](TextureAnalyser3D.processWavelet:322) - Processing wavelet decomposition: LHH  
09-Oct-2020 16:32:48.171 INFO 8576 \[TextureAnalyser3D.m](TextureAnalyser3D.processWavelet:322) - Processing wavelet decomposition: HLL  
09-Oct-2020 16:32:48.232 INFO 8576 \[TextureAnalyser3D.m](TextureAnalyser3D.processWavelet:322) - Processing wavelet decomposition: HLH  
09-Oct-2020 16:32:48.290 INFO 8576 \[TextureAnalyser3D.m](TextureAnalyser3D.processWavelet:322) - Processing wavelet decomposition: HHL  
09-Oct-2020 16:32:48.357 INFO 8576 \[TextureAnalyser3D.m](TextureAnalyser3D.processWavelet:322) - Processing wavelet decomposition: HHH  
09-Oct-2020 16:32:48.555 INFO 8576 \[radiomicsContainer.m](radiomicsContainer:16) - Radiomics analysis complete - container exiting.  
Log4M: INFO 8576 \[Log4M.m](Log4M.readConfig:190) - Configuration file read - /home/gavin/.ether/log4m.xml  
Log4M: INFO 8576 \[Log4M.m](Log4M.parseLogger:248) - * Logger: root (debug)  
Log4M: INFO 8576 \[Log4M.m](Log4M.parseAppender:206) -   * Appender: ether.log4m.ConsoleAppender  
Log4M: INFO 8576 \[Log4M.m](Log4M.parseAppender:206) -   * Appender: ether.log4m.RollingFileAppender  
Log4M: INFO 8576 \[Log4M.m](Log4M.parseAppender:215) -     * Key: File Value: .ether/ether.log  
Log4M: INFO 8576 \[Log4M.m](Log4M.parseAppender:215) -     * Key: MaxFileSize Value: 1M  
Log4M: INFO 8576 \[Log4M.m](Log4M.parseAppender:215) -     * Key: MaxRetainIndex Value: 9  
Log4M: DEBUG 8576 \[LogManager.m](LogManager.delete:66) - LogManager deleted for PID=8576  
Log4M: DEBUG 8576 \[LogManager.m](LogManager.shutdownInternal:103) - LogManager::shutdown()  
Log4M: DEBUG 8576 \[LoggerRepository.m](LoggerRepository.shutdown:91) - LoggerRepository::shutdown()  
Log4M: DEBUG 8576 \[Logger.m](Logger.closeAppenders:98) - Closing 2 appenders for root

## 5. Docker code

The following code is provided in directory "radiomics_MATLAB_docker".

-   "MCR-R2018b-Phusion": a directory containing a single Dockerfile, as described in the main
    text of the chapter.

-   "radiomicsContainer-MCR-R2018b-Phusion": a directory containing the files necessary to run the container using the mock input data

    -   "compiled_MATLAB": a directory that needs to contain the MATLAB executable, shell
        script and other files created in Section 4 of these
        instructions ("radiomicsContainer", "run_radiomicsContainer.sh",
        "mccExcludedFiles.log", "readme.txt" and "requiredMCRProducts.txt").
        One important change is necessary with respect to Section 4: Line 3 of the function "radiomicsContainer.m" must be modified to `USE_MOCK = 0` before compilation and copying the files to this location.

    -   "Dockerfile": note that this file contains references to the files in
        "compiled_MATLAB".

    -   "log4m.xml": note that this is a different version of "log4m.xml" file from the one
        described in Section 2, which locates the EtherJ log file in the
        "/output" directory of the container.

    -   "radiomics_container_mock_data": these are the same data as present in the "radiomics_MATLAB_dev" directory.

## 6. Creating and running the Docker containers

-   Ensure that Docker is correctly installed and that the Docker daemon
    is running. Check all is well by typing "`docker --version`" at a Linux console

-   Create the *MATLAB-Phusion* container. Change to the "MCR-R2018b-Phusion" directory and type the following:

    `docker build --t mcr2018b-phusion:book .`
    
    The tag after the colon is arbitrary. On my system I need to preface
    each Docker command by "sudo" in order to have sufficient privileges
    to run them. Without "sudo" I get a (not very explanatory) error
    messages.
    
    Note: the step to create this container takes a long time, because it has to pull a very
    large file from the Mathworks website. Also note that we used the
    R2018b release. When extending this work, it is important that the
    version used here matches the version used to generate the MATLAB
    executable above.

-   Create the final radiomics container. Change to the "radiomicsContainer-MCR-R2018b-Phusion directory" and type the following:

    `docker build -t radiomics_container-mcr2018b-phusion:book .`

    Remember that you may need to preface the command with "sudo". The
    output of this container should look something like this:

Sending build context to Docker daemon  40.54MB  
Step 1/5 : FROM mcr2018b-phusion:book  
 ---> 07057783f8b6  
Step 2/5 : COPY ./log4m.xml /.ether/log4m.xml  
 ---> Using cache  
 ---> 3edfaef28e78  
Step 3/5 : COPY ./compiled_MATLAB /mnt  
 ---> 3757546c85d3  
Step 4/5 : COPY ./radiomics_container_mock_data /  
 ---> 64d9da4ed01a  
Step 5/5 : CMD \["/sbin/my_init", "--", "/mnt/run_radiomicsContainer.sh",     "/usr/local/MATLAB/MATLAB_Runtime/v95"]  
 ---> Running in 61c4dff7d158  
Removing intermediate container 61c4dff7d158  
 ---> 1db8d719fe3a  
Successfully built 1db8d719fe3a  
Successfully tagged radiomics_container-mcr2018b-phusion:book  

-   Finally, check that the container works correctly:
    
    `docker run -it radiomics_container-mcr2018b-phusion:book`
    
    A successful output will look like this:

\*\*\* Running /etc/my_init.d/00_regen_ssh_host_keys.sh...  
\*\*\* Running /etc/my_init.d/10_syslog-ng.init....  
Oct  9 14:47:23 d35c94e78c1b syslog-ng\[10]: syslog-ng starting up; version='3.5.6'.  
Oct  9 14:47:23 d35c94e78c1b syslog-ng\[10]: WARNING: you are using the pipe driver, underlying file is not a FIFO, it should be used by file(); filename='/dev/stdout'.  
Oct  9 14:47:23 d35c94e78c1b syslog-ng\[10]: EOF on control channel, closing connection;.  
\*\*\* Running /etc/rc.local....  
\*\*\* Booting runit daemon....  
\*\*\* Runit started as PID 16.  
\*\*\* Running /mnt/run_radiomicsContainer.sh /usr/local/MATLAB/MATLAB_Runtime/v95....  
\------------------------------------------  
Setting up environment variables.  
\---  
LD_LIBRARY_PATH is .:/usr/local/MATLAB/MATLAB_Runtime/v95/runtime/glnxa64:/usr/local/MATLAB/MATLAB_Runtime/v95/bin/glnxa64:/usr/local/MATLAB/MATLAB_Runtime/v95/sys/os/glnxa64:/usr/local/MATLAB/MATLAB_Runtime/v95/sys/opengl/lib/glnxa64.  
Oct  9 14:47:24 d35c94e78c1b cron\[21]: (CRON) INFO (pidfile fd = 3).  
Oct  9 14:47:24 d35c94e78c1b cron\[21]: (CRON) INFO (Running @reboot jobs).  
Log4M: INFO 22 \[Log4M.m](Log4M.readConfig:190) - Configuration file read - /.ether/log4m.xml.  
Log4M: INFO 22 \[Log4M.m](Log4M.parseLogger:248) - * Logger: root (debug).  
Log4M: INFO 22 \[Log4M.m](Log4M.parseAppender:206) -   * Appender: ether.log4m.ConsoleAppender.  
Log4M: INFO 22 \[Log4M.m](Log4M.parseAppender:206) -   * Appender: ether.log4m.RollingFileAppender.  
Log4M: INFO 22 \[Log4M.m](Log4M.parseAppender:215) -     * Key: File Value: /output/ether.log.  
Log4M: INFO 22 \[Log4M.m](Log4M.parseAppender:215) -     * Key: MaxFileSize Value: 1M.  
Log4M: INFO 22 \[Log4M.m](Log4M.parseAppender:215) -     * Key: MaxRetainIndex Value: 9.  
Log4M: DEBUG 22 \[LogManager.m](LogManager.LogManager:80) - LogManager initialising....  
Log4M: DEBUG 22 \[RootLogger.m](RootLogger.RootLogger:22) - RootLogger level: DEBUG.  
Log4M: DEBUG 22 \[Logger.m](Logger.set.level:65) - Logger level: DEBUG.  
Log4M: DEBUG 22 \[LogManager.m](LogManager.configureRepo:92) - LogManager::configureRepo(ether).  
Log4M: DEBUG 22 \[LoggerRepository.m](LoggerRepository.configure:32) - Configuring repository.  
Log4M: DEBUG 22 \[Logger.m](Logger.set.level:65) - Logger level: DEBUG.  
Log4M: DEBUG 22 \[Logger.m](Logger.addAppender:76) - Adding appender(Console) to logger(root).  
Log4M: DEBUG 22 \[RollingFileAppender.m](RollingFileAppender.RollingFileAppender:37) - RollingFileAppender: /output/ether.log.  
Log4M: DEBUG 22 \[LoggerRepository.m](LoggerRepository.createRollingFileAppender:156) - MaxFileSize: 1M.  
Log4M: DEBUG 22 \[Logger.m](Logger.addAppender:76) - Adding appender(RollingFile) to logger(root).  
Log4M: DEBUG 22 \[LoggerRepository.m](LoggerRepository.getNamedLogger:173) - Requested: radiomicsContainer.  
Log4M: DEBUG 22 \[LoggerRepository.m](LoggerRepository.updateParents:244) - Updating parents for radiomicsContainer.  
Log4M: DEBUG 22 \[LoggerRepository.m](LoggerRepository.updateParents:275) - Logger radiomicsContainer has parent root.  
09-Oct-2020 14:47:48.667 INFO 22 \[radiomicsContainer.m](radiomicsContainer:6) - Starting radiomics analysis ....  
log4j:WARN No appenders could be found for logger (icr.etherj.AbstractPathScan)..  
log4j:WARN Please initialize the log4j system properly..  
Log4M: DEBUG 22 \[LoggerRepository.m](LoggerRepository.getNamedLogger:173) - Requested: ether.dicom.Toolkit.  
Log4M: DEBUG 22 \[LoggerRepository.m](LoggerRepository.updateParents:244) - Updating parents for ether.dicom.Toolkit.  
Log4M: DEBUG 22 \[LoggerRepository.m](LoggerRepository.updateParents:247) - Searching repository for ether.dicom.  
Log4M: DEBUG 22 \[LoggerRepository.m](LoggerRepository.updateParents:249) - No parent ether.dicom found, creating LoggerNode.  
Log4M: DEBUG 22 \[LoggerRepository.m](LoggerRepository.updateParents:247) - Searching repository for ether.  
Log4M: DEBUG 22 \[LoggerRepository.m](LoggerRepository.updateParents:249) - No parent ether found, creating LoggerNode.  
Log4M: DEBUG 22 \[LoggerRepository.m](LoggerRepository.updateParents:275) - Logger ether.dicom.Toolkit has parent root.  
Log4M: DEBUG 22 \[LoggerRepository.m](LoggerRepository.getNamedLogger:173) - Requested: ether.dicom.SopInstance.  
Log4M: DEBUG 22 \[LoggerRepository.m](LoggerRepository.updateParents:244) - Updating parents for ether.dicom.SopInstance.  
Log4M: DEBUG 22 \[LoggerRepository.m](LoggerRepository.updateParents:247) - Searching repository for ether.dicom.  
Log4M: DEBUG 22 \[LoggerRepository.m](LoggerRepository.updateParents:275) - Logger ether.dicom.SopInstance has parent root.  
Log4M: DEBUG 22 \[LoggerRepository.m](LoggerRepository.getNamedLogger:173) - Requested: ether.dicom.JavaSopInstance.  
Log4M: DEBUG 22 \[LoggerRepository.m](LoggerRepository.updateParents:244) - Updating parents for ether.dicom.JavaSopInstance.  
Log4M: DEBUG 22 \[LoggerRepository.m](LoggerRepository.updateParents:247) - Searching repository for ether.dicom.  
Log4M: DEBUG 22 \[LoggerRepository.m](LoggerRepository.updateParents:275) - Logger ether.dicom.JavaSopInstance has parent root.  
Log4M: DEBUG 22 \[LoggerRepository.m](LoggerRepository.getNamedLogger:173) - Requested: ether.dicom.Image.  
Log4M: DEBUG 22 \[LoggerRepository.m](LoggerRepository.updateParents:244) - Updating parents for ether.dicom.Image.  
Log4M: DEBUG 22 \[LoggerRepository.m](LoggerRepository.updateParents:247) - Searching repository for ether.dicom.  
Log4M: DEBUG 22 \[LoggerRepository.m](LoggerRepository.updateParents:275) - Logger ether.dicom.Image has parent root.  
Log4M: DEBUG 22 \[LoggerRepository.m](LoggerRepository.getNamedLogger:173) - Requested: ether.dicom.MRImage
Log4M: DEBUG 22 \[LoggerRepository.m](LoggerRepository.updateParents:244) - Updating parents for ether.dicom.MRImage.  
Log4M: DEBUG 22 \[LoggerRepository.m](LoggerRepository.updateParents:247) - Searching repository for ether.dicom.  
Log4M: DEBUG 22 \[LoggerRepository.m](LoggerRepository.updateParents:275) - Logger ether.dicom.MRImage has parent root.  
Log4M: DEBUG 22 \[LoggerRepository.m](LoggerRepository.getNamedLogger:173) - Requested: radiomics.TextureAnalyser3D.  
Log4M: DEBUG 22 \[LoggerRepository.m](LoggerRepository.updateParents:244) - Updating parents for radiomics.TextureAnalyser3D.  
Log4M: DEBUG 22 \[LoggerRepository.m](LoggerRepository.updateParents:247) - Searching repository for radiomics.  
Log4M: DEBUG 22 \[LoggerRepository.m](LoggerRepository.updateParents:249) - No parent radiomics found, creating LoggerNode.  
Log4M: DEBUG 22 \[LoggerRepository.m](LoggerRepository.updateParents:275) - Logger radiomics.TextureAnalyser3D has parent root.  
09-Oct-2020 14:47:50.380 INFO 22 \[TextureAnalyser3D.m](TextureAnalyser3D.analyse:23) - Fetching referenced series.  
09-Oct-2020 14:47:50.390 INFO 22 \[TextureAnalyser3D.m](TextureAnalyser3D.buildRefSeriesMap:54) - Fetching referenced series.  
09-Oct-2020 14:47:51.152 INFO 22 \[TextureAnalyser3D.m](TextureAnalyser3D.insertSeries:228) - Series loaded: 1.2.840.113654.2.45.2.109935.  
09-Oct-2020 14:47:51.159 INFO 22 \[TextureAnalyser3D.m](TextureAnalyser3D.buildRefSeriesMap:76) - Series: 2 - T1 SPIN ECHO (1.2.840.113654.2.45.2.109935).  
09-Oct-2020 14:47:51.183 INFO 22 \[TextureAnalyser3D.m](TextureAnalyser3D.buildRefSeriesMap:86) - 1 referenced series loaded.  
09-Oct-2020 14:47:51.186 INFO 22 \[TextureAnalyser3D.m](TextureAnalyser3D.analyse:27) - Processing annotation (1 of 1).  
09-Oct-2020 14:47:51.187 INFO 22 \[TextureAnalyser3D.m](TextureAnalyser3D.processRtRoiItem:290) - Processing Patient case001, RT-STRUCT: Gavin_test.  
09-Oct-2020 14:47:56.575 INFO 22 \[TextureAnalyser3D.m](TextureAnalyser3D.processWavelet:322) - Processing wavelet decomposition: LLL.  
09-Oct-2020 14:47:56.676 INFO 22 \[TextureAnalyser3D.m](TextureAnalyser3D.processWavelet:322) - Processing wavelet decomposition: LLH.  
09-Oct-2020 14:47:56.751 INFO 22 \[TextureAnalyser3D.m](TextureAnalyser3D.processWavelet:322) - Processing wavelet decomposition: LHL.  
09-Oct-2020 14:47:56.969 INFO 22 \[TextureAnalyser3D.m](TextureAnalyser3D.processWavelet:322) - Processing wavelet decomposition: LHH.  
09-Oct-2020 14:47:57.080 INFO 22 \[TextureAnalyser3D.m](TextureAnalyser3D.processWavelet:322) - Processing wavelet decomposition: HLL.  
09-Oct-2020 14:47:57.146 INFO 22 \[TextureAnalyser3D.m](TextureAnalyser3D.processWavelet:322) - Processing wavelet decomposition: HLH.  
09-Oct-2020 14:47:57.205 INFO 22 \[TextureAnalyser3D.m](TextureAnalyser3D.processWavelet:322) - Processing wavelet decomposition: HHL.  
09-Oct-2020 14:47:57.276 INFO 22 \[TextureAnalyser3D.m](TextureAnalyser3D.processWavelet:322) - Processing wavelet decomposition: HHH.  
09-Oct-2020 14:47:57.487 INFO 22 \[radiomicsContainer.m](radiomicsContainer:16) - Radiomics analysis complete - container exiting..  
Log4M: INFO 22 \[Log4M.m](Log4M.readConfig:190) - Configuration file read - /.ether/log4m.xml.  
Log4M: INFO 22 \[Log4M.m](Log4M.parseLogger:248) - * Logger: root (debug).  
Log4M: INFO 22 \[Log4M.m](Log4M.parseAppender:206) -   * Appender: ether.log4m.ConsoleAppender.  
Log4M: INFO 22 \[Log4M.m](Log4M.parseAppender:206) -   * Appender: ether.log4m.RollingFileAppender.  
Log4M: INFO 22 \[Log4M.m](Log4M.parseAppender:215) -     * Key: File Value: /output/ether.log.  
Log4M: INFO 22 \[Log4M.m](Log4M.parseAppender:215) -     * Key: MaxFileSize Value: 1M.  
Log4M: INFO 22 \[Log4M.m](Log4M.parseAppender:215) -     * Key: MaxRetainIndex Value: 9.  
Log4M: DEBUG 22 \[LogManager.m](LogManager.delete:66) - LogManager deleted for PID=22.  
Log4M: DEBUG 22 \[LogManager.m](LogManager.shutdownInternal:103) - LogManager::shutdown().  
Log4M: DEBUG 22 \[LoggerRepository.m](LoggerRepository.shutdown:91) - LoggerRepository::shutdown().  
Log4M: DEBUG 22 \[Logger.m](Logger.closeAppenders:98) - Closing 2 appenders for root.  
\*\*\* /mnt/run_radiomicsContainer.sh exited with status 0..  
\*\*\* Shutting down runit daemon (PID 16)....  
\*\*\* Running /etc/my_init.post_shutdown.d/10_syslog-ng.shutdown....  
Oct  9 14:48:01 d35c94e78c1b syslog-ng\[10]: syslog-ng shutting down; version='3.5.6'.  
Oct  9 14:48:01 d35c94e78c1b syslog-ng\[10]: EOF on control channel, closing connection;.  
\*\*\* Killing all processes....  
