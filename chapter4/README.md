# Importing, manipulating and displaying DICOM data in MATLAB
The MATLAB code files in this folder demonstrates capabilities that MATLAB provides for analyzing medical imaging data. Since the DICOM standard is the established framework for storing and transmitting medical images, this code showcases native DICOM support in MATLAB. The script is:

**chapter4Demo.m** - contains examples of working with DICOM data in MATLAB. The examples in this script are neatly divided into different sections that exactly matches those in the book chapter

Further, there are also one DICOM image and two ZIP-files supplied. These are:

- "phantomImage.dcm" that is used in one of the examples
- "dataBookCover" contains anonymized CT data from a real patient examination
- "configBookCover.zip" contains three different configuration setting files for the MATLAB app *VolumeViewer* which is used in one of the examples together with the patient data described above

*Note: The patient data and configuration setting files were those used to generate the image on the front cover of the book*

## Setup ##
A base install of MATLAB is required. 
 
Required toolboxes:  
- *Imaging Process Toolbox*  

Tested with MATLAB R2020a.

## Contact ##
Piyush Khopkar  
pkhopkar@mathworks.com

## License ##
[MIT](https://choosealicense.com/licenses/mit/). See the license file in this folder (LICENSE.txt).