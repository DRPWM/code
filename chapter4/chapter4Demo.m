% Description of the script chapter4Demo.m
% This script contains the example code used in Chapter 4: 
% Importing, Manipulating, and Displaying DICOM Data in MATLAB
% 
% The code in this script is divided into multiple sections. Every section
% specifies the corresponding section number in the chapter.

% The sections are best run separately using the "Run Section" button. Some
% sections rely on the first section (4.2.1) having been run previously. 
% ---------------------------

% Script name- chapter4Demo.m
% Version- 1.0 
% By Piyush Khopkar & Viju Ravichandran, 2019 
% ---------------------------


%% Section 4.2.1- Locating DICOM Data

clear all
close all
clc

% Assign DICOMDIR variable to the sample DICOM images in MATLAB
dataDir = fullfile(matlabroot,'toolbox/images/imdata');

% Add the directory to the MATLAB search path (unnecessary here)
addpath(dataDir);

%% Section 4.2.2- Interpreting DICOM data

% Generate details about the DICOM data
collection = dicomCollection(dataDir)

% Display metadata from 'CT-MONO2-16-ankle.dcm'
dicomdisp('CT-MONO2-16-ankle.dcm');

% Read metadata from 'CT-MONO2-16-ankle.dcm'
dcmInfo = dicominfo('CT-MONO2-16-ankle.dcm')

% Using dot notation to access the "width" parameter
imageWidth = dcmInfo.Width


%% Section 4.3.2- Importing a DICOM image

% Imports the DICOM data file US-PAL-8-10x-echo.dcm
dcmImage = dicomread('US-PAL-8-10x-echo.dcm');

[dcmImage,cmap] = dicomread('US-PAL-8-10x-echo.dcm');


%% Section 4.3.3- Importing a DICOM image series

source = fullfile(dataDir,'dog');
% Read a DICOM volume
[dcmVolume,spatial,dim] = dicomreadVolume(source);


%% Section 4.4.1- Writing a DICOM file 

% Read knee1.dcm DICOM image
kneeImage = dicomread('knee1.dcm');

% Read metadata from Knee1.dcm
metadataOriginalImage = dicominfo('knee1.dcm');

% Write image kneeImage and metadataOriginalImage to a new DICOM file
dicomwrite(kneeImage, 'newKnee1.dcm', metadataOriginalImage);

% Create a new UID 
newID = dicomuid;

imageData = kneeImage; metadata = metadataOriginalImage;
% Including private attributes when writing to a DICOM file
dicomwrite(imageData, 'newImage.dcm', metadata, 'WritePrivate', true);

% Attempt to write to a DICOM file with an unsupported IOD
dicomwrite(imageData, 'newImage.dcm', metadata, 'CreateMode', 'Copy');


%% Section 4.4.2- Writing a DICOM series

% Copy the 22 files of the 'dog' series specifying new study/series UIDs
StudyInstanceUID = dicomuid;
SeriesInstanceUID = dicomuid;
for iFile = 0:21
    originalName = fullfile(dataDir,'dog',sprintf('dog%02d.dcm', iFile));
    dcmImage = dicomread(originalName);
    dcmInfo = dicominfo(originalName);
    dcmInfo.StudyInstanceUID = StudyInstanceUID;
    dcmInfo.SeriesInstanceUID = SeriesInstanceUID;
    newName = sprintf('copy%02d.dcm',iFile);
    dicomwrite(dcmImage,newName,dcmInfo);
end


%% Section 4.4.3 - Anonymizing DICOM Data

% Create a version of a DICOM file with all personal information removed
dicomanon('US-PAL-8-10x-echo.dcm','US-PAL-anonymized.dcm');

% Remove only patient's personal information while retaining certain fields
% such as age, gender, etc.
dicomanon('US-PAL-8-10x-echo.dcm','US-PAL-anonymized.dcm','keep',...
         {'PatientAge','PatientSex','StudyDescription'});

% Anonymize the 22 files of the 'dog' series specifying new study/series UIDs
values.StudyInstanceUID = dicomuid;
values.SeriesInstanceUID = dicomuid;
for iFile = 0:21
    originalName = fullfile(dataDir,'dog',sprintf('dog%02d.dcm', iFile));
    newName = sprintf('anon%02d.dcm',iFile);
    dicomanon(originalName,newName,'update',values);
end    
     
     
%% Section 4.5.1 Displaying 2D data
% The command window will prompt key presses

% Load 'US-PAL-8-10x-echo.dcm' DICOM image and display it using 'montage' 
[dcmImage,cmap] = dicomread('US-PAL-8-10x-echo.dcm');
figure;
montage(dcmImage)

disp('Press any key to continue!'); pause;

colormap(cmap)

disp('Press any key to continue!'); pause;

% Use colormap 'map' returned in the above step to display the DICOM imaged
% as [2,5] grid in a rectangle 
figure;
montage(dcmImage, cmap, 'Size', [2,5])

% Specify indices to display certain frames
figure;
montage(dcmImage, cmap, 'Indices', 3:5)

% Get the first frame and display firstFrame using imshow
firstFrame = dcmImage(:,:,:,1); 
figure;
imshow(firstFrame, cmap);

disp('Press any key to continue!'); pause;

% Display single frame image directly using imshow
figure;
imshow('CT-MONO2-16-ankle.dcm','DisplayRange',[])

disp('Press any key to continue!'); pause;

% For CT: convert pixel values to HU-values
CTInfo = dicominfo('phantomImage.dcm'); % Import metadata
CTImage = double(dicomread('phantomImage.dcm')); % Import image & convert to double
CTImageRescale = CTInfo.RescaleSlope.* CTImage + CTInfo.RescaleIntercept;
 
% Display image data with standard window leveling (0 = black, 1 = white)
figure;
imshow(CTImageRescale);

% Display image data with "full" window leveling (min to max)
figure;
imshow(CTImageRescale,[]);

% Display image data with window leveling defined in the DICOM metadata
windowCenter = CTInfo.WindowCenter(1);
windowWidth = CTInfo.WindowWidth(1);
lowerBound = windowCenter - windowWidth/2;
upperBound = windowCenter + windowWidth/2;
figure;
imshow(CTImageRescale,'DisplayRange',[lowerBound upperBound]);


%% Section 4.5.3 Displaying 3D data
% The command window will prompt key presses

% Step 1- Load DICOM volume data and extract information from the data 
[dicomVolume,spatial,dim] = dicomreadVolume(fullfile(dataDir,'dog'));

% Remove singleton dimension from the data set
dicomVolume = squeeze(dicomVolume);

% Opening Volume Viewer without data
volumeViewer;

disp('Press any key to continue!'); pause;

volumeViewer close;

% Below are the setting to visualize the volume as shown in the example
% -----
% Background color- go with the default color 
% Zoom and rotate- zoom and rotate the image to get different view
% Change view- set its value to the default as 'volume'
% Changing spatial reference- set 8 units/vz in the Z-axis
% Refine the view with rendering editor
% - Method for volume viewing- set it to 'volume rendering' which is default
% - Alphamap- set it to 'mr-cri' 
% - Colormap- set it to 'MR' 
% - Changing lightning- set it to 'on' which is default 

% Book cover image example using volume viewer, volshow and a pre-saved config

dataExists = exist('dataBookCover');
if dataExists ~= 7
    unzip('dataBookCover.zip');
end

[V,spatial,dim] = dicomreadVolume('dataBookCover');
V = squeeze(V);

% Opening Volume Viewer with book cover data set
volumeViewer(V);

disp('Press any key to continue!'); pause;
volumeViewer close;

configExists = exist('configBookCover');
if configExists ~= 7
    unzip('configBookCover.zip');
end

load('configBookCover/configSettingsSepia.mat')
config = renderSettingsSepia

% V is a 3D grayscale volume 
% config is a struct containing exported rendering information 
volshow(V,config);
