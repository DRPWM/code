# Source code repository for *Diagnostic Radiology Physics with MATLAB: a Problem-based Approach*

![Cover](cover.tif)

## Find the book [here](https://www.taylorfrancis.com/books/9781351188197)

### Editors  
Johan Helmenkamp  
(sjoebergdesign@gmail.com)  

Robert Bujila  
(work.robert.bujila@gmail.com)  

Gavin Poludniowski  
(gpoludniowski@gmail.com)  

Note:  
JH is the designated editor contact for chapters 2, 4, 5, 10, 17, 19, 20  
RB is the designated editor contact for chapters 1, 3, 11, 12, 13, 14  
GP is the designated editor contact for chapters 6, 7, 8, 9, 15, 16, 18  

### Getting started
You can either clone this repository (recommended) or download it as a ZIP-file.

*Alternative 1: Cloning the repository*  
1. If you haven't already done so, follow the instructions by MathWorks on [how to set up Git with MATLAB](https://se.mathworks.com/help/matlab/matlab_prog/set-up-git-source-control.html)  
2. Follow the instructions by MathWorks on [how to clone a Git repository](https://se.mathworks.com/help/matlab/matlab_prog/clone-from-git-repository.html)  
3. Copy and paste the repository URL when prompted in the above step. Press the "Clone" button at the top right corner of this repository to easily access the repository URL  

Note: it is also possible to clone the repository without using MATLAB, e.g. from the command line, if you have Git installed on your system (see [here](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git/) and [here](https://git-scm.com/book/en/v2/Git-Basics-Getting-a-Git-Repository)). 

*Alternative 2: Downloading the repository as a ZIP-file*  
1. Press the more options button (three horizontal dots) right next to the "Clone" button in this repository, and select *Download repository*  

### Issue tracker
There will be bugs, limitations or things that are unclear about the code. There always are. You are welcome to email the editors or the designated author contact for each chapter. But you can also raise an issue on this *bitbucket* repository. Just go to the repository and click on the *Issues* option in the column to the left. Thank you!

### Chapter listing

(1) The Role of Programming in Healthcare *[no digital content]*

*Johan Helmenkamp, Gavin Poludniowski, and Robert Bujila*

(2) MATLAB Fundamentals

*Javier Gazzarri and Cindy Solomon*

(3) Data Sources in Medical Imaging

*Jonas Andersson, Gavin Poludniowski, Robert Bujila, and Josef Lundman*

(4) Importing, Manipulating, and Displaying DICOM Data in MATLAB

*Piyush Khopkar, Josef Lundman, and Viju Ravichandran*

(5) Creating Automated Workflows with MATLAB

*Johan Helmenkamp and Sven Månsson*

(6) Integration With Other Programming Languages and Environments

*Gavin Poludniowski and Matt Whitaker*

(7) Good Programming Practices

*Yanlu Wang and Piyush Khopkar*

(8) Sharing Software

*Yanlu Wang and Piyush Khopkar*

(9) Regulatory Considerations When Deploying Your Software in a Clinical Environment *[no digital content]*

*Philip Cosgriff and Johan Åtting*

(10) Applying Good Software Development Processes in Practice *[no digital content]*

*Tanya Kairn*

(11) Automating Quality Control Tests and Evaluating ATCM in Computed Tomography

*Patrik Nowik*

(12) Parsing and Analyzing Radiation Dose Structured Reports

*Robert Bujila*

(13) Method of Determining Patient Size Surrogates using CT Images

*Christiane Sarah Burton*

(14) Reconstructing the Exposure Geometry in X-ray Angiography and Interventional Radiology

*Artur Omar*

(15) Simulation of Anatomical Structure in Mammography and Breast Tomosynthesis Using Perlin Noise

*Magnus Dustler*

(16) xrTk: a MATLAB Toolkit for X-ray Physics Calculations

*Tomi F. Nano and Ian A. Cunningham*

(17) Automating Daily QC for an MRI Scanner

*Sven Månsson*

(18) Image Processing at Scale by Containerizing MATLAB

*James D'Arcy, Simon J Doran, and Matthew Orton*

(19) Estimation of Arterial Wall Movements

*Magnus Cinthio, John Albinsson, Tobias Erlov, Tomas Jansson, and Åsa Ryden Ahlgren*

(20) Importation and Visualisation of Ultrasound Data

*Tobias Erlöv, Tomas Jansson, and Magnus Cinthio*