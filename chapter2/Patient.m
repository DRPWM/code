classdef Patient
   properties
      name
      age
      height
      weight
   end
   methods
      function obj = Patient(age, name, height, weight) % constructor
        obj.age    = age;
        obj.name   = name;
        obj.height = height;
        obj.weight = weight;
      end
      function BMI = calculateBMI(obj) % method to return BMI
         BMI = [obj.weight]/[obj.height]^2;
         fprintf("BMI: %2.2f\n", BMI);
      end
      function printPatientInfo(obj) % method to print info
         fprintf("Patient Name: %s\n", obj.name);
         fprintf("Age: %d\n", obj.age);
         fprintf("Height: %d m \n", obj.height);
         fprintf("Weight: %d kg \n\n", obj.weight); 
      end
   end
end

