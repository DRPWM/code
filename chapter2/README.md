# MATLAB fundamentals
This set of MATLAB code files showcases basic MATLAB syntax and a couple of useful features. The scripts are:

**demoChapter2.m** - contains all the examples from the chapter bundled nicely into ordered sections that matches the book. You can either run each section by itself to test them individually, or run them all at once

**Patient.m** - example of how to define a class in MATLAB

**myProduct.m** - an example of a function that returns the product of the inputs

**generatePatientData.m** - a script that generates some random patient data that can be used for the other scripts and functions contained within this chapter

**writePatientTablestoExcel.m** - a script that writes pregenerated data from the script above into Excel files

There are also a couple of pregenerated patient data sets supplied in the "data" folder, as well as an image file "leaf.jpg" that is used in one of the examples.

## Setup ##
A base install of MATLAB is required.

Required toolboxes:  
- *None*  

Tested with MATLAB R2020a.

## Contact ##
Javier Gazzarri  
jgazzarr@mathworks.com

## License ##
[MIT](https://choosealicense.com/licenses/mit/). See the license file in this folder (LICENSE.txt).