function writePatientTablesToExcel

% Create a folder called "data" in your current working directory.
if ~exist('data','dir')
    mkdir data
end

% Write to 5 different Excel files

for i=1:5
    T = generatePatientData;
    writetable(T,[pwd filesep 'data' filesep 'data' num2str(i) '.xlsx']);
end