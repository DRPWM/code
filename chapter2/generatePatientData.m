function T = generatePatientData

numPatients = 100;

% Create random genders and names for each patient
a = abs(randn(1,numPatients));
for i=1:numPatients
    if a(i)>1
        patientData.gender{i}='male';
    else
        patientData.gender{i}='female';
    end
    patientData.name{i} = char(80+10*rand());
end

% Specific data for each variable is varied around a mean value
patientData.age = floor(50+10*randn(1,numPatients));
patientData.gender = categorical(patientData.gender);
patientData.height = floor(160+15*randn(1,numPatients));
patientData.weight = floor(70+15*randn(1,numPatients));
patientData.heartRate = floor(60+2*randn(1,numPatients));
patientData.SpO2 = floor(97+3*rand(1,numPatients));


% Add systolic and diastolic blood pressure data to patientData. 
% A slope of about 1 mm Hg / kg is reported here:
% https://www.ncbi.nlm.nih.gov/pubmed/3070038 

slope = 1.3;
slope2 = 0.7;
patientData.sysBP = floor(patientData.weight * slope ...
                          + patientData.age * slope2 ...
                          + 10 * randn(1,numPatients));
                      
patientData.diaBP = floor(patientData.sysBP/2 + 5 * randn(1,numPatients));

% Put all information into a single MATLAB table.
T = table(patientData.name',patientData.age',patientData.gender', ...
    patientData.height', patientData.weight',patientData.sysBP', ...
    patientData.diaBP', patientData.heartRate',patientData.SpO2', ...
    'VariableNames',{'name','age','gender','height','weight', ...
    'sysBP','diaBP','heartRate','SpO2'});

end
