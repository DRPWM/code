clear all % clear workspace
close all % close all figures
clc % clear command window


%% Section 1.2 Variables and Data Types
disp('Section 1.2 Variables and Data Types')

% Initialize a simple variable
disp(newline) % display newline in command window
disp('Initialize a simple variable')
heartRate = 65

% Supress output with a semi-colon (;)
disp(newline)
disp('Initialize a simple variable with supressed output')
heartRate = 65;
disp(newline) 

% Display heartRate variable
disp(newline)
disp('heartRate has the value')
disp(heartRate)

% Perform a computational statement without assigning a variable
disp(newline)
disp('Computational statement without assigning to a variable')
60+5

% Define values with different data types
disp('Define a number as a "single" data type')
singleParam = single(45.678)
disp('Convert value of "single" data type to "double" data type')
doubleParam = double(singleParam)

%% Section 1.3 Arrays and Matrix Manipulation
disp(newline)
disp('Section 1.3 Arrays and Matrix Manipulation')

% Use the whos function to see information about variables
disp(newline)
disp('Displayed output using the whos function')
whos

% Create a row array
heartRate = [55 57 62 58 60 63 58 58 59 60];

% Calculate the mean heartRate
disp(newline)
disp('The average heart rate is:')
avgHeartRate = mean(heartRate)

% Create a matrix with several rows
disp(newline)
disp('Example of a matrix with several rows')
B = [[1 4 5];[2 3 5];[0 7 9]]

% Access specific elements in B
disp(newline)
disp('Accessing the 4th element in B in two equivalent ways')
B(4)
B(1,2)

% Calculate the inverse of B
disp(newline)
disp('The inverse of B is:')
B^-1

% Calculate the inverse of each element in B
disp(newline)
disp('The inverse of each element in B is:')
B.^-1

% Calculate the multiplication of B with itself
disp(newline)
disp('B*B is')
B*B

% Calculate the multiplication of B element by element
disp(newline)
disp('The element by element multiplication of B with itself is:')
B.*B

%% Section 1.4 More Data Types
disp(newline)
disp('Section 1.4 More Data Types')
% Create a structure data type containing information about patients that
% can be both numerical arrays as well as character arrays. Note, the
% numerical arrays use brackets ('[' and ']') while character arrays use
% curly brackets ('{' and '}'). The patientData struct will be used in the
% continuation of this demo. 

disp(newline)
disp('Assigning values to patientData table')
patientData.name = {'A','B','C','D','E','F','G','H','I','J'};
patientData.age = [30 43 38 76 79 18 48 44 64 70];
patientData.gender = {'male','male','female','male','male','female',...
    'female','male','female','female'};
patientData.height = [155 160 156 133 155 147,145 142 151 129];
patientData.weight = [61 65 57 53 107 94 74 51 57 67];
patientData.sysBP = [114 111 132 99 215 193 149 92 109 132];
patientData.diaBP = [64 51 69 51 106 91 73 45 47 66];

%% Section 1.5 Conditional Operators and Logical Indexing
disp(newline)
disp('Section 1.5 Conditional Operators and Logical Indexing')

% Re-assign the patientData.gender variable so that it is a categorical data
% type
disp(newline)
disp('Re-assigning patientData.gender to categorical data type')
patientData.gender = categorical(patientData.gender);

% Return a logical array showing the places where patientData.gender
% variable is equal to 'male'
disp(newline)
disp('Logical indexing showing where patientData.gender is equal to "male"')
maleIndex = patientData.gender == 'male'

% Using the maleIndex vector, get the sysBP values that correspond to a 
% gender that is equal to 'male'
disp(newline)
disp('The sysBP values corresponding to "male" gender')
maleSysBP = patientData.sysBP(maleIndex)

% Calculate the average sysBP for males in patientData
disp(newline)
disp('The average male sysBP')
avgMaleSysBP = mean(patientData.sysBP(maleIndex))

% Combine multiple conditional statements to find the paitients that are
% both male and have a sysBP over 140
disp(newline)
disp('Logical indexing showing a combination of patients that are male and have a sysBP > 140')
maleHSysIdx = (patientData.gender == 'male') & (patientData.sysBP > 140)

%% Section 1.6 Control Flow
disp(newline)
disp('Section 1.6 Control Flow')

% Define a 5x5 matrix using the function magic
disp(newline)
disp('Define a 5x5 matrix using the function magic')
A = magic(5)

% Loop over each element in the 5x5 matrix A. If the value of the element
% is greater than 10, replace that element with zero. 
disp(newline)
disp('Looping over all elements in A. Replacing element with 0 if it exceeds 10')
for i = 1:5
    for j = 1:5
        if A(i,j) > 10
            A(i,j) = 0;
        end
    end
end

disp(newline)
disp('A now has the following appearnce')
A

% In MATLAB, vectorization can be used to perform the same operation as the
% nested for loop above. That is, for all elements that have a value
% greater than 10, replace that element with zero. 
disp(newline)
disp('Replace all elements in A that have a value greater than 10 with 0 using vectorization.')
A(A>10) = 0

% Add another variable called heartRate to patientData.
disp(newline)
disp('Add heartRate variable to patientData')
patientData.heartRate = [70 115 220 83 90 180 100 103 50 120];

% Use a combination of if, elseif and else statements to perform different
% operations depending on patient data. The if, elseif, and else statements
% are used in conjunction with a for loop to loop through the data
% associated with each patient.
disp(newline)
disp('Loop over each patient and display heart rate analysis using if, elseif, or else conditions.')
for i = 1:length(patientData.heartRate) % Loop through each patient
    age = patientData.age(i); % Age of patient i
    maxHeartRate = 220 - age; % Max heart rate of patient i
    heartRate = patientData.heartRate(i); % Heart rate of patient i
    if(heartRate < (maxHeartRate * 0.5)) 
        disp(['Patient ' patientData.name{i} ' must increase exercise intensity.'])
    elseif(heartRate < (maxHeartRate * 0.85))
        disp(['Patient ' patientData.name{i} ' has reached target heart rate.'])
    elseif(heartRate < maxHeartRate)
        disp(['Patient ' patientData.name{i} ' has reached high '...
            'exercise intensity.'])
    else % executed when if and elseif conditions do not apply to patient
        disp(['Patient ' patientData.name{i} ' has exceeded '...
            'recommended exercise intensity.'])
    end % if
end % for loop

% Use the function input to let user assign a value to the variable
% inputVal in the Command Window.
disp(newline)
inputVal = input('Enter a field from patientData: ', 's');
% A switch statement is used to perform an operation dictated by inputVal
% from the previous step. Note, when inputVal is equal to age, heartRate,
% sysBP, or diaBP, the mean value of that data is calculated.
switch inputVal
    case{'age' 'heartRate' 'sysBP' 'diaBP'}
        fprintf('Average value for %s is %2.2f\n', inputVal, ...
            mean(patientData.(inputVal)))
    otherwise
        fprintf("No average can be calculated.\n")
end

%% Section 1.7 User Defined Functions
disp(newline)
disp('Section 1.7 User Defined Functions')

% Section 1.7.1 Functions
disp(newline)
disp('Section 1.7.1 Functions')

% A function myProduct.m is available in the Current Folder. This function
% can be opened in a new tab using the following command.
disp(newline)
disp('Open new tab showing contents of myProduct.m')
open myProduct.m
open demoChapter2.m

% The function myProduct takes two arguments, a and b. After assigning
% values to a and b, the function myProduct can be executed using as
% follows: 
a = 7;
b = 8;
disp(newline)
disp('Using function myProduct with arguments a and b')
answer = myProduct(a, b)

% Section 1.7.2 anonymous functions
disp(newline)
disp('Section 1.7.2 anonymous functions')

% Anonymous functions are declared in-line. That is, they do not have to be
% in a separate file (like the above example). 

% An example anonymous function is given below where @(x) denotes that x is
% an argument to the anonymous function sigmoid, i.e., sigmoid(x). 
disp(newline)
disp('Declare anonymous function sigmoid(x)')
sigmoid = @(x) 1./(1+exp(-x))
% Perform a calculation using the anonymous function sigmoid where the
% argument is 0.
disp('Calculate sigmoid for x=0, i.e., sigmoid(0)')
sigmoid(0)

% An example of a nested anonymous function is given below. Note, the
% argument x is used to define the upper interval of the integral. The 
% integral function uses another anonymous function over the variable t.
disp(newline)
disp('Declare an anonymous function sigmoidIntegral(x) that has a nested anonymous function')
sigmoidIntegral = @(x) integral(@(t) 1./(1+exp(-t)),-Inf,x)
disp('Calculate the integral of sigmoid with an upper interval of x, i.e., sigmoidIntegral(0)')
sigmoidIntegral(0)

%% Section 1.8 Data Analysis
disp(newline)
disp('Section 1.8 Data Analysis')
% The data used in this example has been generated using the
% generatePatientData.m function in this package. Note, the data generated
% with generatePatientData.m is random.

% Import data1.xlsx into MATLAB using the readtable function. 
disp(newline)
disp('Importing data1.xlsx into MATLAB')
patientData = readtable(fullfile('data','data1.xlsx'));

% Query the first 5 rows in the age variable
disp(newline)
disp('Query the first 5 rows in the age variable of PatientData')
patientData.age(1:5)

% Query the first 3 rows of all columns
disp(newline)
disp('Query first 3 rows of all columns in PatientData')
patientData(1:3,:)

% Show the properties of the patientData table
disp(newline)
disp('Show properties of the patientData table')
patientData.Properties

%% Section 1.9 Visualization
disp(newline)
disp('Section 1.9 Visualization')

% Add multiple plots to the same figure
disp(newline)
disp('Add multiple plots to the same figure')

figure,
hold on 
plot(patientData.weight,patientData.sysBP,'o','MarkerFaceColor','k', ...
    'MarkerEdgeColor','k');
plot(patientData.weight,patientData.diaBP,'s','MarkerFaceColor', ...
    [0.5 0.5 0.5],'MarkerEdgeColor',[0.5 0.5 0.5]);

% Add formatting to the figure
legend({'systolic' 'diastolic'},'Location','best')
xlabel('Weight (kg)')
ylabel('Blood Pressure (mm Hg)')

% Create a new figure showing a histogram over patientData.heartRate
disp(newline)
disp('Create a new figure showing a histogram over patientData.heartRate')
figure,
histogram(patientData.heartRate, 'FaceColor', [0.5 0.5 0.5], 'LineWidth',1)
xlabel('Resting Heart Rate (bpm)')
ylabel('Patient Count')

%% Section 1.10 Handling big data sets
disp(newline)
disp('Section 1.10 Handling big data sets')

% Use function datastore to import all of the spreadsheets in the
% data folder.
disp(newline)
disp('Importing spreadsheets in "data" folder to variable dataStore')
dataStore = datastore('data');

% Define T as a tall table over data imported in dataStore. Note, this
% could take some time to pool. 
disp(newline)
disp('Define tall table, T, over data in dataStore')
T = tall(dataStore)

% Queue calculations of average, std, min, and max of sysBP variable in the
% tall table T.
disp(newline)
disp('Queueing calculations on sysBP variable in T')
meanSysBP = mean(T.sysBP);
stdSysBP = std(T.sysBP);
minSysBP = min(T.sysBP);
maxSysBP = max(T.sysBP);

% Gather calculations that have been queued
disp(newline)
disp('Gathering queued calculations of sysBP in T')
results = gather([meanSysBP, stdSysBP, minSysBP, maxSysBP])

%% Section 1.11 Classes
disp(newline)
disp('Section 1.11 Classes')

% An example of a class is provided in the Current Folder. The following
% command will open the example class in a new tab.
disp(newline)
disp('Opening example class Patient.m in a new tab.')
open Patient.m
open demoChapter2.m

% Demonstration of how the example class Patient.m could be used in a
% script. 
disp(newline)
% Create new instance of Patient.m
disp('Creating new instance of Patient.m')
patientX = Patient(30, "Steve Rogers", 1.88, 99.8);
disp('Use methods in Patient.m')
patientX.printPatientInfo; % Display patient information
patientX.calculateBMI; % Calculate BMI

%% Section 1.12 Improving Code Performance
disp(newline)
disp('Section 1.12 Improving Code Performance')

% This section compares performance between using a for loop to perform a
% calcuation, vectorizing the calcuation, and pre-allocating an array to be
% used in a for loop. The function tic-toc is used to clock the execution
% time of each variation. 

% For loop
disp(newline)
disp('Calculation using a simple for loop')
clear f 
tic
for i = 1:1e6 
    f(i) = exp(-(i-1)/1e6).*sin((i-1)/1e6);
end
toc

% Vectorizing the calculation
disp(newline)
disp('Calculation that has been vectorized')
clear f 
tic
t = 0:1e-6:1;
f = exp(-t).*sin(t);
toc

% Pre-allocating array in for loop
disp(newline)
disp('Calculation using pre-allocated array in for loop')
clear f 
tic 
f = zeros(1,1e6);
for i = 1:1e6
    f(i) = exp(-(i-1)/1e6).*sin((i-1)/1e6);
end
toc

%% Section 1.13 Exercise - Basic Image Processing
disp(newline)
disp('Section 1.13 Exercise - Basic Image Processing')

close all % close all figures

% Import and display example image
disp(newline)
disp('Import example image and show in figure')
myImage = imread('leaf.jpg');
figure
imshow(myImage)

% Calculate size of image
disp(newline)
disp('The size of the example image is:')
size(myImage)

% Create new figure and plot color channel intensities of the 240th row of
% the image (mid image). 
disp(newline)
disp('Plot color channel intensities of 240th row of image')
fig = figure;
p4 = plot(1:size(myImage,2), ...
          [myImage(240,:,1); myImage(240,:,2); 
myImage(240,:,3)]);
p4(1).Color = 'k'; p4(2).Color = 'k'; p4(3).Color = 'k';
p4(1).Marker = 'none'; p4(2).Marker = '.'; p4(3).Marker = 'x';
legend({'R' 'G' 'B'},'Location','best')
ylabel('RGB level across row 240')
xlabel('column')

% Create new figure and plot binary image where object is black and the
% background is white. 
disp(newline)
disp('Generate binary image where object is black and background is white')
figure; % Create a new figure
% Define background as those pixels with blue level higher than 100
backgroundIndex = myImage(:,:,3)>100; % generates a (480x480x1) image

% Make background index compatible with image size (480x640x3) using
% repmat, a function that extends the dimensionality of the array specified
% as the first argument (backgroundIndex) along the dimensions indicated
% in subsequent arguments
idx = repmat(backgroundIndex,1,1,3);
myWhiteBackgroundImage = myImage; % Save variable with a different name
myWhiteBackgroundImage(idx) = 255; % Make background white
myWhiteBackgroundImage(~idx) = 0; % Make foreground black
imshow(myWhiteBackgroundImage)