function parsedData = parseCtRdsrSpiral(fileName)
% Function to parse technique parameters and dose indices from irradiation
% events using spiral acquisition mode
% Input: fileName [string]
% Output: parsedData [table]

dcmInfo = dicominfo(fileName); % Read DICOM info
patientWeight = dcmInfo.PatientWeight; % Get patient weight
rdsrContainer = dcmInfo.ContentSequence; % Get RDSR Container
rdsrStruct = simplifyRdsr(rdsrContainer); % Simplify RDSR
% Get the number of irradiation events from accumulated dose data container
accDoseData = rdsrStruct.CTAccumulatedDoseData_1;
numEvents = accDoseData.TotalNumberofIrradiationEvents_1;
% Count spiral events in RDSR
cnt = 1;
% Loop over each irradiation event
for eventNum = 1:numEvents
    % Get the acquisition (scan) type
    eventData = rdsrStruct.(['CTAcquisition_' num2str(eventNum)]);
    scanType = eventData.CTAcquisitionType_1;
    % if scanType is spiral acquisition, parse data
    switch scanType
        case 'Spiral Acquisition'
            % Parse Acquisition Parameters Container
            acqParams = eventData.CTAcquisitionParameters_1;
            collimation(cnt) = acqParams.NominalTotalCollimationWidth_1;
            pitch(cnt) = acqParams.PitchFactor_1;
            % Parse Xray Source Parameters Container
            xraySrcParams = acqParams.CTXRaySourceParameters_1;
            kvp(cnt) = xraySrcParams.KVP_1;
            rotTime(cnt) = xraySrcParams.ExposureTimeperRotation_1;
            % Parse CTDose Container
            ctDose = eventData.CTDose_1;
            ctdi(cnt) = ctDose.MeanCTDIvol_1;
            phantom{cnt} = ctDose.CTDIwPhantomType_1;
            dlp(cnt) = ctDose.DLP_1;
            % Add patient weight
            weight(cnt) = patientWeight;
            cnt = cnt + 1;
    end % switch scanType
end % for exposureNum
% Add data to table
parsedData = table(kvp, collimation, rotTime, pitch, ctdi, phantom, dlp, weight);
end % function

