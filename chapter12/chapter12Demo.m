clear all
close all
clc

%% MATLAB's native import of RDSR objects
% This example shows how difficult it can be to find relevant data in RDSR
% objects using MATLAB's native DICOM import functionality.

disp('Loading example RDSR object')
exampleRDSRObject = fullfile('demoSR', 'PAT_1.dcm');
dcmInfo = dicominfo(exampleRDSRObject);
disp('Extracting the kVp from 3rd Irradiation Event')
kvp = dcmInfo.ContentSequence. ... %RDSR container
        Item_15.ContentSequence. ... % CT Acquisition container for 3rd event
        Item_6.ContentSequence. ... % CT Acquisition Parameters container
        Item_8.ContentSequence. ... % CT Source Parameters container
        Item_2.MeasuredValueSequence.Item_1.NumericValue % kVp
    
%% Simplification of an RDSR object using the simplifyRDSR function
% Using the same RDSR object as above, this example shows how one could
% navigate relevant information after an RDSR object has been imported and
% the struct has been simplified.

disp('Using the function simplifyRdsr to simplify the structure of the RDSR object')
rdsrContainer = dcmInfo.ContentSequence;
simpleRdsr = simplifyRdsr(rdsrContainer);
disp('Extracting the kVp from 3rd Irradiation Event in the simplified struct')
kvp = simpleRdsr. ...
      CTAcquisition_3. ...
      CTAcquisitionParameters_1. ...
      CTXRaySourceParameters_1.KVP_1

%% Looping over multiple RDSR objects
% This example shows how one could loop over many RDSR objects and extract
% relevant information from a CT scans with a sprial technique using the 
% parseCtRdsrSpiral function. 

disp('Loop over RDSR objects in the demoSR directory and parse using the parseCTRdsrSprial function')
% The directory where CT RDSR objets are located
rdsrDirectory = 'demoSR';
% Get contents of directory
dirContents = dir(rdsrDirectory);
% Create a data table to aggregate the parsed data
dataTable = [];
% Loop over directory contents
% This code is setup to work with the parallell processing toolbox, if you
% do have that toolbox, replace for with parfor. 
for fileNumber = 1:numel(dirContents)
    % Create path/file name
    fileName = fullfile(rdsrDirectory, dirContents(fileNumber).name);
    % If "theFile" is not a directory and is DICOM proceed
    if ~dirContents(fileNumber).isdir && isdicom(fileName)
        disp(fileName)
        parsedData = parseCtRdsrSpiral(fileName); 
        % Append parsed data to dataTable 
        dataTable = [dataTable; parsedData];
    end
end
disp('Done parsing')
%% Bin RDSR data by weight
% This example shows how to bin the data that was parsed in the above
% example into different weight categories.

disp('Bin parsed data into different weight categories')
% Categorize the patient's weight
dataTable.weightCategory = ... % Append categorization to datatable
                        discretize(dataTable.weight,... % Use function on weights
                        [0, 60, 90, inf],... % Bin edges
                        'categorical',... % Array type
                        {'<60 kg', '60-90 kg', '>90 kg'}); % Categories
                    
%% Derive Statistics
% This example shows how to derive statistics from the data that was 
% categorized in the example above.

disp('Calculate stats using the grpstats function')
statArray = grpstats(dataTable,...
            {'kvp', 'collimation', 'rotTime',...
            'pitch', 'phantom', 'weightCategory'},... % Group by these variables
            'mean',... % Summary statistics of variables of interest
            'DataVars', {'weight', 'ctdi', 'dlp'}) % Variables of interest
        
%% Visualize Data
% This example shows how to plot the parsed RDSR data to show CTDIvol vs
% Patient Weight.

disp('Visualize data')
plot(dataTable(dataTable.pitch == 1.2,:).weight,... % weights with pitch 1.2
     dataTable(dataTable.pitch == 1.2,:).ctdi,... % ctdi with pitch 1.2
     'k.',...
     'DisplayName', 'Pitch == 1.2')
hold on
plot(dataTable(dataTable.pitch ~= 1.2,:).weight,... % weights with pitch other than 1.2
    dataTable(dataTable.pitch ~= 1.2,:).ctdi,... % ctdi with pitch other than 1.2
    'ko',...
    'DisplayName', 'Pitch != 1.2')
legend('location', 'NorthWest') % Uses the names given with DisplayName
xlabel('Patient Weight [kg]')
ylabel('CTDI_{vol,32cm} [mGy]')
% Use cell array if you want to have a title on multiple rows 
title({'CT Room 1', 'CT Chest w/o IV Contrast'}) 