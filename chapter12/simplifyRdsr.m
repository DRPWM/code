function simpleRdsr = simplifyRdsr(ContentSequence)
% A function to simplify the structure of an RDSR object
% Input: Content Sequence from RDSR object [struct]
% Output: Simplified RDSR struct [struct]

% Create an empty struct
simpleRdsr = struct;
% Get the fieldnames of the input ContentSequence
fieldNamesInSequence = fieldnames(ContentSequence);
% Loop over field names in ContentSequence
for fieldNameNumber = 1:numel(fieldNamesInSequence)
    % Get the concept name and value type 
    fieldData = ContentSequence.(fieldNamesInSequence{fieldNameNumber});
    conceptName = fieldData.ConceptNameCodeSequence.Item_1.CodeMeaning;
    % Remove spaces and hyphens from conceptName
    conceptNameFriendly = regexprep(conceptName, {' ', '-'}, ''); 
    valueType = fieldData.ValueType;
    % Get the value depending on value type
    switch valueType
        case 'CONTAINER'
            % If valueType is CONTAINER use recursion to go to next level
            value = simplifyRdsr(fieldData.ContentSequence);
        case 'NUM'
            value = fieldData.MeasuredValueSequence.Item_1.NumericValue;
        case 'UIDREF'
            value = fieldData.UID;
        case 'TEXT'
            value = fieldData.TextValue;
        case 'DATETIME'
            value = fieldData.DateTime;
        case 'CODE'
            value = fieldData.ConceptCodeSequence.Item_1.CodeMeaning;
    end %switch
    % At the same level in the RDSR, there can be multiple sequences with
    % the same name. Here each sequence is numbered. 
    currentFieldNames = fieldnames(simpleRdsr);
    % See how many fieldnames in the current level match the concept name 
    % of the sequence that is being simplified. 
    currentFieldNameMatch = startsWith(currentFieldNames,...
                                       conceptNameFriendly);                                
    % startsWith returns a logical array. By summing that array, we get the
    % number of matches.
    numMatches = sum(currentFieldNameMatch);
    % number each concept name according to the number of occurences in the
    % same level. 
    if numMatches == 0
        variableName = [conceptNameFriendly '_1'];
    else
        variableName = [conceptNameFriendly '_' num2str(numMatches + 1)];
    end % if
    % Add the value to the simpleRdsr struct
    % Note, because of the recursion that is used, the different levels in
    % the RDSR objects will be maintained in the simpleRdsr struct. 
    simpleRdsr.(variableName) = value;
end % for loop over fieldNamesInSequence
end % function
