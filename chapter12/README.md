# Chapter 12: Parsing and analyzing Radiation Dose Structured Reports
The demo script in this folder execute the code discussed in Chapter 12. The script is:

**chapter12Demo.m** - a script to parse and analyze the RDSR objects in the demoSR folder

## Setup ##
A base install of MATLAB is required.

Required toolboxes:  
- *Image Processing Toolbox*

Tested with MATLAB R2019b.

## Contact ##
Robert Bujila  
work.robert.bujila@gmail.com

## License ##
[MIT](https://choosealicense.com/licenses/mit/). See the license file in this folder (LICENSE.txt).
