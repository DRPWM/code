function [subX, subY] = GS(SADvalue, kernel, imageData, bestMatchPos)   
% (SADvalue) contains similarity coefficients of interest, (kernel) and ...
% (imageData) are two 2D image matrices, (bestMatchPos) is the position in ...
% (imageData) with the best similarity to (kernel), and (subX) and (subY) are ...
% the interpolated positions in units of pixels relative to the minimum ...
% pre-estimated position in lateral and axial direction.
    
[ySizeKernel, xSizeKernel] = size(kernel);
ySize = floor(ySizeKernel./2); xSize = floor(xSizeKernel./2);
    
% Lateral direction
if SADvalue(2) < SADvalue(3)
    normBlock = imageData(bestMatchPos(1)-ySize:bestMatchPos(1)+ySize, ...
        bestMatchPos(2)-1-xSize:bestMatchPos(2)-1+xSize);
else        
    normBlock = imageData(bestMatchPos(1)-ySize:bestMatchPos(1)+ySize, ...
        bestMatchPos(2)+1-xSize:bestMatchPos(2)+1+xSize);
end
block = imageData(bestMatchPos(1)-ySize:bestMatchPos(1)+ySize, ...
    bestMatchPos(2)-xSize:bestMatchPos(2)+xSize);    
normSADx = sum(sum(abs(normBlock-block)));
if (normSADx == 0) || (SADvalue(2) == SADvalue(3))
    subX = 0;
elseif SADvalue(2) < SADvalue(3)
    subX = -0.5 * (1 - (SADvalue(2) - SADvalue(1))/normSADx); 
else
    subX = 0.5 * (1 - (SADvalue(3) - SADvalue(1))/normSADx);
end
    
% Axial direction
if SADvalue(4) < SADvalue(5)
    normBlock = imageData(bestMatchPos(1)-1-ySize:bestMatchPos(1)-1+ySize, ...
        bestMatchPos(2)-xSize:bestMatchPos(2)+xSize);
else
    normBlock = imageData(bestMatchPos(1)+1-ySize:bestMatchPos(1)+1+ySize, ...
        bestMatchPos(2)-xSize:bestMatchPos(2)+xSize);
end
block = imageData(bestMatchPos(1)-ySize:bestMatchPos(1)+ySize, ...
    bestMatchPos(2)-xSize:bestMatchPos(2)+xSize);    
normSADy = sum(sum(abs(normBlock-block)));
if (normSADy == 0) || (SADvalue(4) == SADvalue(5))
    subY = 0;
elseif SADvalue(4) < SADvalue(5)
    subY = -0.5 * (1 - (SADvalue(4) - SADvalue(1))/normSADy);
else
    subY = 0.5 * (1 - (SADvalue(5) - SADvalue(1))/normSADy);
end

end