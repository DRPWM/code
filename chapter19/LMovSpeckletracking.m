function [xPos, yPos] = LMovSpeckletracking(image3D, yKernel, xKernel, ...
    ySizeKernel, xSizeKernel)
% (image3D) contains a 2D ultrasound cineloop, (yKernel) and (xKernel) ...
% the centre position of the kernel and (ySizeKernel) and (xSizeKernel) ...
% the size of the kernel

[~,~,nbrOfFrames] = size(image3D);
ySize = floor(ySizeKernel./2); xSize = floor(xSizeKernel./2);
xPos = zeros(nbrOfFrames,1); yPos = zeros(nbrOfFrames,1);
xPos(1) = xKernel; yPos(1) = yKernel;
    
% Extract kernel in frame 1 and estimate movement between frame 1 and 2
kernelB = image3D(yKernel-ySize:yKernel+ySize,xKernel-xSize:xKernel+xSize,1);
[bestMatchPos, SADvalue] = ARPS(kernelB, image3D(:,:,2), ...
    [yKernel, xKernel], [0 0]);
[subX, subY] = GS15PI(SADvalue, kernelB, image3D(:,:,2), bestMatchPos);
xPos(2) = bestMatchPos(2) + subX; yPos(2) = bestMatchPos(1) + subY;   
for i = 3:nbrOfFrames
    % Save kernel obtained in frame i-2
    kernelA = kernelB;

    % Extract kernel in frame i-1
    kernelPos = [round(yPos(i-1)) round(xPos(i-1))];
    kernelB = image3D(kernelPos(1)-ySize:kernelPos(1)+ySize, ...
        kernelPos(2)-xSize:kernelPos(2)+xSize,i-1);
        
    % Estimate movement between frame i-1 and frame i
    [bestMatchPos, SADvalue] = ARPS(kernelB, image3D(:,:,i), kernelPos, ...
        [yPos(i-1)-yPos(i-2) xPos(i-1)-xPos(i-2)]);
    [subX1, subY1] = GS15PI(SADvalue, kernelB, image3D(:,:,i), bestMatchPos);
    xPosARPS = bestMatchPos(2) + subX1 - (kernelPos(2) - xPos(i-1)); 
    yPosARPS = bestMatchPos(1) + subY1 - (kernelPos(1) - yPos(i-1));
        
    % Estimate movement between frame i-2 and frame i
    [bestMatchPosSAD, SADvalue] = SAD(kernelA, image3D(:,:,i), 3, 3, ...
        bestMatchPos(2), bestMatchPos(1));
    [subX2, subY2] = GS15PI(SADvalue, kernelA, image3D(:,:,i), bestMatchPosSAD);
    xPosSAD = bestMatchPosSAD(2) + subX2 - (round(xPos(i-2)) - xPos(i-2)); 
    yPosSAD = bestMatchPosSAD(1) + subY2 - (round(yPos(i-2)) - yPos(i-2));
        
    % Estimate average movement
    xPos(i) = (xPosARPS + xPosSAD)/2;
    yPos(i) = (yPosARPS + yPosSAD)/2;
end

end