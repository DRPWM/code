function [subX, subY] = GS15PI(SADvalue, kernel, imageData, bestMatchPos)
% (SADvalue) contains similarity coefficients of interest, (kernel) and ...
% (imageData) are two 2D image matrices, (bestMatchPos) is the position in ...
% (imageData) with the best similarity to (kernel, and (subX) and (subY) are ...
% the interpolated positions in units of pixels relative to the minimum ...
% pre-estimated position in lateral and axial directions.
    
[subX, subY] = PI(SADvalue);
if (abs(subX) > 0.15) || (abs(subY) > 0.15)
    [subX_GS, subY_GS] = GS(SADvalue, kernel, imageData, bestMatchPos);
    if abs(subX) > 0.15
        subX = subX_GS;
    end
    if abs(subY) > 0.15
        subY = subY_GS;
    end 
end

end