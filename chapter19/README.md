# Estimation of arterial wall movements
The code files in this folder encompasses a novel method for tracking of two-dimensional movements in ultrasound cine loops. The method is specifically developed for estimating arterial wall movements. The scripts and functions are:

### Example scripts
**insilicoExample.m** - An example script that uses the methodology described in the chapter for tracking an individual ultrasound echo in simulated ultrasound data  
**invivoExample.m** - An example script that uses the methodology described in the chapter for tracking an individual ultrasound echo in real patient ultrasound data 

### Block-matching algorithms 
**SAD.m** - a function that uses the *Sum of Absolute Difference* method for block-matching  
**ARPS.m** - a function that uses a sparse iterative search algorithm for block-matching called the *Adaptive Rood Pattern Search*  

### 2D interpolation techniques
**PI.m** - a function that uses a parabolic interpolation to increase the resolution for the purpose of enabling sub-sample position estimation  
**GS.m** -  a function that uses a grid slope interpolation to increase the resolution for the purpose of enabling sub-sample position estimation  
**GS15PI.m** -  a function that uses a novel combined method of the *PI* and *GS* interpolation techniques with a cut-off pixel displacement of more than 0.15 mm

### Framework for estimating wall-movement
**LMovSpeckletracking.m** - a function that combines the *GS15PI* sub-pixel interpolator and the *ARPS* search algorithm for estimating the arterial wall movement in 2D ultrasound data

In addition two ultrasound datasets are included, *insilicoExample.mat* and *invivoExample.mat* which are used by the *insilicoExample.m* and *invivoExample.m*, respectively. 

## Setup ##
A base install of MATLAB is required.

Required toolboxes:  
- *None*

Tested with MATLAB R2020a.

## Contact ##
Magnus Cinthio  
magnus.cinthio@bme.lth.se

## License ##
[MIT](https://choosealicense.com/licenses/mit/). See the license file in this folder (LICENSE.txt).