function [bestMatchPos, SADvalue] = ARPS(kernel, imageData, startPos, speed)  
% (kernel) and (imageData) are two 2D image matrices. (startPos) state the ...
% centre position of the search region and (speed) state the movement ...
% in the previous estimation. (bestMatchPos) returns the coordinates ...
% of the position with the best match and (SADvalue) returns similarity ...
% coefficients that are important in sub-pixel estimations.
    
[ySizeKernel, xSizeKernel] = size(kernel);
ySize = floor(ySizeKernel./2); xSize = floor(xSizeKernel./2);
searching = true;
    
% Set the intial search positions
searchRadius = max([1, round(speed(1)), round(speed(2))]);
searchPos = [startPos; startPos(1)-searchRadius startPos(2); ...
    startPos(1)+searchRadius startPos(2); ...
    startPos(1) startPos(2)-searchRadius; ...
    startPos(1) startPos(2)+searchRadius; ...
    startPos + round(speed)];
SADvalues = zeros(1,6);
while searching
    % Estimate the similaritiy coefficients in the search positions
    for i = 1:length(searchPos)
        block = imageData(searchPos(i,1)-ySize:searchPos(i,1)+ySize, ...
            searchPos(i,2)-xSize:searchPos(i,2)+xSize);
        SADvalues(i) = sum(sum(abs(kernel - block))); 
    end
        
    % Find the search position with minimum similaritiy coefficient
    [~, minIndex] = min(SADvalues);
    newPos = searchPos(minIndex,:);
        
    % Stop the estimation if we used the same search position twice 
    if (minIndex == 1) && (searchRadius == 1)
        searching = false;
    else
        searchRadius = max(1, floor(searchRadius/2));
        searchPos = [newPos; newPos(1)-searchRadius newPos(2); ...
            newPos(1)+searchRadius newPos(2); ...
            newPos(1) newPos(2)-searchRadius; ...
            newPos(1) newPos(2)+searchRadius];
        SADvalues = zeros(1,5);
    end
end
bestMatchPos = newPos;
SADvalue = [SADvalues(1); SADvalues(4); SADvalues(5); SADvalues(2); SADvalues(3)];

end