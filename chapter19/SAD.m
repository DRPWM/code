function [bestMatchPos, SADvalue] = SAD(kernel, imageData, n, m, xPos, yPos)
% (kernel) and (imageData) are two 2D image matrices. (n) and (m) state the ...
% size of the search region in (imageData), and (xPos) and (yPos) the centrum ...
% position of the search region. (SADvalue) returns similarity coefficients ...
% that are important in sub-pixel estimations, and (bestMatchPos) returns the ...
% coordinates of the position with the best match.
    
[ySizeKernel, xSizeKernel] = size(kernel);
ySize = floor(ySizeKernel./2); xSize = floor(xSizeKernel./2);
SADoutput = zeros(n*2+1,m*2+1);
for i = -n:n 
    for j = -m:m
        block = imageData(yPos+i-ySize:yPos+i+ySize,xPos+j-xSize:xPos+j+xSize);
        SADoutput(i+n+1,j+m+1) = sum(sum(abs(kernel-block)));
    end
end
    
% Find the position of the best match
SADtemp = SADoutput(2:end-1,2:end-1);
[~, minIndex] = min(SADtemp(:));
[r,c] = ind2sub(size(SADtemp),minIndex);
bestMatchPos = [yPos, xPos] + [r,c] + [1,1] - [n+1, m+1];
    
% Save similarity coeffcients that is needed for sub-pixel estimations
SADvalue = zeros(1,5); SADvalue(1) = SADoutput(r+1,c+1); 
SADvalue(2) = SADoutput(r+1,c-1+1); SADvalue(3) = SADoutput(r+1,c+1+1);
SADvalue(4) = SADoutput(r-1+1,c+1); SADvalue(5) = SADoutput(r+1+1,c+1);

end