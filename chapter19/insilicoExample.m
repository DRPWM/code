% Loads a simulated US image field and runs the tracking algorithm. Plots:
% (1) The first frame with the kernel ROI superimposed 
% (2) The longitudinal motion (x) against frame number
% (3) The radial motion (y) against frame number
load('insilicoExample.mat')
yKernel = 198; xKernel = 50; ySizeKernel = 9; xSizeKernel = 9;

figure(1)
frameWithROI=grayscaleImage.imageData(:,:,1);
frameWithROI(yKernel-floor(ySizeKernel/2):yKernel+floor(ySizeKernel/2), ...
    xKernel-floor(xSizeKernel/2):xKernel+floor(xSizeKernel/2))=200;
imshow(frameWithROI,[])
title('First frame (field) with ROI displayed')

[xPos,yPos]=LMovSpeckletracking(grayscaleImage.imageData, ...
    yKernel,xKernel,ySizeKernel,xSizeKernel);

figure(2)
plot(xPos)
title('Longitudinal motion')
xlabel('Frame number')
ylabel('Pixel number')

figure(3)
plot(yPos)
title('Radial motion')
xlabel('Frame number')
ylabel('Pixel number')
