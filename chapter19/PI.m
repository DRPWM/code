function [subX, subY] = PI(SADvalue)
% (SADvalue) contains similarity coefficients of interest and (subX) and ...
% (subY) are the interpolated positions in units of pixels relative to ...
% the minimum pre-estimated position in lateral and axial directions.
    
% Lateral direction
if (SADvalue(2)+ SADvalue(3) ~= 2*SADvalue(1))
    subX = (SADvalue(2) - SADvalue(3)) / 2 ...
        / (SADvalue(2) + SADvalue(3) - 2*SADvalue(1));
else 
    subX = 0; 
end
    
% Axial direction
if (SADvalue(4)+ SADvalue(5) ~= 2*SADvalue(1))
    subY = (SADvalue(4) - SADvalue(5)) / 2 ...
        / (SADvalue(4) + SADvalue(5) - 2*SADvalue(1));
else
    subY = 0; 
end

end