% Selection changed function: ClinicButtonGroup
function ClinicButtonGroupSelectionChanged(app, event)
selectedButton = app.TownButtonGroup.SelectedObject;
if(strcmpi(selectedButton.Text, 'A'))
    % Show number of CT scans in clinic A 
    app.UIAxes.Children(1).Visible = 'on';
    app.UIAxes.Children(2).Visible = 'off';
elseif(strcmpi(selectedButton.Text, 'B'))
    % Show number of CT scans in clinic B
    app.UIAxes.Children(1).Visible = 'off';
    app.UIAxes.Children(2).Visible = 'on';
else 
    % Show number of CT scans in clinic A and B
    app.UIAxes.Children(1).Visible = 'on';
    app.UIAxes.Children(2).Visible = 'on';
end
end