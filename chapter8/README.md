# Chapter 8: Sharing software
The files in this folder are for the example of building an app in MATLAB using App Designer, as discussed in Chapter 8. The files are:

**CTScanData.mlapp** - a MATLAB app for generating a bar chart, showing the number of CT scans in two clinics between 2015 and 2018

**startupFcn.m** - a callback function as used by the app at startup. It contains the data, and generates the bar chart and labels. This file is included for illustrative purposes only as the function is also encapsulated in the app file (to see this open the app in App Designer and select *code view*)

**ClinicButtonGroupSelectionChanged.m** - a callback function as used by the app when a button selection is changed. It changes the data displayed in the bar chart. This file is included for illustrative purposes only as the function is also encapsulated in the app file (to see this open the app in App Designer and select *code view*)

To run the app, first make sure that the app is in the MATLAB working directory. Then simply type the app's name (without the file suffix):

`>> CTScanData`

The app can be opened in App Designer by simply double-clicking on the file in the Curent Folder window.

## Setup ##
A base install of MATLAB is required.

Required MATLAB toolboxes:  
- *None*

Tested with MATLAB R2019b.

## Contact ##
Piyush Khopkar  
pkhopkar@mathworks.com

## License ##
[MIT](https://choosealicense.com/licenses/mit/). See the license file in this folder (LICENSE.txt).
