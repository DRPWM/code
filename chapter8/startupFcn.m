function startupFcn(app)
% Create dummy data
y = [2 2; 2 5; 2 8; 11 12];

% Visualize data through bar chart
% app.UIAxes is an handle to the axes 
bar(app.UIAxes,y)

% Set XTickLabels
app.UIAxes.XTickLabel=  ['2015';'2016';'2017';'2018'];
end