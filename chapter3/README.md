# Chapter 3: Data sources in medical imaging
This demo executes the MATLAB code discussed in chapter 3. The script is:

**chapter3Demo.m** - basic DICOM functionality in MATLAB 

## Setup ##
A base install of MATLAB is required.

Required toolboxes:  
- *Image Processing Toolbox*

Tested with MATLAB R2019b.

## Contact ##
Robert Bujila  
work.robert.bujila@gmail.com

## License ##
[MIT](https://choosealicense.com/licenses/mit/). See the license file in this folder (LICENSE.txt).
