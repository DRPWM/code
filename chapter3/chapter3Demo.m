% Demonstration of code listed in chapter 3

disp('Load DICOM metadata')
dcmInfo = dicominfo('CT-MONO2-16-ankle.dcm');
disp('Display number of rows (note uint16 data type)')
rows = dcmInfo.Rows
disp('Perform a calculation (note unit16 data type in results')
midOfFirstPixel = -rows/2 + 0.5

disp('Recast rows to the double data type')
rows = double(dcmInfo.Rows)
disp('Re-do previous calculation (note expected results when using double')
midOfFirstPixel = -rows/2 + 0.5

disp('Load new Image with Private metadata')
dcmInfo = dicominfo('exampleImage.dcm');
disp('Extract the private attribute 10*Noise Index (attribute tag 0027,101f')
GE_NoiseIndex = double(typecast(dcmInfo.Private_0027_101f, 'int32'))./10