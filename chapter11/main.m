function main(input)
% inputs
% input: the path to the DICOM files of the test

% Get all objects in folder to be analysed
allPaths = dirPlus(input); % Function available in File Exchange (by K Eaton)
% Check if objects are DICOM and if they can be analysed
go = zeros(numel(allPaths),1); % Filetype vector; only analyse if go is = 1
for i = 1:numel(allPaths)
    imagePath = allPaths{i}; % Image path to image i
    if isdicom(imagePath) % If DICOM-file, proceed
            meta = dicominfo(imagePath); % Get DICOM header
            modality = meta.Modality;
            width = meta.Width;
            if strcmp(modality,'CT') && width == 512 % Hardcoded image size
                go(i) = 1; % A CT image with 512x512 pixels: ok
            end
            if meta.SeriesNumber==999 || meta.SeriesNumber==997 % GE
                go(i) = 2; % A dose screen or dose report, not ok
            end
            if meta.SeriesNumber==501 || meta.SeriesNumber==502 % Siemens
                go(i) = 2; % A dose screen or dose report, not ok
            end
    end
end
if sum(go==0)
    disp('No DICOM files in folder')
    return % Exit the MonitorCT script
end
if sum(go==0)>0 % Remove non-DICOM objects
    allPaths(go==0)=[];
    go(go==0)=[];
end
if sum(go==2)>0 % Remove dose screens
    allPaths(go==2)=[];
    go(go==2)=[];
end

% Get basic series data
imagePath = allPaths{1};
meta = dicominfo(imagePath); % meta is the DICOM header of image number 1.
scannerData.date = meta.(dicomlookup('0008','0020'));
scannerData.time = meta.(dicomlookup('0008','0030'));
scannerData.institutionName = meta.(dicomlookup('0008','0080'));
scannerData.stationName = meta.(dicomlookup('0008','1010'));
scannerData.seriesDesc = meta.(dicomlookup('0008','103E'));

% ROI placements relative to the phantom diameter
waterInput.positionUniformity = 0.15; % How close to phantom boarder ROI is placed
waterInput.diameterCentral = 0.4; % ROI size for noise test
waterInput.diameterUniformity = 0.1; % ROI size for CT number and uniformity tests
% Slice positions (all images treated as water if not having these image numbers)
positioningSlice = 1;
airSlice = 10;

allResults={}; % Cell for storage of relevant data and results
for imageNumber = 1:numel(allPaths) % A loop through all images of the test
disp(['Image ' num2str(imageNumber) ' out off ' num2str(numel(allPaths))])
    imagePath = allPaths{imageNumber}; % Path to image to be analyzed
    meta = dicominfo(imagePath); % DICOM header of the image to be analyzed
    allResults(imageNumber,1,1:3) = {'StationName','StudyDate','StudyTime'};
    allResults(imageNumber,2,1:3) = {meta.StationName,meta.StudyDate,...
        meta.StudyTime};  
    instanceNumber = meta.(dicomlookup('0020','0013')); % Image number
    HU = double(dicomread(imagePath))-1024; % Get image (CT images are rescaled)
    switch instanceNumber
        case positioningSlice % Analysis of an image of the positioning part
            imageType = 'positioning'; disp('Positioning')
            % Analysis for POSITION. Left out in this demonstration.   
            allResults(imageNumber,1,4) = {'imageType'};
            allResults(imageNumber,2,4) = {imageType};
        case airSlice % Analysis of an image of air
            imageType = 'air'; disp('Air')
            % Analysis for AIR. Left out in this demonstration.
            allResults(imageNumber,1,4) = {'imageType'};
            allResults(imageNumber,2,4) = {imageType};
        otherwise  % Analysis of an image of the water part
            imageType = 'water'; disp('Water')
            waterInput.HU = HU;
            waterInput.instanceNumber = instanceNumber;
            % Start analysis
            waterResults = water(waterInput); % The analysis function.
            % Store data and analysis for each analyzed image
            allResults(imageNumber,1,4) = {'imageType'};
            allResults(imageNumber,2,4) = {imageType};
            allResults(imageNumber,1,5:18) = fieldnames(waterResults)';
            allResults(imageNumber,2,5:18) = struct2cell(waterResults)';
    end
end

% Results to .csv
writecell(allResults,'results.csv')