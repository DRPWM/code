function waterResults = water(waterInput)
% Inputs
% waterInput.HU: CT image (2d array)
% waterinput.positionUniformity: ROI positioning for uniformity test
% waterInput.diameterCentral: ROI size for noise test
% waterInput.diameterUniformity: ROI size for CT number and uniformity test
% waterInput.instanceNumber: Image number

% Outputs
% waterResults: analysis data

HU = waterInput.HU;
positionUniformity = waterInput.positionUniformity;
diameterCentral = waterInput.diameterCentral;
diameterUniformity = waterInput.diameterUniformity;

phantomData = regionprops(HU>-300);
phantomCenter = phantomData(1).Centroid;
phantomSize = mean(phantomData(1).BoundingBox(3:4));

radiusCentral = diameterCentral/2*phantomSize;
radiusUniformity = diameterUniformity/2*phantomSize;
positionUniformity3 = phantomCenter+[(0.5-positionUniformity)*phantomSize 0];
positionUniformity6 = phantomCenter+[0 (0.5-positionUniformity)*phantomSize];
positionUniformity9 = phantomCenter-[(0.5-positionUniformity)*phantomSize 0];
positionUniformity12 = phantomCenter-[0 (0.5-positionUniformity)*phantomSize];

maskCentral = circleMask(phantomCenter, size(HU), radiusCentral); 
maskUniformityC = circleMask(phantomCenter, size(HU), radiusUniformity); 
maskUniformity3 = circleMask(positionUniformity3, size(HU), radiusUniformity); 
maskUniformity6 = circleMask(positionUniformity6, size(HU), radiusUniformity); 
maskUniformity9 = circleMask(positionUniformity9, size(HU), radiusUniformity); 
maskUniformity12 = circleMask(positionUniformity12, size(HU), radiusUniformity); 

waterResults.phantomCenterX = phantomCenter(1);
waterResults.phantomCenterY = phantomCenter(2);
waterResults.meanCentral = mean(HU(maskCentral));
waterResults.meanUniformityC = mean(HU(maskUniformityC));
waterResults.meanUniformity3 = mean(HU(maskUniformity3));
waterResults.meanUniformity6 = mean(HU(maskUniformity6));
waterResults.meanUniformity9 = mean(HU(maskUniformity9));
waterResults.meanUniformity12 = mean(HU(maskUniformity12));
waterResults.stdCentral = std(HU(maskCentral));
waterResults.stdUniformityC = std(HU(maskUniformityC));
waterResults.stdUniformity3 = std(HU(maskUniformity3));
waterResults.stdUniformity6 = std(HU(maskUniformity6));
waterResults.stdUniformity9 = std(HU(maskUniformity9));
waterResults.stdUniformity12 = std(HU(maskUniformity12));