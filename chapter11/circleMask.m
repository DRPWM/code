function outMask = circleMask(roiCenter, imageSize, radiusCentral)
% Inputs
% phantomCenter: center coordinates of the ROI
% imageSize: size of mask image (same as image size)
% radiusCentral: radius of the ROI
% Outputs
% outMask: mask with value of 1 within circular ROI (value 0 outside)

cx = roiCenter(1); % Center X-coordinate of wanted circle
cy = roiCenter(2); % Center Y-coordinate of wanted circle
ix = imageSize(1); % Number of columns in original image 
iy = imageSize(2); % Number of rows in original image
[x,y] = meshgrid(-(cx-1):(ix-cx), -(cy-1):(iy-cy));
outMask = ((x.^2+y.^2) <= radiusCentral^2);