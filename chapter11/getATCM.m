function [slicePositions, tubeCurrents, imageNoises] = getATCM(seriesPath)
% Inputs
% seriesPath: path to the folder with the CT scan
% Outputs
% slicePositions: slice positions
% tubeCurrents: tube currents

fileNames = dirPlus(seriesPath); % function available in File Exchange (by K Eaton)
slicePosition = nan(size(fileNames,1),1);
tubeCurrent = nan(size(fileNames,1),1);
imageNoise = nan(size(fileNames,1),1);
for i=1:size(fileNames,1)
    imagePath = fileNames{i};
    HU = double(dicomread(imagePath))-1024; % Get image (CT images are rescaled)
    dicommeta = dicominfo(imagePath);
    slicePosition(i) = dicommeta.SliceLocation;
    tubeCurrent(i) = dicommeta.XRayTubeCurrent;
    imageNoise(i) = getATCMImageNoise(HU);
end
% Sort tube currents
[slicePositions,I] = sort(slicePosition);
tubeCurrents = tubeCurrent(I);
imageNoises = imageNoise(I);