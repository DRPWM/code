# Chapter 11: Automating quality control tests and evaluating ATCM in computed tomography
The demo scripts in this folder execute the code discussed in chapter 11. The scripts include:

**demoQC.m** - example of an automated QC in CT using the example images in the IQPhantom folder

**demoATCM.m** - example of evaluating autoamtic tube current modulation using images of an ATCM phantom in the ATCMPhantom folder

## Setup ##
A base install of MATLAB is required.

Required toolboxes:  
- *Image Processing Toolbox*

Additional requirements:

- [dirPlus](https://www.mathworks.com/matlabcentral/fileexchange/60716-dirplus) by K Eaton on the MATLAB File Exchange

Tested with MATLAB R2019b.

## Contact ##
Patrik Nowik  
patrik.nowik@ki.se

## License ##
[MIT](https://choosealicense.com/licenses/mit/). See the license file in this folder (LICENSE.txt).
