function imageNoise = getATCMImageNoise(HU)
% Inputs
% HU: image
% Outputs
% imageNoise: image noise

diameterCentral = 0.4; % ROI 40 % of phantom diameter
phantomData = regionprops(HU>-300);
[~, maxAreaIndex] = max([phantomData.Area]);
if phantomData(maxAreaIndex).Centroid(2)>384
    imageNoise = nan; % If no phantom, set noise to nan
else
    phantomCenter = phantomData(maxAreaIndex).Centroid;
    phantomSize = mean(phantomData(maxAreaIndex).BoundingBox(3:4));
    radiusCentral = diameterCentral/2*phantomSize; 
    maskNoise = circleMask(phantomCenter,size(HU),radiusCentral); 
    imageNoise = std(HU(maskNoise));
end