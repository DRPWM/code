clear all
close all
clc

disp('Running demo. Please be patient, this may take a minute or two!')

seriesPath = fullfile('ExampleDataChapter11', 'ATCMPhantom');

% Create synthetic localizer radiograph
[rawDicomVolume, ~, ~] = dicomreadVolume(seriesPath); % Read volume
dicomVolume = squeeze(rawDicomVolume); % Remove unused dimension
% Calculate SLR by calculating mean along 1st dim (ignoring nans)
syntheticLocalizerRadiograph = squeeze(nanmean(dicomVolume,1));

[slicePositions, tubeCurrents, imageNoises] = getATCM(seriesPath);

% Plot ATCM curves on top of synthetic localizer radiograph
image2plot = syntheticLocalizerRadiograph;

% Find range of slice positions for x-axis of image
slicePositionMin = min(slicePositions); 
slicePositionMax = max(slicePositions); 
slicePositionSteps = size(image2plot,2); % Image size in X-direction
imageXRange = linspace(slicePositionMin, slicePositionMax, slicePositionSteps);

% Find range of tube currents for y-axis of image
tubeCurrentMin = min(min(tubeCurrents))-50; 
tubeCurrentMax = max(max(tubeCurrents))+50;
tubeCurrentSteps = size(image2plot, 1); % Image size in Y-direction
imageYRange = linspace(tubeCurrentMin, tubeCurrentMax, tubeCurrentSteps);

% Find pixel-value range for setting window settings
pixelRange = [min(min(image2plot)), max(max(image2plot))];

% Plot image first, followed by tube currents plotted over the image
imagesc(imageXRange, imageYRange, image2plot, [pixelRange(1)*1.2 pixelRange(2)])
colormap gray; hold on
plot(slicePositions, tubeCurrents, '.'); hold off % Plot mA on top
figureAxis = gca;
figureAxis.YDir = 'normal'; % Y-axis direction opposite when using imagesc
xlabel('Slice position [mm]')
ylabel('Tube current [mA]')