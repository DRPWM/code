# Chapter 7: Good programming practices
The demo scripts in this folder are for the script-based unit testing example discussed in Chapter 7. The scripts are:

**addition.m** - a MATLAB function returning the output of an addition operation on two numbers. The function throws an error if the type of the two numbers is not *real scalar double*

**testAddition.m** - a script-based unit test example testing the addition function

The test is run in MATLAB with following syntax:

`>> testResults = runtests('testAddition')`

This should produce an output like (all 5 tests should pass):

    testResults =

      1x5 TestResult array with properties:

        Name  
        Passed  
        Failed  
        Incomplete  
        Duration  
        Details  

      Totals:  
        5 Passed, 0 Failed, 0 Incomplete.  
        0.082361 seconds testing time.

## Setup ##
A base install of MATLAB is required.

Required MATLAB toolboxes:  
- *None*

Tested with MATLAB R2019b.

## Contact ##
Piyush Khopkar  
pkhopkar@mathworks.com

## License ##
[MIT](https://choosealicense.com/licenses/mit/). See the license file in this folder (LICENSE.txt).
