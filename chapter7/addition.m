%% Description of function- addition
% ----------------------------------

% Function name: addition.m
% Version: 1.0 
% By Piyush Khopkar & Yanlu Wang, 2019

% This function takes two inputs a and b. The function outputs a variable
% 'result' of type double. Valid inputs to the function are real
% scalar doubles 
% ----------

function result = addition(a,b)
% Check input types of a and b. Throw error if input is invalid
if(isa(a,'double') && isreal(a) && isscalar(a) && ...
        isa(b,'double') && isreal(b) && isscalar(b))
    result = a + b;
else
    error('One or both inputs are invalid. Input should be a real scalar double.');
end
end