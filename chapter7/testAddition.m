% Description of the script testAddition.m
% ---------

% Script name- testAddition.m
% Version- 1.0 
% By Yanlu Wang and Piyush Khopkar, 2019

% testAddition.m is a test script to test addition.m function
% ----------

% Test addition 
set1InputA = 2; 
set1InputB = 4; 

set2InputA = 0.20; 
set2InputB = 10; 

set3InputA = 2; 
set3InputB = 10.12; 

set4InputA = -10; 
set4InputB = 20;

set5InputA = -10; 
set5InputB = -20;

%% Test 1: sum when both inputs integer
result = addition(set1InputA, set1InputB); 
assert(result == 6); 

%% Test 2: sum when input 'a' is integer and 'b' is float
result = addition(set2InputA, set2InputB); 
assert(result == 10.20); 

%% Test 3: sum when input 'a' is float and 'b' is integer
result = addition(set3InputA, set3InputB); 
assert(result == 12.12);

%% Test 4: sum when input 'a' is negative and 'b' is positive integer
result = addition(set4InputA, set4InputB); 
assert(result == 10);

%% Test 5: sum when both inputs are negative integers
result = addition(set5InputA, set5InputB); 
assert(result == -30);
