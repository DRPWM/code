# Chapter 13: Method of determining patient size surrogates using CT images
The demo script in this folder execute the code discussed in Chapter 13. The script is:

**main.m** - a script to analyze the images in the dcmData folder and derive size metrics for the calculation of the SSDE

When run, the "main" script calls the two other scripts: "calcSizeMetrics" and "calcSSDE". Example CT data to analyze is supplied in the "dcmData" folder (of the ATCM phantom described in Chapter 11 rather than genuine patient data).

## Setup ##
A base install of MATLAB is required.

Required toolboxes:  
- *Image Processing Toolbox*

Tested with MATLAB R2019b.

## Contact ##
Christiane Sarah Burton  
christiane.burton@childrens.harvard.edu

## License ##
[MIT](https://choosealicense.com/licenses/mit/). See the license file in this folder (LICENSE.txt).
