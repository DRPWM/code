% Reorder the slices in each exam so we go from low to high z-position
for iExam=1:length(exam)
  [~,b] = sort(exam(iExam).sliceLocation); % Array b holds the reordering
  % Iterate through the fieldnames of the structure
  for fieldName = fieldnames(exam(iExam))'
    fieldValues=exam(iExam).(fieldName{1}); 
    % Reorder only if the field has a value for each slice 
    if numel(fieldValues)==numel(b) 
      exam(iExam).(fieldName{1}) = fieldValues(b);
    end
  end
end

% For each exam, average the size metric and CTDIvol over middle 20% of scan
for iExam = 1:length(exam) 
    numberSlices=numel(exam.sliceLocation);
    index1 = round(numberSlices/2 - 0.2*numberSlices/2);
    index2 = round(numberSlices/2 + 0.2*numberSlices/2);
    midDiaEff(iExam) = mean(exam(iExam).diaEff(index1:index2));
    midDiaWaterEq(iExam) = mean(exam(iExam).diaWaterEq(index1:index2));
    midCtdiVol(iExam) = mean(exam(iExam).ctdiVol(index1:index2));
end

% Fit coefficients from AAPM Report 204 (for CTDIvol 32cm)
a = 3.704369;
b = 0.03671937*0.1;

% Estimate the SSDE using effective diameter
ssdeDiaEff = a*exp(-b*midDiaEff).*midCtdiVol; % mGy
% Estimate the SSDE using water equivalent diameter
ssdeDiaWaterEq = a*exp(-b*midDiaWaterEq).*midCtdiVol; % mGy