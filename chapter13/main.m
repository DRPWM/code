% Locate the directory where the patient folders are stored
% Note, the script is expecting image data in the following directory 
% structure: dataLocation/patient_x/image_x.
defaultDirectory = '';
dataLocation = uigetdir(defaultDirectory);

% Whether to remove table: 'y' (yes) or 'n' (no)
tableRemoval = 'y'; 

% Exit if user aborted the uigetdir dialog
if dataLocation==0, return, end 

% Store the path to each folder
folderNames = dir(dataLocation);

iExam = 1; %start exam count
for iFolder = 1:(size(folderNames,1)) %loop over all folders in dataLocation

  % Ignore top-level (.) and parent (..) folders for dataLocation
  if strcmp(folderNames(iFolder).name, '.') || ...
    strcmp(folderNames(iFolder).name, '..')
    continue
  end
    
  % Store the path to each file in the folder
  fileNames = dir(fullfile(dataLocation, folderNames(iFolder).name));

  iSlice = 1; % Start slice count
  for iFile = 1:(size(fileNames,1)) % Loop over all files in a folder

    % Ignore top-level (.) and parent (..) folders of fileNames
    if strcmp(fileNames(iFile).name, '.') || ...
      strcmp(fileNames(iFile).name, '..')
      continue
    end

    % Full path to DICOM file
    file = fullfile(dataLocation,folderNames(iFolder).name,fileNames(iFile).name);
    % Read DICOM meta-data
    dcmInfo = dicominfo(file);
    % Read DICOM image and convert to CT-numbers
    dcmImage = dcmInfo.RescaleSlope.*double(dicomread(file)) ...
      + dcmInfo.RescaleIntercept;
    
    % Only analyze the file if it is an axial slice
    if (count(dcmInfo.ImageType,'ORIGINAL\PRIMARY\AXIAL')>0)  
      calcSizeMetrics; % Execute MATLAB script, calcSizeMetrics.m
      iSlice = iSlice + 1; % Increment slice count
    end             
    
  end 

  iExam = iExam + 1; % Increment folder count
end 

clearvars -except exam % Clean up MATLAB workspace

calcSSDE; % Execute MATLAB script, calcSSDE.m 