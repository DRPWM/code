% Set any padded values to CT-number of air
dcmImage(dcmImage <= -1024) = -1000;
% Make a binary image of the patient
axialImageThresh=dcmImage;
axialImageThresh(axialImageThresh <= -150) = 0; % Exclude low densities
axialImageBin(axialImageThresh ~= 0) = 1; % Include the rest

% Exclude connected components smaller than 5000 pixels (e.g. table)
axialImageBinCR = bwareaopen(axialImageThresh,5000);

% Binary projection along columns and width calculated from non-zero 
% elements of projection
projAP=sum(axialImageBinCR,2)>0;
widthAP = sum(projAP)*dcmInfo.PixelSpacing(1);
% Binary projection along rows and width calculated from non-zero 
% elements of projection
projLAT=sum(axialImageBinCR,1)>0;
widthLAT = sum(projLAT)*dcmInfo.PixelSpacing(2);
 
% Effective diameter is calculated using two formulas given in 
% AAPM Report 204
diaEff1 = sqrt(widthLAT*widthAP); % Geometric mean 
diaEff2 = (widthLAT + widthAP)/2; % Arithmetic mean
    
ellipRat = widthLAT/widthAP; % Ellipticity ratio    
    
% Make a new binary image of patient including low densities inside patient
if tableRemoval == 'y' % Option to remove background and table from image
  % Fill in the low density regions inside patient e.g. lungs
  binImage = imfill(axialImageBinCR,'holes'); 
  areaROI = sum(sum(binImage)) ...
    *dcmInfo.PixelSpacing(1)*dcmInfo.PixelSpacing(2);
elseif tableRemoval == 'n' % Option to keep background and table
  % Loop through pixels in image
  imSize=size(dcmImage);
  binImage=zeros(imSize,'logical');
  for iRow=1:imSize(1)
    for iCol=1:imSize(2)
      if ((iRow-imSize(1)/2)^2+(iCol-imSize(2)/2)^2 < (imSize(1)/2)^2)
        binImage(iRow,iCol)=1; % Assign a non-zero value if inside FOV
      end
    end
  end
  % Area of the FOV
  areaROI = pi* (double(dcmInfo.Width)/2)^2 ...
    *double(dcmInfo.PixelSpacing(1))*double(dcmInfo.PixelSpacing(2));    
end        

% Mean CT number in patient
meanCTNum = mean2(dcmImage(binImage));

% Water equivalent area calculation formula from AAPM Report 220
areaWaterEq = .001 * meanCTNum * areaROI + areaROI; 

% Water equivalent diameter (WED) calculation
diaWaterEq = 2 * (areaWaterEq / pi)^(1/2); 
        
% Store data in a structure called exam
% This structure is dynamically extended as iExam increases
exam(iExam).sliceLocation(1,iSlice) = dcmInfo.SliceLocation;
exam(iExam).diaWaterEq(1,iSlice) = diaWaterEq;
exam(iExam).ellipRat(1,iSlice) =ellipRat;
exam(iExam).diaEff(1,iSlice) = diaEff1;
exam(iExam).widthLAT(1,iSlice) = widthLAT;
exam(iExam).widthAP(1,iSlice) = widthAP;
exam(iExam).ctdiVol(1,iSlice) = dcmInfo.CTDIvol;
% Store entire DICOM header as a cell
exam(iExam).dicomHeaderAxial(1,iSlice) = {dcmInfo}; 
