# Chapter 16: xrTk: a MATLAB toolkit for x-ray physics calculations
The demo scripts in this folder are for the code discussed in Chapter 16, using the xrTk toolkit. The scripts are:

**spectrum.m** - example generating a spectrum at a specified tube potential and calculating associated metrics such as air kerma and half-value-layer

**spectrumiec.m** - example generating a standard IEC spectrum and calculating associated metrics such as air kerma and half-value-layer

**xraycoef.m** - example returning interpolated x-ray interaction coefficients for an element

**xraycoefformula.m** - example returning interpolated x-ray interaction coefficients based on a specified chemical formula

**transmission.m** - example illustrating how to calculate transmission of an x-ray spectrum through an attenuator

**airkerma.m** - example illustrating how to scale a spectrum to produce a specified air kerma value

**contrast.m** - example demonstrating the calculation of iodine contrast in soft-tissue (CsI-based detector)

**snrrose.m** - example demonstrating the calculation of Rose signal-to-noise ratio for iodine in soft-tissue (CsI-based detector)

## Setup ##
A base install of MATLAB is required.

Required MATLAB toolboxes:  
- *None*

Tested with MATLAB R2019b.

Additional requirements:  
- The [xrTk MATLAB toolkit](http://dqe.robarts.ca/icunningham/xrTk) must be downloaded and added to the MATLAB path (available under the GPLv3.0 open-source license).

A customized version of xrTk has been included in this bitbucket repository. This has two minor bug fixes that allow the examples scripts to run. When Ian Cunningham's GitLab repository for xrTk is updated this will be removed.

## Contact ##
Ian Cunningham  
icunningham@robarts.ca

## License ##
[MIT](https://choosealicense.com/licenses/mit/). See the license file in this folder (LICENSE.txt).

Note, however, that the customized version of xrTk in this repository is provided under the [GPLv3](https://choosealicense.com/licenses/gpl-3.0/) license.
