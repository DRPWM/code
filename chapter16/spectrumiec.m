% Generation of standard IEC x-ray spectra

E = 20:.25:100;
spectrumName = 'RQA-5'; iecVersion = 2015;
qi = xrSpecIec(E, spectrumName, iecVersion);
hvl = xrHvl(E, qi);
K = xrAirKerma(E, qi)*1e6;
Qo = sum(qi)/K;