% Scale spectrum qo to 2.5 uGy air KERMA and calculate Qo (quanta/mm2 per Gy)

E = 10:.25:120; kV = 90;
qi0 = xrSpec(E, kV);

f = 2.5e-6/xrAirKerma(E, qi0); % Conversion factor to 2.5 uGy air KERMA
qi = qi0*f; % Normalized spectrum with 2.5 uGy air KERMA

K = xrAirKerma(E, qi)*1e6;
Qo = sum(qi)/K;