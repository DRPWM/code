% Interpolation of x-ray interaction coefficients

E = 1:.01:50;
material = 'iodine';
mu = xrCoef(E, material, 'attenuation'); % Mass attenuation coefficient of iodine