% Transmission of x-ray beam with spectrum q0 through an attenuator

E = 10:.25:120; kV = 90;

AlInfo = xrMaterialInfo('aluminum');  % Attenuator material data
AlThickness = 10.;  % Attenuator thickness [cm]
qi0 = xrSpec(E, kV);  % Spectrum incident on attenuator
massLoading = AlThickness*AlInfo.m_density; % Mass-loading [cm2/g]
qi = qi0.*xrTrans(E, 'aluminum', massLoading);  % Transmitted spectrum 
