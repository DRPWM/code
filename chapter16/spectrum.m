% Generation of x-ray spectra

dE = 0.25; EMin = 10; EMax = 120; % keV
E = EMin:dE:EMax;

kV = 90; target = 'W'; % 90 kV, tungsten target (radiography) 
qi = xrSpec(E, kV, target);
hvl = xrHvl(E, qi);
K = xrAirKerma(E, qi)*1e6;
Qo = sum(qi)/K;