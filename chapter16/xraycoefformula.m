% Interpolation of x-ray interaction coefficients based on chemical formula

E = 1:.01:50;
formula = 'Gd2O2S';
mu = xrCoefFormula(E, formula, 'attenuation');