%   xrFormula2Elements(formula) Returns structure with formula info
%
%   Routine to accept a chemical formula writen as a string (such as 'CsI4')
%   and return a structure with the following information:
%   elements().element = abreviated element name
%   elements().Natoms  = number of element atoms in compound
%
%   Updated October 2018 by I.A. Cunningham and T.F. Nano


function [elements] = xrFormula2Elements(formula)

UPPER = 1;
LOWER = 2;
NUMERIC = 3;
%formula = 'CsI3959C10Df5';

uppercase=['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'];
lowercase=['a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z'];
number = ['1','2','3','4','5','6','7','8','9','0'];

elements = [];
charType = [];
for k = 1:length(formula),
    if length(find(formula(k) == uppercase)) > 0,
        charType(k) = UPPER;
    elseif length(find(formula(k) == lowercase)) > 0,
        charType(k) = LOWER;
    elseif length(find(formula(k) == number)) > 0,
        charType(k) = NUMERIC;
    else
        charType(k) = 0;
    end
    if isequal(formula(k),'('),
        fprintf('The chemical formula %s contains a bracket which is not supported\n', formula);
        return;
    end
end

if charType(1) ~= UPPER,
    fprintf('Something wrong, first letter is not upper case\n');
    return;
end

 
Nelement = 0;
Natoms = 0;
for k = 1:length(formula),
    if charType(k)==UPPER,
        if Nelement > 0,
            if Natoms == 0,
                Natoms = 1;
            end
            elements(Nelement).element = element;
            elements(Nelement).Z = xrGetZ(element);
            elements(Nelement).Natoms = Natoms;
%            fprintf('%s %d\n', elements(Nelement).element, elements(Nelement).Natoms);
        end
        Nelement = Nelement + 1;
        element = formula(k);
        Natoms = 0;
    elseif charType(k) == LOWER,
        element = [element formula(k)];
    elseif charType(k) == NUMERIC,
        Natoms = Natoms * 10 + (double(formula(k)) - double('0'));
    end
    if k == length(formula),
        if Natoms == 0,
            Natoms = 1;
        end
        elements(Nelement).element = element;
        elements(Nelement).Z = xrGetZ(element);
        elements(Nelement).Natoms = Natoms;
%        fprintf('%s %d\n', elements(Nelement).element, elements(Nelement).Natoms);
    end
end

return
