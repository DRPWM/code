%   xrCoef(E,material[,coefficient,plotresult])  Return vector of x-ray coefficients 
%
%   using   NIST for attenuation and absorption (cm2/g)
%           XCOM for interaction coefficients (barns/atom)
%
% Photons.txt files contain 8 columns:
% 1 Photon energy in MeV
% 2 Coherent cross section barns/atom
% 3 Incoherent cross section barns/atom
% 4 Photoelectric cross section barns/atom
% 5 Pair Production in nuclear field barns/atom
% 6 Pair Production in electron field barns/atom
% 7 Total attenuation with coherent scattering barns/atom
% 8 Total attenuation without coherent scattering barns/atom
%
% E_abs.txt files contain 3 columns:
% 1 Photon energy in MeV
% 2 Mass attenuation coefficient cm2/g
% 3 Mass energy absorption coefficient cm2/g
%
%   Returns a vector of x-ray coefficient values at energies E (keV)
%
%   E (keV) can be a single value or a vector of increasing values.  E
%       may start at any energy but must have uniform spacing.
%   material can be any material in NIST database
%       eg. 'hydrogen', 'water', 'iodine', ...
%       (see tables/NIST/)
%   coefficient (optional) can be 'sigmaCoherent', 'sigmaIncoherent', 
%       'sigmaPhotoelectric', 'attenuation' or 'absorption'
%   If not provided, 'attenuation' is assumed.
%   Coefficients can also be 'coherent', 'incoherent' or 'photoelectric'
%   which are determined from the corresponding cross sections scaled
%   by the atomic density to give cm2/g.
%
%   plotresult (optional) if set to 1 will plot the coefficient comparing the original
%       table values as a solid line and the interpolated values as circles.
%
%   Version:
%   1.0 October 2018 Initial version from xrlib by I.A. Cunningham and T.F. Nano
%

function [coef_i] = xrCoef(E, material, varargin)

if nargin > 2
    coef_type = varargin{1};
else
    coef_type = 'attenuation';
end
if nargin > 3
    plotresult = varargin{2};
else
    plotresult = 0;
end
nEbins = length(E);

% Verify that the energies in E are between 1 keV and 100 MeV, the range
% of the coefficients in the .tab files.

if or(E(1)<1,E(nEbins)>100000)
    error('Energy exceeds valid range of 1 keV to 100 MeV in coefficient tables');
end


if ~ischar(material)
    Z = material;
    material = xrElementName(Z);
end


% Attenuation and absorption coefs are in a different file to interaction
% cross sections coefs.

Wa = 0;         % atomic weight required for interaction coefs units
attenfile = false;
switch coef_type
    case 'coherent'
        kenable = 0;
        materialInfo = xrMaterialInfo(material);
        Wa = materialInfo.a_weight;
    case 'incoherent'
        kenable = 0;
        materialInfo = xrMaterialInfo(material);
        Wa = materialInfo.a_weight;
    case 'photoelectric'
        kenable = 1;
        materialInfo = xrMaterialInfo(material);
        Wa = materialInfo.a_weight;
    case 'pair'
        kenable = 0;
        materialInfo = xrMaterialInfo(material);
        Wa = materialInfo.a_weight;
    case 'attenuation'
        kenable = 1;
        attenfile = true;
    case 'transfer'
        kenable = 1;
    case 'absorption'
        kenable = 1;
        attenfile = true;
    case 'Etransfer'
        kenable = 1;
    case 'Eabsorption'
        kenable = 1;
    case 'sigmaCoherent'
        kenable = false;
    case 'sigmaIncoherent'
        kenable = false;
    case 'sigmaPhotoelectric'
        kenable = true;
    otherwise
        error('Undefined coefficient type');
end


% Look for datafile
material = material(~isspace(material));    % remove spaces
fmaterial = material; % Line edited by G. Poludniowski (10-11-2020)
fmaterial(1) = upper(fmaterial(1));
if attenfile
    filename = sprintf('%s Photons E_Abs.txt', fmaterial);
else
    filename = sprintf('%s Photons.txt', fmaterial);
end
fid = fopen(filename,'r');
if fid <= 0
    error('Could not open data file: %s',filename);
end

% First line is material name

materialFileName = fgets(fid);

% In the data files if there are two entries at the same energy indicates edge energies.

maxRowCount = 100;
coef = zeros(1, maxRowCount);
coefE = zeros(1, maxRowCount);
edge = zeros(1, maxRowCount);
fE = zeros(1, maxRowCount);
prevE = 0;
n_edges = 0;
j = 0;
rowCount = 0;

while (~feof(fid))
    rowCount = rowCount + 1;
    line = fgets(fid);
    s = sscanf(line, '%f');
    coefE(rowCount) = s(1) * 1000.0;
    
    % Check to see if this is an edge energy.  Recognize edge as a repeat
    % in the energy value.  The first energy is always considered
    % to be an edge.

    if and(rowCount>1, round(coefE(rowCount),10) == round(prevE,10))
        n_edges = n_edges + 1;  % Count and record edge energies
        edge(n_edges) = rowCount;

        if kenable
            j = j + 1;
        end
    else
        j = j + 1;      % Always increase j if this is not an edge energy
    end
    
    fE(j) = coefE(rowCount);      % keV
    NAb = 6.02252e23 * 1e-24;
    switch coef_type
        case 'sigmaCoherent'
            coef(j) = s(2);
        case 'sigmaIncoherent'
            coef(j) = s(3);
        case 'sigmaPhotoelectric'
            coef(j) = s(4);
        case 'coherent'
            coef(j) = s(2) * NAb / Wa;
        case 'incoherent'
            coef(j) = s(3) * NAb / Wa;
        case 'photoelectric'
            coef(j) = s(4) * NAb / Wa;
        case 'attenuation'
            coef(j) = s(2);
        case 'absorption'
            coef(j) = s(3);
        otherwise
            error('Undefined coefficient type');
    end
    prevE = coefE(rowCount);
end
fclose(fid);
n_coefs = j;

% If no edges were found (low Z material), disable kenable

if n_edges == 0
    kenable = 0;
end

% Check the input array of coefficients for reasonableness.  Init the
% interpolation limits to the first non-zero coef values.

ilow_initial = n_coefs;
ihigh_initial = 1;
for i=1:n_coefs
    if coef(i) > 0
        ilow_initial = i;
        break;
    end
end
for i=n_coefs:-1:1
    if coef(i) > 0
        ihigh_initial = i;
        break;
    end
end

%  For each value to be interpolated, search over energies below and
%  above to determine the two nearest shell edges.  These determine the
%  limits ilow and ihigh used in the interpolation routine.

%  If next energy bin from k-edge energy is less than 1%, do not include in
%  interpolation

coef_i = zeros(size(E));
pilow = 0;
Kflag = 0;     % flag to check next energy bin after K-edge
for i=1:nEbins
    ilow = ilow_initial;
    ihigh = ihigh_initial;
    if and(E(i) >= fE(ilow), E(i) <= fE(ihigh))
        if kenable
            for j=n_edges:-1:1
                if E(i) >= fE(edge(j))
                    break;
                end
            end
            
            if E(i) < fE(edge(1))
                j = 0;
            else
                ilow = edge(j);
                % check if Ebin after K-edge is 1% smaller than next
                if (fE(ilow+1) - fE(ilow)) < 0.01*(fE(ilow+2)-fE(ilow))
                    Kflag = 1;
                end
            end
            if j < n_edges
                ihigh = edge(j+1)-1;
            end
        end
        
        EnergyForFit = fE(ilow:ihigh);
        CoefForFit = coef(ilow:ihigh);
        
        % do not include next data point if it is very close to K-edge energy
        if Kflag
            EnergyForFit(2) = [];
            CoefForFit(2) = [];
        end
                
        int_tab_size = ihigh-ilow+1;
        if (int_tab_size == 1)
            coef_i(i) = E(i);
        elseif int_tab_size >= 2
            if ilow ~= pilow
                logfEtab = log(EnergyForFit);
                logcoeftab = log(CoefForFit);
                pp = spline(logfEtab,logcoeftab);
                pilow = ilow;
            end
            coef_i(i) = exp(ppval(pp,log(E(i))));
        else
            coef_i(i) = 0;
        end
    else
        error('xraylib::xrCoefNist(): Energy (%.1f keV) is outside of available data in data file for %s\n', E(i),fname);
    end
end

if plotresult == 1
    figure;
    semilogy(E,coef_i, fE(1:n_coefs),coef(1:n_coefs),'o');
    axis([0 max(E)+.001 min(coef_i)/2 max(coef_i)*2]);
    s = sprintf('Mass %s coefficient for %s',coef_type,material);
    title(s);
    legend('Interpolated curve','Raw data table values');
    xlabel('Energy (keV)');
    ylabel('Mass coefficient (cm2/g)');
    grid on;
end

return
