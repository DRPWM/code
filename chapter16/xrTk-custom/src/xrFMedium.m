%   xrFMedium(E,medium) Returns vector of absorbed dose in a medium (Gy) per unit exposure (R)
%
%   Returns vector of f-med values, the absorbed dose in a medium (Gy) per unit
%   exposure (R), at energies E.  See Eq 9-5 of J&C in 3rd Version
%
%   Updated October 2018 by I.A. Cunningham and T.F. Nano



function [fMedium] = xrFMedium(E, medium)

% fAir = energy absorbed by 1 g of air (rads) exposed to 1 R of exposure

fAir = 0.869;
fMedium = fAir * xrCoef(E, medium, 'absorption') ./ xrCoef(E, 'air', 'absorption') / 100;

return
