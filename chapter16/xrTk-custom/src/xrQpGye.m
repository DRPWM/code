%   XRQPGYE(E, medium) Get quanta per mm2 per Gy of absorbed dose in specified medium
%
%   Returns a vector of number of quanta per mm2 per Gy of absorbed dose in specified
%   medium at the energies specified by vector E (keV).  If no medium is
%   specified, water is assumed.
%
%   Updated October 2018 by I.A. Cunningham and T.F. Nano

function [qpGye] = xrQpGye(E, medium)

E_CHG = 1.6021892E-19;   % charge on an electron (C)
C_kg_R = 2.580E-4;       % Coul/kg/R for air
W_air = 33.97;           % work function for air (eV)

% Calculate quanta/R values

airabs = xrCoef(E, 'air', 'absorption');
f = W_air * C_kg_R * 10.0 / (E_CHG * 1E9);
qpRe = f ./ airabs ./ E;

% Calculate quanta/Gy values

qpGye = qpRe ./ xrFMed(E, medium);

return