%   xrGetGpcm2(E,phi,material,transmission) Calculates the g/cm2 for ...
%   transmission factor
%
%   Calculates the g/cm2 of a specified material that results in a 
%   given exposure transmission factor.
%
%   Updated October 2018 by I.A. Cunningham and T.F. Nano

function [gpcm2] = xrGetGpcm2(E,phi,material,transmission)
    e1 = xrExposure(E,phi);		% Initial exposure
    eh = e1 * transmission;		% Target exposure
    al1 = 0.0;
    al2 = 0.0;
    ald = 0.01;                 %thickness increment
    while abs(ald) > 0.0001

        al2 = al2 + ald;
        if al2 <= 0
            al2 = 0.01;
        end

        phi2 = phi .* xrTrans(E, material, al2);

        e2 = xrExposure(E,phi2);

        if e2 == e1
            gpcm2 = -1;
            disp('got bang-on match')
            return
        end

        ald = (eh-e2) * (al2-al1)/(e2-e1);
        e1 = e2;
        al1 = al2;
    end
    gpcm2 = al2;
return
