%   xrCoefFormula(E,formula[,coefficient,plotresult])  Return vector of x-ray coefficients
%   for a compound material described by chemical formula.
%
%   Returns a vector the size of E of coefficient values (cm2/g or barns/atom) at energies 
%   E (keV) for a compound material described by chemical formula.
%
%   See xrcoef for details on argument list.
%
%   Formula is a string such as 'CsI' or 'Gd2O2S'
%
%   Updated October 2018 by I.A. Cunningham and T.F. Nano


function [coef_i] = xrCoefFormula(E,formula,varargin);

if nargin > 2
    coef_type = varargin{1};
else
    coef_type = 'attenuation';
end
if nargin > 3
    plotresult = varargin{2};
else
    plotresult = 0;
end


% Loop over elements in the formula

elements = xrFormula2Elements(formula);

for i = 1:length(elements),
    Info = xrMaterialInfo(elements(i).Z);
    elements(i).a_weight = Info.a_weight;
end

m_weight = sum([elements(:).a_weight] .* [elements(:).Natoms]);

coef_i = zeros(size(E));
for i=1:length(elements),
    elements(i).F = elements(i).Natoms * elements(i).a_weight / m_weight; 
    
    % abs and atten coef is a weighted sum based on element molecular weight 
    if(strcmpi(coef_type, 'absorption')  || strcmpi(coef_type, 'attenuation'))
        coef_i = coef_i + elements(i).F * xrCoef(E,elements(i).Z, coef_type, plotresult); % Line edited by G. Poludniowski (10-11-2020)
    % interaction coef (barns/atom) sum based on number of atoms
    else
        coef_i = coef_i + elements(i).Natoms * xrCoef(E,elements(i).Z, coef_type, plotresult); 
    end
    
end


