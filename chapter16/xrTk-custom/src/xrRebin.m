%   XRREBIN(Eout, Ein, Sin) Rebins a spectrum from one set of energy bins to another
%
%   Returns the rebinned spectrum PHIout.  Both Ein and Eout must be 
%   uniformly spaced energy bins.
%
%   Updated October 2018 by I.A. Cunningham and T.F. Nano 2018


function Sout = xrRebin(Eout, Ein, Sin)

E0in = Ein(1);
E0out = Eout(1);
deltaEin = Ein(2) - Ein(1);
deltaEout = Eout(2) - Eout(1);
F = deltaEout / deltaEin;
Nout = length(Eout);
Nin = length(Ein);
Sout = zeros(Nout,1);

if deltaEout > deltaEin
    for i=1:Nin
        El = Ein(i) - deltaEin/2;
        Eh = El + deltaEin;
        Blf = (El-((E0out-deltaEout/2)-(E0in-deltaEin/2))) / deltaEout + 1.00000001;
        Bhf = (Eh-((E0out-deltaEout/2)-(E0in-deltaEin/2))) / deltaEout + 1.00000001;

        Bl = floor(Blf);
        Bh = floor(Bhf);

        if Bl == Bh
            Wl = 1;
            Wh = 0;
        else
            Wl = (Bh-Blf) * F;
            Wh = (Bhf-Bh) * F;
        end

        if and(Bl>0,Bh<=Nout)
            Sout(Bl) = Sout(Bl) + Wl*Sin(i);
            Sout(Bh) = Sout(Bh) + Wh*Sin(i);
        end
    end
else
    Sout = spline(Ein,Sin,Eout) * F;
    [j,k] = find(Sout < 0);
    Sout(j(:),k(:)) = 0;
end
