%   xrGypQe(E, medium) Returns vector of KERMA distribution
%
%   Returns vector of KERMA distribution (Gy / mm2 / interacting-quanta / 
%   energy-bin) in specified medium at energies E (keV).
%
%   See Eq. 9-4 in J&C 3rd Ed
%
%   Updated October 2018 by I.A. Cunningham and T.F. Nano

function [GypQe] = xrGypQe(E, medium)

dE = E(2)-E(1);
GypQe = xrRpQe(E) .* xrFMedium(E, medium);

return