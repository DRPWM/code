%   XRDOSE(E, phi[, medium]) Returns the dose (Gy) of the spectrum
%
%   Returns the dose (Gy) of the spectrum specified by the vector phi
%   at energies specified by E (keV) in the specified medium.
%   Elements of phi must correspond to the energies E.
%
%   Updated October 2018 by I.A. Cunningham and T.F. Nano

function [Gy] = xrDose(E,phi,varargin)

if nargin > 2,
    medium = varargin{1};
else
    medium = 'water';
end

%Gy = sum(phi ./ xrqpGye(E, medium));
Gy = sum(phi .* xrGypqe(E, medium));
return
