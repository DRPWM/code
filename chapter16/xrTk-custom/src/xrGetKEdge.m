%   XRGETKEDGE(Z) returns the Kedge energy
% 
%   Given an element name or atomic number, returns the kedge binding energy
%   Only includes elements 54(Xenon) through 80(Mercury)
%
%   Updated to give binding energy from both element name and symbol.
%
%   Updated October 2018 by I.A. Cunningham and T.F. Nano

function kedge = xrGetKEdge(Z)

kedges(54) = 34.582;
kedges(55) = 35.985;
kedges(56) = 37.441;
kedges(57) = 38.925;
kedges(58) = 40.444;
kedges(59) = 41.991;
kedges(60) = 43.559;
kedges(61) = 45.184;
kedges(62) = 46.835;
kedges(63) = 48.520;
kedges(64) = 50.240;
kedges(65) = 51.996;
kedges(66) = 53.789;
kedges(67) = 55.618;
kedges(68) = 57.486;
kedges(69) = 59.390;
kedges(70) = 61.332;
kedges(71) = 63.314;
kedges(72) = 65.351;
kedges(73) = 67.414;
kedges(74) = 69.524;
kedges(75) = 71.676;
kedges(76) = 73.872;
kedges(77) = 76.112;
kedges(78) = 78.395;
kedges(79) = 80.723;
kedges(80) = 83.103;

if isnumeric (Z) == 0;
    t = lower(Z);
    s = sprintf('%15s', t);
elseif isnumeric (Z) == 1 & Z >= 54 | Z <= 75;
    s = Z;
elseif isnumeric (Z) == 1 & Z < 54 | Z > 75;
    s = Z;
    fprintf('ERROR: Atomic Number out of range.\n');
    return
end
    

if strcmp(s,'          xenon') | strcmp(s,'             xe') | s == 54
    kedge = kedges(54);
    return
elseif strcmp(s,'         cesium') | strcmp(s,'             ce') | s == 55
    kedge = kedges(55);
    return
elseif strcmp(s,'         barium') | strcmp(s,'             ba') | s == 56
    kedge = kedges(56);
    return 
elseif strcmp(s,'      lanthanum') | strcmp(s,'             la') | s == 57
    kedge = kedges(57);
    return
elseif strcmp(s,'         cerium') | strcmp(s,'             ce') | s == 58
    kedge = kedges(58);
    return
elseif strcmp(s,'   praseodymium') | strcmp(s,'             pm') | s == 59
    kedge = kedges(59);
    return
elseif strcmp(s,'      neodymium') | strcmp(s,'             nd') | s == 60
    kedge = kedges(60);
    return
elseif strcmp(s,'     promethium') | strcmp(s,'             pm') | s == 61
    kedge = kedges(61);
    return
elseif strcmp(s,'       samarium') | strcmp(s,'             sm') | s == 62
    kedge = kedges(62);
    return
elseif strcmp(s,'       europium') | strcmp(s,'             eu') | s == 63
    kedge = kedges(63);
    return
elseif strcmp(s,'     gadolinium') | strcmp(s,'             gd') | s == 64
    kedge = kedges(64);
    return
elseif strcmp(s, '        terbium') | strcmp(s, '             tb') | s == 65
    kedge = kedges(65);
    return
elseif strcmp(s,'     dysprosium') | strcmp(s,'             dy') | s == 66
    kedge = kedges(66);
    return
elseif strcmp(s,'        holmium') | strcmp(s,'             ho') | s == 67
    kedge = kedges(67);
    return
elseif strcmp(s, '         erbium') | strcmp(s, '             er') | s == 68
    kedge = kedges(68);
    return
elseif strcmp(s,'        thulium') | strcmp(s,'             tm') | s == 69
    kedge = kedges(69);
    return
elseif strcmp(s,'      ytterbium') | strcmp(s,'             yb') | s == 70
    kedge = kedges(70);
    return
elseif strcmp(s,'       lutetium') | strcmp(s,'             lu') | s == 71
    kedge = kedges(71);
    return
elseif strcmp(s,'        hafnium') | strcmp(s,'             hf') | s == 72
    kedge = kedges(72);
    return
elseif strcmp(s,'       tantalum') | strcmp(s,'             ta') | s == 73
    kedge = kedges(73);
    return
elseif strcmp(s,'       tungsten') | strcmp(s,'              w') | s == 74
    kedge = kedges(74);
    return
elseif strcmp(s,'        rhenium') | strcmp(s,'             re') | s == 75
    kedge = kedges(75);
    return
elseif strcmp(s,'         osmium') | strcmp(s,'             re') | s == 76
    kedge = kedges(75);
    return
elseif strcmp(s,'        iridium') | strcmp(s,'             re') | s == 77
    kedge = kedges(75);
    return
elseif strcmp(s,'       platinum') | strcmp(s,'             re') | s == 78
    kedge = kedges(75);
    return
elseif strcmp(s,'           gold') | strcmp(s,'             re') | s == 79
    kedge = kedges(75);
    return
elseif strcmp(s,'        mercury') | strcmp(s,'             re') | s == 80
    kedge = kedges(75);
    return

else fprintf('ERROR: Atomic Number out of range.\n');
    return
end