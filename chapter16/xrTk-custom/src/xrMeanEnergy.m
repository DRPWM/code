%   XRMEANENERGY(E,phi) Calculates the mean energy of the spectrum
%
%   Returns mean energy (keV) of spectrum phi (quanta/mm2/E bin) at E (keV)
%
%   Updated October 2018 by I.A. Cunningham and T.F. Nano

function [Emean] = xrMeanEnergy(E,phi)
Emean = xrMoment(E,phi,1)/xrMoment(E,phi,0);
return
