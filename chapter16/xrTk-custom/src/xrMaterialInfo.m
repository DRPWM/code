%   xrMaterialInfo(material)   Get material information from x-ray interaction coefficient tables
%   
%   returns a structure of information values
%
%   MATERIAL can be any material with a .tab file
%       eg. hydrogen, water, iodine, ...
%
%   Returns structure with the following fields:
%       Z_average       average atomic number
%       m_density       mass density (g/cm3)
%       a_weight        atomic weight
%       Z_effective     effective atomic number
%		e_density       electron density (electrons/g)
%       a_density       atomic density (atoms/g)
%       ion_potential   effective ionization potential(electrons)
%       Neshell         number of electrons in each atomic shell (elements only)
%       Eshell          ionization energy for each atomic shell (elements only)
%
%   Updated October 2018 by I.A. Cunningham and T.F. Nano

function info = xrMaterialInfo(material);

info = [];

if ~ischar(material),
    Z = material;
    material = xrElementName(Z);
end
fmaterial = strcat(material,'xxxxxx');
fname = sprintf('%s.tab',fmaterial(1:6));
filename = fname;
fid = fopen(filename,'r');
if fid <= 0
    error(sprintf('Could not open .tab file: %s',fname));
    return;
end
% Read file header info
L1 = '';
while length(L1) <= 2
    L1 =  fgets(fid);
end
[n_rows n_cols n_elem] = sscanf(L1,'%d %d %d %s');
L2 = fgets(fid);
s = sscanf(L2,'%f');
info.Z_average = s(1);          % average atomic number
info.m_density = s(2)/1000;     % mass density g/cm3
info.a_weight = s(3);           % atomic weight
info.Z_effective = s(4);        % effective atomic number
info.e_density = s(5)/1000;     % electrons/g
info.a_density = s(6)/1000;     % atoms/g
info.ion_potential = s(7);      % effective ionization potential (electrons)
info.name = material;
fclose(fid);
return
