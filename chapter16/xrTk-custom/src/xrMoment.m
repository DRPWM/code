%   XRMOMENT(E,phi,n) Calculates the nth energy moment of the spectrum
%
%   Returns nth moment for specifid phi and E 
%
%   Updated October 2018 by I.A. Cunningham and T.F. Nano

function [moment] = xrMoment(E,phi,n)

moment = sum(phi.*(E.^n));
return
