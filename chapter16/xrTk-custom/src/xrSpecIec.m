%   xrSpecIec(E, spectrumName[, year, mAs, mdis]) Generate IEC spectrum for mammography and radiography
%
%   Returns a vector the size of E of photons per mm2 per energy bin
%   for a spectrum of stated mAs (default=1) and source distance in m
%   (default=1)
%
%   E (keV) can be a single value or a vector of increasing values.  Elements
%       may start at any energy but must have uniform spacing.
%   spectrumname can be
%       RQA-3, RQA-5, RQA-7, RQA-9, RQA-M1, RQA-M2, RQA-M3, RQA-M4
%
%   Note that while the HVL is guaranteed to be the same as the IEC value,
%   the value of q/mm2/R is not the same, as summarized here...
%
%          ---- IEC 62220-1-1 2015 ----      ------- This Code ------------
%  Spectrum  kV  HVL q/mm2/uGy              HVL     q/mm2/uGy   Dif
%   RQA-3    50  3.8   20763                3.8     20608       -0.3%
%   RQA-5    70  6.8   29653                6.8     29869       +0.7%
%   RQA-7    90  9.2   32490                9.2     32716       +0.7%
%   RQA-9   120 11.6   31007                11.6    31007       +0.7%
%
%          ---- IEC 62220-1-2 2007 ----      ------- This Code ------------
%  Spectrum  kV  HVL q/mm2/uGy              HVL     q/mm2/uGy   Dif
%   RQA-M1   25 0.56    4639                0.56    4860        +4.8%
%   RQA-M2   28 0.60    4981                0.60    5217        +4.7%
%   RQA-M3   30 0.62    5303                0.62    5450        +2.8%
%   RQA-M4   35 0.68    6325                0.68    6275        -0.8%
%   Mo/Rh    28 0.65    5439                0.65    5511        +1.3%
%   Rh/Rh    28 0.74    5944                    
%   W/Rh     28 0.75    5975                0.75    6073        +1.6
%   W/Al     28 0.83    6575                0.83    6721        +2.2
%
%          ---- IEC 62220-1-1 2003 ----      ------- This Code ------------
%  Spectrum  kV  HVL q/mm2/uGy              HVL     q/mm2/uGy   Dif
%   RQA-3    50  4.0   21759                4.0     21500       -1.2%
%   RQA-5    70  7.1   30174                7.1     30523       +1.2%
%   RQA-7    90  9.1   32362                9.1     32650       +0.9%
%   RQA-9   120 11.5   31077                11.5    31303       +0.7%
%
%   Updated October 2018 by I.A. Cunningham and T.F. Nano 2018
%

 
function phi = xrSpecIec(E,spectrumName,varargin)

if nargin > 2
    year = varargin{1};
else
    year = 0;   % year not specified, default is 2015 for radiography
end

if nargin > 3
    mAs = varargin{2};
else
    mAs = 1;
end

if nargin > 4
    mdis = varargin{3};
else
    mdis = 1;
end

% check that IEC mammo year is not used for radiography spectra
if( (year == 2007) && ( strcmpi(spectrumName, 'RQA-3') || strcmpi(spectrumName, 'RQA-5')  || ... 
        strcmpi(spectrumName, 'RQA-7')  || strcmpi(spectrumName, 'RQA-9') ))
    fprintf('Error in specified spectrum name and year.\n');
    phi = [];
    return;
end

% check that IEC radiography year is not used for mammo spectra
if( (year == 2003 || year == 2015) && ( strcmpi(spectrumName, 'RQA-M1') || strcmpi(spectrumName, 'RQA-M2')  || ... 
        strcmpi(spectrumName, 'RQA-M3')  || strcmpi(spectrumName, 'RQA-M4') || ...
        strcmpi(spectrumName, 'RQA-Mo/Rh')  || strcmpi(spectrumName, 'Rh/Rh') || ...
        strcmpi(spectrumName, 'RQA-W/Rh')  || strcmpi(spectrumName, 'W/Al') ))
    fprintf('Error in specified spectrum name and year.\n');
    phi = [];
    return;
end

% check that incorrect year is not used
if( year ~= 0 && year ~= 2003 && year ~= 2007 && year ~= 2015 )
    fprintf('Error in specified year.\n');
    phi = [];
    return;
end


% Define a typical tungsten target tube

WRTube.target = 'W';
WRTube.degrees = 10;
WRTube.mmpyrex = 2.38;
WRTube.mmoil = 3.06;
WRTube.mmlexan = 2.66;


% Define a typical molybdenum target tube for mammo

MoMTube.target = 'Mo';
MoMTube.degrees = 13;
MoMTube.mmpyrex = 1.11;
MoMTube.mmoil = 1;
MoMTube.mmlexan = 1;

% Define a typical tungsten target tube for mammo

WMTube.target = 'W';
WMTube.degrees = 13;
WMTube.mmpyrex = 1.11;
WMTube.mmoil = 1;
WMTube.mmlexan = 1;

% Define a typical rhodium target tube for mammo

RhMTube.target = 'Rh';
RhMTube.degrees = 13;
RhMTube.mmpyrex = 1.11;
RhMTube.mmoil = 1;
RhMTube.mmlexan = 1;


if(strcmpi(spectrumName, 'RQA-3') && (year == 2015 || year == 0))
    %qpGyIEC = 20673e6;
    %hvlIEC = 3.8;
    kV = 50;
    tube = WRTube;
    addedFilterMaterial = 'aluminum';
    addedFiltermm = 1.50;
    hardeningFilterMaterial = 'aluminum';
    hardeningFiltermm = 10;
    mmAlAdj = 0.5;
elseif( strcmpi(spectrumName,'RQA-5') && (year == 2015 || year == 0))
    %qpGyIEC = 29653e6;
    %hvlIEC = 6.8;
    kV = 70;
    tube = WRTube;
    addedFilterMaterial = 'aluminum';
    addedFiltermm = 1.50;
    hardeningFilterMaterial = 'aluminum';
    hardeningFiltermm = 21;
    mmAlAdj = 0.9;
elseif(strcmpi(spectrumName,'RQA-7') && (year == 2015 || year == 0))
    %qpGyIEC = 32490e6;
    %hvlIEC = 9.2;
    kV = 90;
    tube = WRTube;
    addedFilterMaterial = 'aluminum';
    addedFiltermm = 1.50;
    hardeningFilterMaterial = 'aluminum';
    hardeningFiltermm = 30;
    mmAlAdj = 0.9;
elseif( strcmpi(spectrumName,'RQA-9') && (year == 2015 || year == 0))
    %qpGyIEC = 31007e6;
    %hvlIEC = 11.6;
    kV = 120;
    tube = WRTube;
    addedFilterMaterial = 'aluminum';
    addedFiltermm = 15;
    hardeningFilterMaterial = 'aluminum';
    hardeningFiltermm = 40;
    mmAlAdj = 1;
elseif( strcmpi(spectrumName, 'RQA-3') && (year == 2003))
    %qpGyIEC = 21759e6;
    %hvlIEC = 4.0;
    kV = 50;
    tube = WRTube;
    addedFilterMaterial = 'aluminum';
    addedFiltermm = 1.50;
    hardeningFilterMaterial = 'aluminum';
    hardeningFiltermm = 10;
    mmAlAdj = 2.4;%2.349491;
elseif( strcmpi(spectrumName,'RQA-5') && (year == 2003))
    %qpGyIEC = 30174e6;
    %hvlIEC = 7.1;
    kV = 70;
    tube = WRTube;
    addedFilterMaterial = 'aluminum';
    addedFiltermm = 1.50;
    hardeningFilterMaterial = 'aluminum';
    hardeningFiltermm = 21;
    mmAlAdj = 4.9;%4.733465;
elseif( strcmpi(spectrumName,'RQA-7') && (year == 2003))
    %qpGyIEC = 32362e6;
    %hvlIEC = 9.1;
    kV = 90;
    tube = WRTube;
    addedFilterMaterial = 'aluminum';
    addedFiltermm = 1.50;
    hardeningFilterMaterial = 'aluminum';
    hardeningFiltermm = 30;
    mmAlAdj = 0;%-0.724284;
elseif( strcmpi(spectrumName,'RQA-9') && (year == 2003))
    %qpGyIEC = 31077e6;
    %hvlIEC = 11.5;
    kV = 120;
    tube = WRTube;
    addedFilterMaterial = 'aluminum';
    addedFiltermm = 1.50;
    hardeningFilterMaterial = 'aluminum';
    hardeningFiltermm = 40;
    mmAlAdj = 0.373237;
elseif( strcmpi(spectrumName, 'RQA-M1') && (year == 2007 || year == 0) )
    %qpGyIEC = 4639e6;
    %hvlIEC = 0.56;
    kV = 25;
    tube = MoMTube;
    addedFilterMaterial = 'molybdenum';
    addedFiltermm = 0.032;
    hardeningFilterMaterial = 'aluminum';
    hardeningFiltermm = 2;
    mmAlAdj = -0.7;
elseif( strcmpi(spectrumName, 'RQA-M2') && (year == 2007 || year == 0))
    %qpGyIEC = 4981e6;
    %hvlIEC = 0.60;
    kV = 28;
    tube = MoMTube;
    addedFilterMaterial = 'molybdenum';
    addedFiltermm = 0.032;
    hardeningFilterMaterial = 'aluminum';
    hardeningFiltermm = 2;
    mmAlAdj = -0.7;
elseif( strcmpi(spectrumName, 'RQA-M3') && (year == 2007 || year == 0))
    %qpGyIEC = 5303e6;
    %hvlIEC = 0.62;
    kV = 30;
    tube = MoMTube;
    addedFilterMaterial = 'molybdenum';
    addedFiltermm = 0.032;
    hardeningFilterMaterial = 'aluminum';
    hardeningFiltermm = 2;
    mmAlAdj = -0.75;
elseif( strcmpi(spectrumName, 'RQA-M4') && (year == 2007 || year == 0))
    %qpGyIEC = 6325e6;
    %hvlIEC = 0.68;
    kV = 35;
    tube = MoMTube;
    addedFilterMaterial = 'molybdenum';
    addedFiltermm = 0.032;
    hardeningFilterMaterial = 'aluminum';
    hardeningFiltermm = 2;
    mmAlAdj = -0.67;
elseif( strcmpi(spectrumName, 'Mo/Rh') && (year == 2007 || year == 0))
    %qpGyIEC = 5439e6;
    %hvlIEC = 0.65;
    kV = 28;
    tube = MoMTube;
    addedFilterMaterial = 'rhodium';
    addedFiltermm = 0.025;
    hardeningFilterMaterial = 'aluminum';
    hardeningFiltermm = 2;
    mmAlAdj = -0.75;
elseif( strcmpi(spectrumName, 'Rh/Rh') && (year == 2007 || year == 0))
   %qpGyIEC = 5944;
   %hvlIEC = 0.74;
   kV = 28;
   tube = RhMTube;
   addedFilterMaterial = 'rhodium';
   addedFiltermm = 0.025;
   hardeningFilterMaterial = 'aluminum';
   hardeningFiltermm = 2;
   mmAlAdj = 0;
elseif strcmpi(spectrumName, 'W/Rh')
    %qpGyIEC = 5975;
    %hvlIEC = 0.75;
    kV = 28;
    tube = WMTube;
    addedFilterMaterial = 'rhodium';
    addedFiltermm = 0.050;
    hardeningFilterMaterial = 'aluminum';
    hardeningFiltermm = 2;
    mmAlAdj = -0.9;
elseif( strcmpi(spectrumName, 'W/Al') && (year == 2007 || year == 0))
    %qpGyIEC = 6575;
    %hvlIEC = 0.83;
    kV = 28;
    tube = WMTube;
    addedFilterMaterial = 'aluminum';
    addedFiltermm = 0.50;
    hardeningFilterMaterial = 'aluminum';
    hardeningFiltermm = 2;
    mmAlAdj = -0.817;
else
    phi = [];
    return;
end

% Generate raw spectrum

phi = xrSpec(E, kV, tube.target, mAs, mdis, tube.degrees, tube.mmpyrex, tube.mmoil, tube.mmlexan, 0);

% Added filter in tube

addedFilterInfo = xrMaterialInfo(addedFilterMaterial);
phi = phi .* xrTrans(E,addedFilterMaterial,addedFiltermm/10*addedFilterInfo.m_density);

% Beam-hardening filter for desired spectrum

hardeningFilterInfo = xrMaterialInfo(hardeningFilterMaterial);
phi = phi .* xrTrans(E,hardeningFilterMaterial,hardeningFiltermm/10*hardeningFilterInfo.m_density);

% Adjustment filter to force hvl to agree with iec value

AlInfo = xrMaterialInfo('aluminum');
phi = phi .* xrTrans(E,hardeningFilterMaterial,mmAlAdj/10*AlInfo.m_density);


return;
