% Interpolation of x-ray interaction coefficients

clear all;
close all;

dE = 0.01;
E = 1:dE:50;
material = 'iodine';

mu = xrCoef(E, material, 'attenuation', 1); % Mass attenuation coefficient of iodine

atten = xrCoef(E, material, 'attenuation');
pe = xrCoef(E, material, 'photoelectric');
coh = xrCoef(E, material, 'coherent');
inc = xrCoef(E, material, 'incoherent');

figure
semilogy(E, coh, E, inc, E, pe, E, atten);
grid on;
ylabel('Mass Coefficient (cm^2 g^{-1})');
xlabel('Energy (keV)');
legend('coherent', 'incoherent', 'photoelectric', 'attenuation');

