% Scale spectrum qo to 2.5 uGy air KERMA and calculate Qo (quanta/mm2 per Gy)

clear all;
close all;

dE = 0.25;
E = 10:dE:120; kV = 90;
qi0 = xrSpec(E, kV);

f = 2.5e-6/xrAirKerma(E, qi0); % Conversion factor to 2.5 uGy air KERMA
qi = qi0*f; % Normalized spectrum with 2.5 uGy air KERMA

K = xrAirKerma(E, qi)*1e6;
Qo = sum(qi)/K;

[Eb PhiB] = xrSpectrum2Bins(E, qi);

figure
plot(Eb, PhiB/dE);
grid on;
xlim([0 100]);
ylabel('Photons (mm^{-2} keV^{-1})');
xlabel('Energy (keV)');

fprintf('air KERMA = %f uGy\n', K);
fprintf('Qo = %f q/mm2/uGy\n', Qo);