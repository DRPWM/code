% Interpolation of x-ray interaction coefficients based on chemical formula

clear all;
close all;

E = 1:.01:50;
formula = 'Gd2O2S';
mu = xrCoefFormula(E, formula, 'attenuation');

figure
semilogy(E, mu);
grid on;
ylabel('Mass Attenuation Coefficient (cm^{2} g^{-1})');
xlabel('Energy (keV)');

