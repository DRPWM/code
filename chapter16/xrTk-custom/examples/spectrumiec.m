% Generation of standard IEC x-ray spectra

clear all;
close all;

dE = 0.25; EMin = 10; EMax = 120; % keV
E = EMin:dE:EMax;

spectrumName = 'RQA-5'; iecVersion = 2015;
qi = xrSpecIec(E, spectrumName, iecVersion);
hvl = xrHvl(E, qi);
K = xrAirKerma(E, qi)*1e6;
Qo = sum(qi)/K;


figure
[Eb PhiB] = xrSpectrum2Bins(E, qi);
plot(Eb, PhiB/dE);
grid on;
xlim([0 100]);
ylabel('Photons (mm^{-2} keV^{-1})');
xlabel('Energy (keV)');

fprintf('hvl = %f mmAl\n', hvl);
fprintf('air KERMA = %f uGy\n', K);
fprintf('Qo = %f q/mm2/uGy\n', Qo);

