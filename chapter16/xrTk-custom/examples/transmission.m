% Transmission of x-ray beam with spectrum q0 through an attenuator

clear all;
close all;

dE = 0.25;
E = 10:dE:100; kV = 90;

AlInfo = xrMaterialInfo('aluminum');  % Attenuator material data
AlThickness = 1.;  % Attenuator thickness [cm]
qi0 = xrSpec(E, kV);  % Spectrum incident on attenuator
massLoading = AlThickness*AlInfo.m_density; % Mass-loading [cm2/g]
qi = qi0.*xrTrans(E, 'aluminum', massLoading);  % Transmitted spectrum 

[Eb Phi0] = xrSpectrum2Bins(E, qi0);
[Eb Phi] = xrSpectrum2Bins(E, qi);

figure
plot(Eb, Phi0, Eb, Phi);
grid on;
ylabel('Photons (mm^{-2} keV^{-1})');
xlabel('Energy (keV)');

