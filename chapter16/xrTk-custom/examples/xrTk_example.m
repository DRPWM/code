% Iodine contrast and SNR with a CsI detector

dE = 0.5; % energy bin spacing size [keV]
E = 10:dE:150; % energy vector, from 10 to 150keV with dE bin size [keV]
q0 = xrSpec(E,120,'W'); % 120kV beam from W tube 1m from source [photons/mm2/keV] 
K = xrAirKerma(E,q0); % air kerma of spectrum q0 [Gy]
q = 250e-6 * q0 / K; % normalize the spectrum to give 250uGy air-KERMA

t_thickness = 15; % background attenuator (tissue) thickness [cm]
t_density = 1.06; % soft tissue density [g/cm^3] from ICRU-44
i_thickness = 0.2; i_density = 0.1; % thickness [cm] and density [g/cm^3] of iodine
iodine = xrMaterialInfo('iodine'); % material info of iodine, used to get density

% x-ray coefficients and quantum efficiency of CsI detector
CsI = xrMaterialInfoFormula('CsI'); % material info of ceasium iodine
CsI_gcm2 = 0.15; % detector mass loading [g/cm^2]
cm2g_ba = 6.022e23 / CsI.a_weight * 1e-24; % conversion [barns/atom] to [cm^2/g]
mu_CsI_PE = xrCoefFormula(E, 'CsI', 'sigmaPhotoelectric') * cm2g_ba; % [cm^2/g]
mu_CsI_INC = xrCoefFormula(E,'CsI','sigmaIncoherent') * cm2g_ba; % [cm^2/g]
QE_CsI = 1 - exp(-(mu_CsI_PE + mu_CsI_INC) * CsI_gcm2); % Eq. 6

kVset = 50:1:150; % range of kV settings used to generate different spectra

contrasti_CsI = zeros(1, length(kVset)); % vector of contrast values for each kV 
SNRi_CsI = zeros(1, length(kVset)); % vector of SNR values for each kV

k = 1; % arbitrary detector element gain, [0]
f = 0.85; % detector CsI fill (packing) factor, [0]
A = (0.01 * 0.01);  % element size is 0.1mm, element area [cm^2]
N = 100 * 100; % number of pixels, 100x100 pixels, ROI area = 1cm^2
G = 0.5; % anti-scatter grid factor [0]
M = 116 / 100; % geometric magnification from phantom entrance to detector

i=1; % counter and index for iteration through kV set

for kV=kVset
    q0 = xrSpec(E, kV, 'W'); % kV beam from W tube 1m from source [photons/mm2/keV] 
    q = 250e-6 * q0 / xrAirKerma(E, q0); % normalized spectrum to 250 uGy air-KERMA

    % quanta transmitted through tissue and incident on detector
    qt = G * (M^-2) * q .* xrTrans(E, 'water', t_thickness * t_density);

    % quanta transmitted through water and iodine and incident on detector 
    qi = G * (M^-2) * q .* xrTrans(E, 'water', (t_thickness-i_thickness) * t_density) .* xrTrans(E, 'iodine', i_thickness * i_density);

    % detector signal, Eqs. 5 and 7
    di = k * f * A * sum(qi .* QE_CsI .* E *dE);
    dt = k * f * A * sum(qt .* QE_CsI .* E *dE);

    % variance, Eq. 10
    var_t = (k^2) * f * A * sum(qt.*QE_CsI .* E .* E) *dE;
    var_i = (k^2) * f * A * sum(qi.*QE_CsI .* E .* E) *dE;

    % contrast as a function of x-ray beam kV, Eq. 4
    contrasti_CsI(i) = (di - dt) / dt;

    % SNR as a function of x-ray beam kV, Eq. 9
    SNRi_CsI(i) =  contrasti_CsI(i) * sqrt(N) * dt / sqrt(var_t);

    i = i + 1; % increment counter
end