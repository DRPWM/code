% Iodine contrast and SNR with a CsI detector

dE = 0.5; % Energy bin spacing size [keV]
E = 10:dE:150; % Energy vector, from 10 to 150 keV with dE bin size [keV]

tissue = 'Tissue_Soft_ICRU-44';
tissue_cm = 15; % Background attenuator (soft-tissue) thickness [cm]
tissue_density = 1.06; % Soft-tissue density [g/cm^3] from ICRU-44

iodine = 'iodine';
iodine_cm = 0.2; % Thickness [cm] of iodine
iodine_density = 0.1; % Density [g/cm^3] of iodine

% X-ray coefficients and quantum efficiency of detector
materiald = 'CsI';
infod = xrMaterialInfoFormula(materiald); % Material info of cesium iodide
massLoading = 0.141; % Detector mass loading [g/cm^2]
mu_pe = xrCoefFormula(E, materiald, 'photoelectric'); % [cm^2/g]
mu_inc = xrCoefFormula(E, materiald,'incoherent'); % [cm^2/g]
alpha = 1-exp(-(mu_pe+mu_inc)*massLoading); % Eq. 7


kVSet = 50:1:150; % Range of kV settings used to generate different spectra

contrast = zeros(1, length(kVSet)); % Vector for contrast values for each kV 
snr = zeros(1, length(kVSet)); % Vector for SNR values for each kV

k = 1; % Arbitrary detector element gain, [0]
fCsI = 0.8; % Detector CsI fill (packing) factor, [0]
a = 0.01*0.01;  % Element size is 0.1 mm, element area [cm^2]
N = 100*100; % Number of pixels, 100x100 pixels, ROI area = 1 cm^2
G = 0.5; % Anti-scatter grid factor [0]
M = 115/100; % Geometric magnification from phantom entrance to detector

i=1; % Counter and index for iteration through kV set
fprintf('Detector=%s\n', materiald);
for kV=kVSet
    qi0 = xrSpec(E, kV, 'W'); % Beam from W tube 1 m from source [photons/mm2/keV] 
    qin = 250e-6*qi0/xrAirKerma(E, qi0); % Normalized spectrum to 250 uGy air-KERMA

    % Quanta per mm2 transmitted through tissue and incident on detector
    qib = G*(M^-2)*qin.*xrTrans(E, tissue, tissue_cm*tissue_density);

    % Quanta per mm2 transmitted through tissue and iodine and incident on detector 
    qi = G*(M^-2)*qin.*xrTrans(E, tissue, (tissue_cm-iodine_cm)*tissue_density) ...
        .*xrTrans(E, iodine, iodine_cm*iodine_density);

    % Detector signal, Eqs. 6 and 8
    db = k*fCsI*a*sum(qib.*alpha.*E);
    d = k*fCsI*a*sum(qi.*alpha.*E);

    % Variance, Eq. 11
    varb = (k^2)*fCsI*a*sum(qib.*alpha.*E.*E);
    var = (k^2)*fCsI*a*sum(qi.*alpha.*E.*E);

    % Contrast as a function of x-ray beam kV, Eq. 5, made positive
    contrast(i) = (db-d)/db;

    % SNR as a function of x-ray beam kV, Eq. 10
    snr(i) = contrast(i)*sqrt(N)*db/sqrt(varb);
    
    fprintf('kV=%d contrast=%f SNR=%f\n', kV, contrast(i), snr(i));
    
    i = i+1; % Increment counter
end