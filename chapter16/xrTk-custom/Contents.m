%   xrTk routines (all start with "xr")
%
%   In the following, one-dimensional matrices (vectors) may be row or column
%   matrices.  Two dimensional matrices must have energy values extending in 
%   columns and spatial frequency values extending in rows.
%
%   xrairkerma     	  - calculates the air KERMA associated with the specified spectrum
%   xrcoef         	  - returns vector of x-ray interaction coefficients for specified element name
%   xrcoefformula  	  - returns vector of x-ray interaction coefficients for specified chemical formula
%   xrdose         	  - returns "entrance dose" for a specified spectrum
%   xrelementname  	  - returns name of specified atomic nnumber
%   xrenergy       	  - returns total energy in spectrum (intensity)
%   xrexposure     	  - calculate the exposure (R) of a specified spectrum
%   xrfmed         	  - returns a vector of f-medium values, ratio of Gy / R for a spectrum
%   xrformula2elements 	  - returns structure with formula info
%   xrgetbin		  - returns bin number of spectrum of specified energy value
%   xrgetgpcm2     	  - calculates the g/cm2 of a specified material to result in a specified
%   xrgetkedge     	  - returns k-edge energy of specified element
%   xrgetsymbol    	  - returns symbol of specified element
%   xrgetZ         	  - returns atomic number of specified element
%   xrGypqe        	  - returns vector of Gy per quantum/mm2 values at specified energies and material
%   xrhvl          	  - calculates the half-value layer of a specified spectrum
%   xrmaterialinfo 	  - Return a vector of information on the specified material 
%   xrmaterialinfoformula - returns information of specified chemical formula
%   xrmeanenergy   	  - returns mean energy of a spectrum (first moment of normalized spectrum)
%   xrmoment       	  - returns nth moment of spectrum
%   xrnquanta      	  - calculates the total number of quanta/mm2 in a spectrum 
%   xrqpGye        	  - returns vector of quanta per mm2 per Gy at specified energies
%   xrqpRe         	  - calculate quanta/mm2/R for a specified beam
%   xrrebin 	    	  - rebins a spectrum from one set of energy bins to another
%   xrqpRe         	  - calculates quanta/mm2/R at specified energies 
%   xrrebin        	  - rebin a spectrum from one E vector to another
%   xrRpqe		  - returns vector R per quanta/mm2 values at specified energies 
%   xrspec         	  - Generate diagnostic spectrum using Tucker/Barnes algorithm 
%   xrspeciec      	  - generates IEC spectra
%   xrspectrum2bins 	  - returns E and Phi vectors of specfied spectrum that can will plot spectrum in bins
%   xrtrans        	  - returns transmission factors at specified energies through specified element
%   xrtransformula 	  - returns transmission factors at specified energies through specfied chemical formula
%
%   Copyright 2018 I.A. Cunningham and lab
