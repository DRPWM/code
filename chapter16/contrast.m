% Image contrast from iodine in soft tissue

% Background region material, thickness [cm] and density [g/cm3]
materialb = 'Tissue_Soft_ICRU-44'; xb = 15; densityb = 1.06;

% Iodinated material, thickness [cm] and density [g/cm3]
material = 'iodine'; x = 0.2; density = 0.1;

% Generate spectrum
E = 10:.5:150;
kV = 50;
qi = xrSpec(E, kV);

% Detector converter material, thickness [cm], density [g/cm3]
materiald = 'CsI'; xd = 0.03; densityd = 4.7;
infod = xrMaterialInfoFormula(materiald); 

fprintf('Detector: %s\n', materiald);
fprintf('Soft tissue: %s\n', materialb);
fprintf('Contrast material: %s\n', material);

% Pixel area [cm2], k-factor, CsI fill-factor and grid-factor 
a = 0.1*0.1; k = 1;
f = 0.8; % Fill factor less than unity due to hexagonal packing of CsI
G = 0.5; % Grid factor

% Quantum efficiency
mu_pe = xrCoefFormula(E, materiald, 'photoelectric'); % [cm^2/g]
mu_inc = xrCoefFormula(E, materiald,'incoherent'); % [cm^2/g]
alpha = 1-exp(-(mu_pe+mu_inc)*densityd*xd); % Eq. 16.7

% Detector signals
db = k*f*a*sum(qi.*xrTrans(E, materialb, xb*densityb).*G.*alpha.*E); % Eq. 16.6
d = k*f*a*sum(qi.*xrTrans(E, materialb, (xb-x)*densityb) ...
    .*xrTrans(E, material, x*density).*G.*alpha.*E); % Eq. 16.8

% Contrast
C = (d-db)/db; % Eq. 16.5

fprintf('Contrast with 50-kV spectrum: %f\n', C);