% Examples of integrating with Java

%% Basics
clear all
close all
clc

disp('Check java version')
version -java
disp('Check which methods belong to the "File" Class')
methods('java.io.File')
disp('Display the files listed in the current working directory')
java.io.File('.').listFiles()

%% Integrating with Fiji (ImageJ)
% Before running install Fiji (see: https://fiji.sc)
% After installing Fiji be sure to go to help->update and specify the
% ImageJ MATLAB extension.

% Add the Fiji scripts path to MATLAB
addpath '/path/to/Fiji.app/scripts' % Customize this line for your system

disp(newline)
disp('Load ImageJ libraries and then close the ImageJ tooĺbar')
ImageJ;
ij.IJ.run('Quit','')

disp(newline)
disp('Load an X-ray image into ImageJ and calculate SNR')
imp = ij.IJ.openImage('myProjXray.dcm'); % Read in an image (DICOM) file
impDim = imp.getDimensions(); % Get the dimensions of the image
width = impDim(1);
height = impDim(2);
imp.setRoi(width/2-51, height/2-51, 100, 100); % Put ROI at centre of image
stats = imp.getStatistics(); % Get statistics for the ROI
imp.deleteRoi(); % Delete ROI (not necessary here, but can be useful)
meanVal = stats.mean; % Get mean pixel-value in ROI
stdVal = stats.stdDev; % Get standard deviation in ROI
SNR = meanVal/stdVal % Calculate pixel SNR

disp(newline)
disp('Load CT slice')
imp = ij.IJ.openImage('mySliceCT.dcm');

disp(newline)
disp('Display methods for DicomTools class')
methods('ij.util.DicomTools')
disp('Get study date using the DicomTools class')
studyDate = ij.util.DicomTools.getTag(imp,'0008,0020')

disp('Display an image slice using ImageJ')
imp.show()

disp('Hide the image')
imp.hide()

disp(newline)
disp('Duplicate image and run "Make Binary" and "Find Edges"')
imp2 = imp.duplicate();
ij.IJ.run(imp2, 'Make Binary', '');
ij.IJ.run(imp2, 'Find Edges', '');
imp2.show() % Show image with ImageJ

disp('Save image data shown in ImageJ as myArray in MATLAB workspace')
IJM.getDatasetAs('myArray') % Put the data in a MATLAB array
imp2.hide() % Close the image in ImageJ
imshow(myArray') % Display the image using MATLAB