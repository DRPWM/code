% Examples of integrating with .NET

%% Basics
clear all
close all
clc

disp('Check .NET supported')
NET.isNETSupported
disp('Display the files listed in the current working directory')
System.IO.Directory.GetFiles('.','*',System.IO.SearchOption.TopDirectoryOnly)

disp('Using an assembly registered in the GAC (system speech synthesizer)')
asm = NET.addAssembly('System.Speech');
speechSyn = System.Speech.Synthesis.SpeechSynthesizer;
speechSyn.Volume = 70;
Speak(speechSyn,'I am using .NET')

%% Example of integrating with a private assembly (Evil DICOM)
% Before running install Evil DICOM (see: https://www.nuget.org/packages/EvilDICOM)

% Add an assembly
evilDicomAssembly = 'EvilDICOM.dll'; % Name of assembly
pathToEDDll = '/path/to'; % Path to assembly (edit as appropriate)
NET.addAssembly(fullfile(pathToEDDll,evilDicomAssembly)); % Add assembly

% Import the namespaces you will need 
import EvilDICOM.Core.*;
import EvilDICOM.Core.Helpers.*;
import EvilDICOM.Core.Selection.*;
import EvilDICOM.Core.Enums.*;
import EvilDICOM.Core.IO.Writing.*;

dcmFile ='CT-MONO2-16-ankle.dcm';
dcm = DICOMObject.Read(dcmFile); % Read dicom file into a DICOMObject

% Finding tags in the DICOMObject
patientName = dcm.FindFirst(TagHelper.PatientName); % Find patient name tag

allPersonNameElements = dcm.FindAll(Enums.VR.PersonName); % A .NET List
for n = 0:allPersonNameElements.Count-1 % List is indexed from 0 (not 1)
    thisElement = allPersonNameElements.Item(n);
    % Lookup the description for each returned element
    desc=char(EvilDICOM.Core.Dictionaries.TagDictionary.GetDescription(...
        thisElement.Tag.CompleteID));
    disp(['Tag name: ' desc]); % Display each description to screen
end

% The DICOMSelector object is very handy for quick selection and manipulation
sel = dcm.GetSelector();
rowSize = sel.Rows.Data;
columnSize = sel.Columns.Data;

% Modify a DICOM Tag
originalValue = char(sel.PatientName.Data);
newValue = 'Modified using Evil DICOM';
sel.PatientName.Data = System.String(newValue); % Modify PatientName string
sel.ToDICOMObject.Write('ModifiedSample.dcm'); % Write modified DICOM file
dcmMod = DICOMObject.Read('ModifiedSample.dcm'); % Read it back in
selMod = dcmMod.GetSelector();
disp(['Original patient name: ' originalValue]); % Display
disp(['Modified patient name: ' char(selMod.PatientName.Data)]); % Display