% Examples of integrating with Python

%% Basics
clear all
close all
clc

disp('Check Python environment')
try
    pyenv % R2019b and later
catch
    pyversion % R2019a and before
end
disp('Display the files listed in the current working directory')
py.os.listdir('.')

%% Integrating with Python and SpekPy toolkit
% Before running install SpekPy (see: https://bitbucket/spekpy/spekpy_release)

% This condition is triggered for Unix systems only
% It prevents some compatibility problems between e.g. Python's 
% numpy/scipy libraries and MATLAB's use of Intel's MKL
if ~ispc & ~ismac
    RTLD_NOW = int32(2);
    RTLD_DEEPBIND = int32(8);
    py.sys.setdlopenflags(bitor(RTLD_NOW,RTLD_DEEPBIND));
end

pot = 80; % The x-ray tube potential in kV
ang = 12; % The target anode angle
spekpyArgs = pyargs('kvp', pot, 'th', ang); % Define Python keyword args
s = py.spekpy.Spek(spekpyArgs); % Create a spectrum model with arguments
s.filter('Al',6.0); % Filter the spectrum with 6 mm of aluminium

hvl = s.get_hvl1()

spectrumArgs = pyargs('edges',true); % Define Python arguments
spectrumData = s.get_spectrum(spectrumArgs); % Get the spectrum data

kNumPy = spectrumData{1}; % Extract the energy bins
fNumPy = spectrumData{2}; % Extract the fluence values
kListPy = py.list(kNumPy); % Convert NumPy array to Python list
fListPy = py.list(fNumPy); % Convert NumPy array to Python list
kCellMAT = cell(kListPy); % Convert Python list to MATLAB cell array
fCellMAT = cell(fListPy); % Convert Python list to MATLAB cell array
k = cell2mat(kCellMAT); % Convert MATLAB cell array to double array
f = cell2mat(fCellMAT); % Convert MATLAB cell array to double array

plot(k, f) % Plot spectrum
xlim([0 90])
xlabel('Energy bin  [keV]')
ylabel('Fluence per unit energy @ 1 m  [cm^{-2} keV^{-1}]')
title('X-ray spectrum')