# Chapter 6: Integration with other programming languages and environments
The demo scripts in this folder execute the code discussed in Chapter 6. The scripts are:

**systemDemo.m** - examples using the MATLAB "`system()`" command (including running the ImageJ macro "roiMacro.ijm")

**javaDemo.m** - examples using Java in MATLAB (including interfacing with Fuji-ImageJ using the ImageJ-MATLAB extension)

**pythonDemo.m** - examples using Python in MATLAB (including SpekPy, a spectrum modelling toolkit)

**dotnetDemo.m** - example using the .NET framework in MATLAB (including the Evil DICOM library)

There are also two DICOM images supplied, "myProjXray.dcm" and "mySliceCT.dcm", that are used by the *system* and *Java* demos, respectively.

## Setup ##
A base install of MATLAB is required.

Required MATLAB toolboxes:  
- *Image Processing Toolbox* (only for the DICOM dataset used for the .NET demo)

Tested with MATLAB R2019b.

Additional requirements (see the chapter text for further details):  
- the [Fiji](https://fiji.sc) ImageJ distribution (for the sytem and Java demos)  
- A compatible Python distribution and [SpekPy](https://bitbucket.org/spekpy/spekpy_release) (for the Python demo)  
- A Windows computer with compatible .NET framework version and the [EVIL DICOM](https://github.com/rexcardan/Evil-DICOM) library (for the .NET demo)

## Contact ##
Gavin Poludniowski  
gpoludniowski@gmail.com

## License ##
[MIT](https://choosealicense.com/licenses/mit/). See the license file in this folder (LICENSE.txt).
