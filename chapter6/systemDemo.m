% Examples of integrating with operating system

%% Basics
clear all
close all
clc

disp('Display the files listed in the current working directory')
if ispc % Windows
[status, cmdout] = system('dir');
elseif isunix % Unix/Mac
[status, cmdout] = system('ls');
end
cmdout

%% Integrating with Fiji (ImageJ) via system command
% Before running install Fiji (see: https://fiji.sc)
% If you want to add Fiji to the system path from MATLAB try:
% >> PATH = getenv('PATH')
% and customize the following for your system:
% >> setenv('PATH', [PATH ':/path/to/Fiji.app']); % Unix
% or
% >> setenv('PATH', [PATH ';C:\path\to\Fiji.app']); % Windows

disp('Load an X-ray image into ImageJ and calculate SNR')
% Replace "ImageJ.exe" with binary name as appropriate e.g. ImageJ-linux64 
command = 'ImageJ.exe --headless -macro roiMacro.ijm';
[status, cmdout] = system(command);

cmdoutSplit = splitlines(cmdout); % Split the output by lines
matchArray = contains(cmdoutSplit,'StdDev'); % Get binary array for string matches
iMatch = find(matchArray); % Find index of the non-zero element (the matching line)
dataLine = char(cmdoutSplit(iMatch+1)); % Get the next line and convert from cell
dataLineSplit = strsplit(dataLine); % Split character array by white spaces
meanVal = str2double(dataLineSplit(2)); % Extract mean, convert to double
stdVal = str2double(dataLineSplit(3)); % Extract stdev, convert to double

SNR = meanVal/stdVal